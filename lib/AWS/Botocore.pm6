unit module AWS::Botocore;

#`«
** UNIMPLEMENTED SYMBOLS **
class NullHandler
log
tuple ScalarTypes
class UNSIGNED
»

# todo implement BOTOCORE_ROOT if needed for AWS::Botocore::Loaders

my regex first-cap { <( (.) (<[A..Z]>) )> <[a..z]>+ }
my regex end-cap { (<[a..z | 0..9]>) (<[A..Z]>) }
my regex special-case-transform { <[A..Z]> ** 3..* s $ }

my Str %xform-cache{Str} =
    'CreateCachediSCSIVolume:_' => 'create_cached_iscsi_volume',
    'CreateCachediSCSIVolume:-' => 'create-cached-iscsi-volume',
    'DescribeCachediSCSIVolumes:_' => 'describe_cached_iscsi_volumes',
    'DescribeCachediSCSIVolumes:-' => 'describe-cached-iscsi-volumes',
    'DescribeStorediSCSIVolumes:_' => 'describe_stored_iscsi_volumes',
    'DescribeStorediSCSIVolumes:-' => 'describe-stored-iscsi-volumes',
    'CreateStorediSCSIVolume:_' => 'create_stored_iscsi_volume',
    'CreateStorediSCSIVolume:-' => 'create-stored-iscsi-volume',
    'ListHITsForQualificationType:_' => 'list_hits_for_qualification_type',
    'ListHITsForQualificationType:-' => 'list-hits-for-qualification-type'
;

sub xform-name(Str $name is copy, Str $sep = '_', :%cache = %xform-cache) is export {
    my &join-with-sep = { join $sep, .[*] };
    return $name if $name.contains: $sep;
    my $key = join ':', $name, $sep;
    without %cache{$key} {
        $name.=subst: &special-case-transform, {$sep ~ .lc};
        $name.=subst: &first-cap, &join-with-sep;
        $name.=subst: &end-cap, &join-with-sep;
        %cache{$key} = $name.lc;
    }
    return %cache{$key};
}
