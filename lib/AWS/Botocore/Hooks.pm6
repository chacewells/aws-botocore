unit module AWS::Botocore::Hooks;

use AWS::Botocore::BaseEventHooks;
use AWS::Botocore::Constants :EVENT-ALIASES;
use AWS::Botocore::Logger;

my Logger $LOG.=new: name => $?PACKAGE.^name;

my constant $FIRST = 0;
my constant $MIDDLE = 1;
my constant $LAST = 2;

class NodeList does Positional is export(:NodeList, :Trie) {
    has (@.first, @.middle, @.last) is rw;
    method elems { 3 }
    multi method AT-POS(0) { return-rw @!first }
    multi method AT-POS(1) { return-rw @!middle }
    multi method AT-POS(2) { return-rw @!last }
    multi method AT-POS(Int) { !!! }
    multi method EXISTS-POS(Int $ where ^3) { True }
    multi method EXISTS-POS(Int $) { False }
    multi method copy(NodeList:U:) { NodeList }
    multi method copy(NodeList:D: --> NodeList:D) {
        my $copy = NodeList.new: :@!first, :@!middle, :@!last;
        $copy;
    }
}

subset Response of Positional where {.elems == 2};

sub first-non-none-response(@responses, :$default = Nil) is export {
    for @responses -> Response $r {
        return $_ with $r[1];
    }
    $default;
}

class TrieNode is export(:Trie) {
    has Str $.chunk;
    has TrieNode %.children{Str};
    has NodeList $.values is rw;

    multi method copy(TrieNode:U:) { TrieNode }
    multi method copy(TrieNode:D:) {
        my TrieNode %children{Str} = %!children.map({ .key => .value.copy });
        my NodeList $values = $!values.copy;
        TrieNode.new: :$!chunk, :%children, :$values;
    }
}

#`((Specialized prefix trie that handles wildcards.

The prefixes in this case are based on dot separated
names so 'foo.bar.baz' is::

    foo -> bar -> baz

Wildcard support just means that having a key such as 'foo.bar.*.baz' will
be matched with a call to ``get_items(key='foo.bar.ANYTHING.baz')``.

You can think of this prefix trie as the equivalent as defaultdict(list),
except that it can do prefix searches:

    foo.bar.baz -> A
    foo.bar -> B
    foo -> C

Calling ``get_items('foo.bar.baz')`` will return [A + B + C], from
most specific to least specific.

))
class PrefixTrie is export(:Trie) {
    has TrieNode $.root;

    submethod BUILD(TrieNode :$!root) {
        .=new without $!root;
    }

    method append-item(Str $key, $value, Int :$section = $MIDDLE --> Nil) {
        my Str @key-parts = split '.', $key;
        my TrieNode $current = $!root;
        for @key-parts -> Str $part {
            if $current.children{$part}:!exists {
                my TrieNode $new-child.=new: :chunk($part);
                $current.children{$part} = $new-child;
                $current = $new-child;
            } else {
                $current = $current.children{$part};
            }
        }
        $current.values = NodeList.new without $current.values;
        push $current.values[$section], $value;
    }

    method prefix-search(Str $key) {
        my Str @key-parts = split '.', $key;
        my TrieNode $current = $!root;
        my @collected;
        self!get-items: $current, @key-parts, @collected, 0;
        @collected;
    }

    method !get-items(TrieNode $starting-node, Str @key-parts, @collected, Int $starting-index) {
        my @stack = $($starting-node, $starting-index);
        my Int $key-parts-len = +@key-parts;
        while @stack {
            my (TrieNode $current-node, Int $index) = pop @stack;
            with $current-node.values {
                my @complete-order = (.first, .middle, .last).flat given $current-node.values;
                prepend @collected, @complete-order;
            }
            if $index != $key-parts-len {
                my %children := $current-node.children;
                my TrieNode $directs = %children{@key-parts[$index]};
                my TrieNode $wildcard = %children<*>;
                my Int $next-index = $index.succ;
                push @stack, $($_, $next-index) with $wildcard;
                push @stack, $($_, $next-index) with $directs;
            }
        }
    }

    method remove-item(Str $key, $value --> Nil) {
        my Str @key-parts = split '.', $key;
        my TrieNode $current = $!root;
        self!remove-item: $current, @key-parts, $value, 0;
    }

    method !remove-item(TrieNode $current-node, Str @key-parts, $value, Int $index --> Nil) {
        return without $current-node;
        if $index < @key-parts {
            with $current-node.children{@key-parts[$index]} -> TrieNode $next-node {
                self!remove-item: $next-node, @key-parts, $value, $index.succ;
                if $index == @key-parts - 1 {
                    given $next-node.values {
                        with .first.first({ $_ eqv $value }, :k) -> $i {
                            .first.splice: $i, 1;
                        } orwith .middle.first({ $_ eqv $value }, :k) -> $i {
                            .middle.splice: $i, 1;
                        } orwith .last.first({ $_ eqv $value }, :k) -> $i {
                            .last.splice: $i, 1;
                        }
                    }
                }
                $current-node.children{@key-parts}:delete
                    unless $next-node.children or $next-node.values;
            } else { # todo typed exception
                die "Key is not in trie: @key-parts.join('.')";
            }
        }
    }

    method copy {
        my $copy = PrefixTrie.new: :root($!root.copy);
        $copy;
    }
}

class HierarchicalEmitter does BaseEventHooks is export {
    has %.lookup-cache;
    has PrefixTrie $.handlers;
    has %.unique-id-handlers;

    submethod BUILD(:%!lookup-cache, PrefixTrie :$!handlers, :%!unique-id-handlers) {
        .=new without $!handlers;
    }

    method emit-until-response(Str $event-name, *%args) {
        my @responses = self!emit: $event-name, %args, :stop-on-response;
        @responses ?? @responses[*-1] !! $(Nil, Nil);
    }

    #`((
            Emit an event by name with arguments passed as keyword args.

            >>> responses = emitter.emit(
            ...     'my-event.service.operation', arg1='one', arg2='two')

        :rtype: list
        :return: List of (handler, response) tuples from all processed
                 handlers.
    ))
    method emit(Str $event-name, *%args) { self!emit: $event-name, %args }

    #`((
        Emit an event with optional keyword arguments.

        :type event_name: string
        :param event_name: Name of the event
        :type kwargs: dict
        :param kwargs: Arguments to be passed to the handler functions.
        :type stop_on_response: boolean
        :param stop_on_response: Whether to stop on the first non-None
                                response. If False, then all handlers
                                will be called. This is especially useful
                                to handlers which mutate data and then
                                want to stop propagation of the event.
        :rtype: list
        :return: List of (handler, response) tuples from all processed
                 handlers.
    ))
    method !emit(Str $event-name, %args, Bool :$stop-on-response = False) {
        my @responses;
        # Invoke the event handlers from most specific
        # to least specific, each time stripping off a dot.
        my @handlers-to-call;
        if %!lookup-cache{$event-name}:!exists {
            @handlers-to-call = $!handlers.prefix-search: $event-name;
            %!lookup-cache{$event-name} = @handlers-to-call;
        } else {
            @handlers-to-call = | %!lookup-cache{$event-name};
        }
        return () unless @handlers-to-call;
        %args<event-name> = $event-name;
        for @handlers-to-call -> &handler {
            $LOG.debug: "Event $event-name: calling handler {&handler.name}";
            my $response = handler |%args;
            push @responses, $(&handler, $response);
            return @responses if $stop-on-response and $response.defined;
        }
        return @responses;
    }

    method register($event-name, &handler:(*%), :$unique-id, Bool :$unique-id-uses-count = False) {
        self!register-section: $event-name, &handler, :$unique-id, :$unique-id-uses-count, :section($MIDDLE);
    }

    method register-last($event-name, &handler:(*%), :$unique-id, Bool :$unique-id-uses-count = False) {
        self!register-section: $event-name, &handler, :$unique-id, :$unique-id-uses-count, :section($LAST);
    }

    method register-first($event-name, &handler:(*%), :$unique-id, Bool :$unique-id-uses-count = False) {
        self!register-section: $event-name, &handler, :$unique-id, :$unique-id-uses-count, :section($FIRST);
    }

    method !register-section($event-name, &handler:(*%), :$unique-id, Bool :$unique-id-uses-count, :$section) {
        with $unique-id {
            if $unique-id ∈ %!unique-id-handlers {
                my $count = %!unique-id-handlers{$unique-id}<count> orelse Nil;
                if $unique-id-uses-count {
                    if not $count.defined {
                        die qq:to/ERROR/;
                        Initial registration of unique id $unique-id was
                        specified to use a counter. Subsequent register
                        calls to unique id must specify use of a counter
                        as well.
                        ERROR
                    } else {
                        %!unique-id-handlers{$unique-id}<count>.=succ;
                    }
                } elsif $count {
                    die qq:to/ERROR/;
                    Initial registration of unique id $unique-id was
                    specified to not use a counter. Subsequent
                    register calls to unique id must specify not to
                    use a counter as well.
                    ERROR
                }
                return;
            } else {
                # Note that the trie knows nothing about the unique
                # id.  We track uniqueness in this class via the
                # _unique_id_handlers.
                $!handlers.append-item: $event-name, &handler, :$section;
                my %unique-id-handler-item = :&handler;
                %unique-id-handler-item<count> = 1 if $unique-id-uses-count;
                %!unique-id-handlers{$unique-id} = $%unique-id-handler-item;
            }
        } else {
            $!handlers.append-item: $event-name, &handler, :$section;
        }
        # Super simple caching strategy for now, if we change the registrations
        # clear the cache.  This has the opportunity for smarter invalidations.
        %!lookup-cache{}:delete;
    }

    method unregister($event-name, &handler? is copy, :$unique-id, Bool :$unique-id-uses-count = False) {
        with $unique-id {
            return unless $unique-id ∈ %!unique-id-handlers;
            my Int $count = %!unique-id-handlers{$unique-id}<count> // Int;
            if $unique-id-uses-count {
                if not $count.defined {
                    die qq:to/ERROR/;
                    Initial registration of unique id $unique-id was specified to
                    use a counter. Subsequent unregister calls to unique
                    id must specify use of a counter as well.
                    ERROR
                } elsif $count == 1 {
                    &handler = %!unique-id-handlers{$unique-id}<handler>;
                    %!unique-id-handlers{$unique-id}:delete;
                } else {
                    %!unique-id-handlers{$unique-id}<count>.=pred;
                    return;
                }
            } else {
                die qq:to/ERROR/ with $count;
                    Initial registration of unique id $unique-id was specified
                    to not use a counter. Subsequent unregister calls
                    to unique id must specify not to use a counter as
                    well.
                ERROR
                &handler = %!unique-id-handlers{$unique-id}<handler>;
                %!unique-id-handlers{$unique-id}:delete;
            }
        }
        try {
            $!handlers.remove-item: $event-name, &handler;
            %!lookup-cache{}:delete;
        }
    }

    method copy {
        my %lookup-cache = %!lookup-cache.map: {.key => .value.clone};
        my %unique-id-handlers = %!unique-id-handlers.map: {.key => .value.clone};
        my PrefixTrie $handlers = $!handlers.copy;
        HierarchicalEmitter.new: :%lookup-cache, :$handlers, :%unique-id-handlers;
    }
}

class EventAliaser does BaseEventHooks is export {
    has BaseEventHooks $.emitter;
    has Str %.event-aliases{Str};

    submethod BUILD(:$!emitter, :%!event-aliases) {
        %!event-aliases = %EVENT-ALIASES unless %!event-aliases;
    }

    # todo capture arguments to pass on to forwarded method call
    method register-last($event-name, &handler, :$unique-id, Bool :$unique-id-uses-count = False) {
        my $aliased-event-name = self!alias-event-name: $event-name;
        $!emitter.register-last: $event-name, &handler, :$unique-id, :$unique-id-uses-count;
    }

    method register-first($event-name, &handler, :$unique-id, Bool :$unique-id-uses-count = False) {
        my $aliased-event-name = self!alias-event-name: $event-name;
        $!emitter.register-first: $event-name, &handler, :$unique-id, :$unique-id-uses-count;
    }

    method unregister($event-name, &handler, :$unique-id, Bool :$unique-id-uses-count = False) {
        my $aliased-event-name = self!alias-event-name: $event-name;
        $!emitter.unregister: $aliased-event-name, &handler, :$unique-id, :$unique-id-uses-count;
    }

    method emit($event-name, *%args) {
        my $aliased-event-name = self!alias-event-name: $event-name;
        $!emitter.emit: $aliased-event-name, |%args;
    }

    method register($event-name, &handler:(*%), :$unique-id, Bool :$unique-id-uses-count = False) {
        my $aliased-event-name = self!alias-event-name: $event-name;
        $!emitter.register: $aliased-event-name, &handler, :$unique-id, :$unique-id-uses-count;
    }

    method !alias-event-name(Str $event-name) {
        for %!event-aliases.kv -> Str $old-part, Str $new-part {
            my @event-parts = split '.', $event-name;
            if not $old-part.index('.').defined {
                try {
                    @event-parts[@event-parts.first($old-part, :k)] = $new-part;
                    CATCH { default { next } }
                }
            } orwith $event-name.index($old-part) {
                my @old-parts = split '.', $old-part;
                replace-subsection @event-parts, @old-parts, $new-part;
            } else {
                next;
            }

            my $new-name = join '.', @event-parts;
            $LOG.debug: "Changing event name from $event-name to $new-name";
            return $new-name;
        }

        return $event-name;
    }

    method copy(--> EventAliaser:D) {
        EventAliaser.new: emitter => $!emitter.copy, event-aliases => %!event-aliases.clone;
    }

    my sub replace-subsection(@sections, @old-parts, $new-part) {
        for @sections.keys.rotor(@old-parts.elems => -@old-parts.elems.pred) -> @keys {
            if @sections[@keys] ~~ @old-parts {
                splice @sections, @keys[0], @keys.elems, $new-part;
                return;
            }
        }
    }

}

