unit module AWS::Botocore::Request;
use AWS::Botocore::Utils;
use URL;

#`«
    This function prepares a request dict to be created into an
    AWSRequestObject. This prepares the request dict by adding the
    url and the user agent to the request dict.

    :type request_dict: dict
    :param request_dict:  The request dict (created from the
        ``serialize`` module).

    :type user_agent: string
    :param user_agent: The user agent to use for this request.

    :type endpoint_url: string
    :param endpoint_url: The full endpoint url, which contains at least
        the scheme, the hostname, and optionally any path components.
»
sub prepare-request-hash(%r, :$endpoint-url, :$user-agent, :%context) is export {
    %r<headers><User-Agent> = $_ with $user-agent;
    my Str $host-prefix = $_ with %r<host_prefix>;
    my $url = urljoin $endpoint-url, %r<url_path>, $host-prefix;
    with %r<query_string> {
        my $encoded-query-string = percent-encode-sequence %r<query_string>;
        my $join-char = $url.contains('?') ?? '&' !! '?';
        $url ~= $join-char ~ $encoded-query-string if $encoded-query-string;
    }
    %r<url> = $url;
    %r<context> = %context;
}

sub urljoin(Str $endpoint-url, Str $url-path, Str $host-prefix? --> Str) {
    my URL $p.=new($endpoint-url);
    with $url-path {
        when .chars > 1 and .starts-with('/') {
            $p.=add-path(.substr(1));
        }
        when .so and $_ ne '/' {
            $p.=add-path($_)
        }
    }
    my $new-host = $p.hostname;
    $new-host.=prepend($_) with $host-prefix;
    my $result = URL.new( |$p.Hash, hostname => $new-host );
    ~$result;
}

class AWSRequest {...}
class AWSPreparedRequest {...}
class HeadersHash is Hash {...}

class AWSRequestPreparer {
    method prepare(AWSRequest $original --> AWSPreparedRequest) {
        my $url = prepare-url $original;
        my $body = prepare-body $original.data;
        my %headers is HeadersHash = prepare-headers $original, $body;
        return AWSPreparedRequest.new: :$url, :$body, :%headers;
    }

    my sub prepare-url(AWSRequest $original --> Str) {
        my $url = $original.url;
        with $original.params -> $params-coll {
            my $params = percent-encode-sequence @$params-coll;
            $url ~= '?' ~ $params;
        }
        return $url;
    }

    my multi prepare-body(Any:U --> Nil) {}
    my multi prepare-body('' --> Nil) {}
    my multi prepare-body(Any:D $body) { $body }
    my multi prepare-body(%data --> Str) {
        percent-encode-sequence %data;
    }

    my sub prepare-headers(AWSRequest $original, $prepared-body?) {
        my %headers is HeadersHash = $original.headers;
        return %headers if any(%headers<Transfer-Encoding Content-Length>:exists);

        unless $original.method ~~ any <GET HEAD OPTIONS> {
            my $length = determine-content-length $prepared-body;
            with $length {
                %headers<Content-Length> = .Str;
            } else {
                my $body-type = $prepared-body.WHAT;
                # debug "failed to determine length of $body-type"
                %headers<Transfer-Encoding> = 'chunked';
            }
        }

        return %headers;
    }

    my multi determine-content-length(Any:U $body) { 0 }
    my multi determine-content-length(Str:D $body --> Int:D) { $body.chars }
    my multi determine-content-length(IO::Handle:D $body -->Int:D) {
        my $orig-pos = $body.tell;
        $body.seek: 0, SeekFromEnd;
        my $eof-pos = $body.tell;
        $body.seek: $orig-pos;
        return $eof-pos - $orig-pos;
    }
}

class AWSRequest is rw is export {
    has Str $.method;
    has Str $.url;
    has %.headers is HeadersHash;
    has $.data;
    has $.params;
    has Str $.auth-path;
    has Bool $.stream-output is default(False);
    has %.context;
    has AWSRequestPreparer $!request-preparer;

    submethod BUILD(
            Str :$!method,
            Str :$!url,
            :%!headers,
            :$!data,
            :$!params,
            Str :$!auth-path,
            Bool :$!stream-output) { }

    method prepare(--> AWSPreparedRequest) {
        return $!request-preparer.prepare(self);
    }

    method body(--> Str) { self.prepare.body }

    method Hash {
        return %(:$!method, :$!url, :%!headers, :$!data, :$!params, :$!auth-path, :$!stream-output);
    }

    method with(*%attrs --> AWSRequest) {
        self.new: |self.Hash, |%attrs;
    }
}

class AWSPreparedRequest is export {
    has Str $.method; # todo method enum? is there an existing lib I can use for HTTP methods?
    has Str $.url;
    has %.headers is HeadersHash;
    has $.body; # type TBD; maybe just IO::Handle and populate with IO::Str or bytes for non-streamable content
    has Bool $.stream-request;

    method reset-stream {
        .($!body, 0) with $!body.^can('seek').first;
    }

    method Hash {
        return %( :$!method, :$!url, :%!headers, :$!body, :$!stream-request )
    }

    method with(*%attrs) {
        self.new: |self.Hash, |%attrs;
    }
}

class HeadersHash is export {
    method AT-KEY(\key) {
        return-rw self.Hash::AT-KEY(key.lc);
    }
    method EXISTS-KEY(\key) {
        return self.Hash::EXISTS-KEY(key.lc);
    }
    method STORE(\values) {
        my \lc-values = values.map: {
            .key.lc => .value
        };
        self.Hash::STORE(lc-values);
    }
}