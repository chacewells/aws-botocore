unit module AWS::Botocore::Paginate;

use Jmespath:from<Perl5>;

use MIME::Base64;
use JSON::Fast;

use AWS::Botocore::Utils;
use AWS::Botocore::Model;

class TokenEncoder is export {
    method encode($token) {
        my $json-string = to-json $token;
        return MIME::Base64.encode-str: $json-string;
    }
}

class TokenDecoder is export {
    method decode(Str $token) {
        my $json-string = MIME::Base64.decode-str($token);
        my $decoded-token = from-json $json-string;

        with $decoded-token<boto_encoded_keys>:delete -> $encoded-keys {
            return decode $decoded-token, $encoded-keys;
        } else {
            return $decoded-token;
        }
    }

    sub decode($token is rw, $encoded-keys) {
        for @$encoded-keys -> $key {
            my $encoded = path-get $token, $key;
            my $decoded = MIME::Base64.decode-str($encoded);
            path-set $token, $key, $decoded;
        }
        return $token;
    }

    sub path-get($data, @path) {
        my $d = $data;
        for @path -> $step {
            $d = $d ~~ Positional ?? $d[$step] !! $d{$step};
        }
        return $d;
    }

    sub path-set($data, @path, $value) {
        my $container = path-get $data, @path[^@path.end];
        given $container {
            when Positional { .[@path[*-1]] = $value }
            when Associative { .{@path[*-1]} = $value }
            default { die "not a collection" }
        }
    }
}

class PaginatorModel is export {
    has %.paginator-config;
    submethod TWEAK {
        %!paginator-config = %!paginator-config<pagination>;
    }
    method get-paginator(Str:D $operation-name) {
        # todo definite error
        %!paginator-config{$operation-name} // die "Paginator for operation does not exist: $operation-name";
    }
}

class PageIterator does Iterable is export {
    has &.method;
    has @.input-token;
    has @.output-token;
    has $.more-results;
    has @.result-keys;
    has $.max-items;
    has $.limit-key;
    has $.starting-token;
    has $.page-size;
    has %.op-kwargs;
    has $!resume-token;
    has @.non-aggregate-keys;
    has %!non-aggregate-part;
    has TokenEncoder $!token-encoder.=new;
    has TokenDecoder $!token-decoder.=new;

    method iterator(--> Iterator) { self.Seq.iterator }

    method Seq(--> Seq:D) {
        my %current-kwargs = %!op-kwargs;
        my %previous-next-token;
        my %next-token = @!input-token Z=> Nil xx *;
        (%next-token, $) = self!parse-starting-token with $!starting-token;
        my $total-items = 0;
        my Bool $first-request = True;
        my $primary-result-key = @!result-keys[0];
        my $starting-truncation = 0;
        self!inject-starting-params: %current-kwargs;
        return gather loop {
            my $response = self!make-request: %current-kwargs;
            my $parsed = self!extract-parsed-response: $response;
            if $first-request {
                # The first request is handled differently.  We could
                # possibly have a resume/starting token that tells us where
                # to index into the retrieved page.
                $starting-truncation = self!handle-first-request: $parsed, $primary-result-key, $starting-truncation
                    with $!starting-token; # todo 3rd parameter not used
                $first-request = False;
                self!record-non-aggregate-key-values: $parsed # todo
            } else {
                $starting-truncation = 0;
            }
            my @current-response = $primary-result-key.search($parsed);
            my $truncate-amount = 0;
            $truncate-amount = ($total-items + @current-response) - $_ with $!max-items;
            if $truncate-amount > 0 {
                self!truncate-response: $parsed, $primary-result-key, $truncate-amount, $starting-truncation,
                        %next-token;
                take $response;
                last;
            } else {
                take $response;
                $total-items += @current-response;
                %next-token = self!get-next-token: $parsed;
                last if %next-token{*}:v.none.defined;
                if $!max-items.defined and $total-items == $!max-items {
                    self.set-resume-token: %next-token;
                    last;
                }
                if %previous-next-token and %previous-next-token eqv %next-token {
                    # todo non-adhoc exception
                    die "The same next token was received twice: {%next-token.gist}";
                }
                self!inject-token-into-kwargs: %current-kwargs, %next-token;
                %previous-next-token = %next-token;
            }
        }
    }

    method !record-non-aggregate-key-values($response) {
        my %non-aggregate-keys;
        for @!non-aggregate-keys ->  {
            my $result = .search: $response;
            set-value-from-jmespath %non-aggregate-keys, .<expression>, $result;
        }
        %!non-aggregate-part = %non-aggregate-keys;
    }

    method set-resume-token(%value) {
        my @token-keys = %value<boto_truncate_amount>:exists
                ?? ['boto_truncate_amount', |@!input-token].sort
                !! @!input-token.sort;
        my @hash-keys = %value.keys.sort;
        if @token-keys eqv @hash-keys {
            $!resume-token = $!token-encoder.encode: %value;
        } else {
            die "Bad starting token: {%value.gist}";
        }
    }

    method !get-next-token($parsed) {
        with $!more-results {
            return {} unless .search: $parsed;
        }
        my %next-tokens = gather for @!output-token Z @!input-token -> ($output-token, $input-token) {
            my $next-token = $output-token.search($parsed);
            take $input-token => $_ with $next-token;
        }
        return %next-tokens;
    }

    method !truncate-response($parsed, $primary-result-key, $truncate-amount, $starting-truncation, %next-token) {
        my @original = $primary-result-key.search($parsed);
        my $amount-to-keep = @original - $truncate-amount;
        my @truncated = @original[0..$amount-to-keep];
        set-value-from-jmespath $parsed, $primary-result-key<expression>, @truncated;
        # The issue here is that even though we know how much we've truncated
        # we need to account for this globally including any starting
        # left truncation. For example:
        # Raw response: [0,1,2,3]
        # Starting index: 1
        # Max items: 1
        # Starting left truncation: [1, 2, 3]
        # End right truncation for max items: [1]
        # However, even though we only kept 1, this is post
        # left truncation so the next starting index should be 2, not 1
        # (left_truncation + amount_to_keep).
        %next-token<boto_truncate_amount> = $amount-to-keep + $starting-truncation;
        self.set-resume-token: %next-token;
    }

    method !handle-first-request(%parsed, $primary-result-key, $?) {
        my ($, $starting-truncation) = self!parse-starting-token;
        my @all-data = $primary-result-key.search(%parsed) // []; # todo implement .search (maybe)
        my @data = (@all-data and @all-data.all ~~ Str) ?? @all-data[$starting-truncation..*] !! ();
        set-value-from-jmespath %parsed, $primary-result-key<expression>, @data;
        for @!result-keys -> $token {
            next if $token<expression> eq $primary-result-key<expression>;
            my $empty-value = do given $token.search(%parsed) {
                when List { [] }
                when Str { '' }
                when Int|Rat { 0 }
                default { 0 }
            }
            set-value-from-jmespath %parsed, $token.expression, $empty-value;
        }
        return $starting-truncation;
    }

    method !extract-parsed-response($response) { $response }

    method !make-request(%current-kwargs) {
        try {
            &!method( |%current-kwargs );
        }
    }

    method build-full-result(--> Hash) {
        my %complete-result;
        for self -> $response {
            my $page = $response;
            $page = $response[1] if $response ~~ Positional and $response.elems == 2;
            for @!result-keys -> $result-expression {
                my $result-value = $result-expression.search: $page;
                next without $result-value;
                my $existing-value = $result-expression.search: %complete-result;
                without $existing-value {
                    set-value-from-jmespath %complete-result, $result-expression<expression>, $result-value;
                    next;
                }
                # todo do something better than querying type
                if $result-value ~~ Positional {
                    push $existing-value, $_ for @$result-value;
                } elsif $result-value ~~ Int|Rat {
                    set-value-from-jmespath %complete-result, $result-expression<expression>, $existing-value + $result-value;
                } elsif $result-value ~~ Str {
                    set-value-from-jmespath %complete-result, $result-expression<expression>, $existing-value ~ $result-value;
                }
            }
        }
        merge-hashes %complete-result, %!non-aggregate-part;
        %complete-result<NextToken> = $_ with $!resume-token;
        return %complete-result;
    }

    method !parse-starting-token {
        return () without $!starting-token;
        my $next-token = $!token-decoder.decode: $!starting-token;
        my $index = 0;
        $index = $_ with $next-token<boto_truncate_token>:delete;
        return $next-token, $index;
    }

    method !inject-starting-params(%op-kwargs) {
        with $!starting-token {
            my (%next-token, $) = self!parse-starting-token;
            self!inject-token-into-kwargs: %op-kwargs, %next-token;
        }
        %op-kwargs{$!limit-key} = $_ with $!page-size;
    }

    method !inject-token-into-kwargs(%op-kwargs, %next-token) {
        %op-kwargs = |%op-kwargs, |%next-token;
    }
}

role Paginator is export {
    has OperationModel $.model;
    has &.method;
    has %.pagination-config;
    has @!output-token;
    has @!input-token;
    has $!more-results; # Jmespath
    has @!non-aggregate-keys;
    has @.result-keys;
    has Str $!limit-key;

    submethod TWEAK {
        @!output-token = get-output-tokens %!pagination-config;
        @!input-token = get-input-tokens %!pagination-config;
        $!more-results = get-more-results-token %!pagination-config;
        @!non-aggregate-keys = get-non-aggregate-keys %!pagination-config;
        @!result-keys = get-result-keys %!pagination-config;
        $!limit-key = %!pagination-config<limit_key> // Nil;
    }

    sub get-output-tokens(%config) {
        my @output = |(%config<output_token> // []);
        map -> $config { Jmespath.compile($config) }, @output;
    }

    sub get-input-tokens(%config) {
        |(%config<input_token> // []);
    }

    sub get-more-results-token(%config) {
        Jmespath.compile($_) with %config<more_results>;
    }

    sub get-non-aggregate-keys(%config) {
        my @keys = |(%config<non_aggregate_keys> // []);
        map -> $key { Jmespath.compile($key) }, @keys;
    }

    sub get-result-keys(%config) {
        my @result-key = |(%config<result_key> // []);
        map -> $key { Jmespath.compile($key) }, @result-key;
    }

    method paginate(*%op-kwargs) {
        my %page-params = self!extract-paging-params: %op-kwargs;
        return PageIterator.new:
                :&!method, :@!input-token,
                :@!output-token, :$!more-results,
                :@!result-keys, :@!non-aggregate-keys,
                :$!limit-key,
                |%page-params,
                :%op-kwargs;
    }

    method !extract-paging-params(%args) {
        my %pagination-config = %args<PaginationConfig>:delete // ();
        my $max-items = %pagination-config<MaxItems> // Nil;
        .=Int with $max-items;
        my $page-size = %pagination-config<PageSize> // Nil;
        with $page-size {
            without $!limit-key {
                die "PageSize parameter is not supported for the "
                            ~"pagination interface for this operation."
            }
            my %input-members = $!model.input-shape.members;
            my $limit-key-shape = %input-members{$!limit-key};
            my $type = $limit-key-shape.type-name eq 'string' ?? Str !! Int;
            $page-size.=$type;
        }
        return %( :$max-items, :starting-token(%pagination-config<StartingToken> // Str), :$page-size );
    }
}
