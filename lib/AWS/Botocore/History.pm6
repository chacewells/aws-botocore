unit module AWS::Botocore::History;

use AWS::Botocore::Logger;

class HistoryRecorder {...}
my HistoryRecorder $HISTORY-RECORDER;
my Logger $LOG.=new: name => $?PACKAGE.^name;

role BaseHistoryHandler is export {
    method emit($event_type, $payload, $source) {!!!}
}

class HistoryRecorder is export {
    has Bool $!enabled = False;
    has BaseHistoryHandler @!handlers;

    method enable(--> Nil) { $!enabled = True }
    method disable(--> Nil) { $!enabled = False }
    method add-handler(BaseHistoryHandler $handler --> Nil) {
        push @!handlers, $handler;
    }
    method record(Str $event_type, Str() $payload, Str $source = 'BOTOCORE' --> Nil) {
        if $!enabled and @!handlers {
            for @!handlers -> $handler {
                try {
                    CATCH { default { $LOG.debug: "Exception raised in $handler" } }
                    $handler.emit: $event_type, $payload, $source;
                }
            }
        }
    }
}

sub get-global-history-recorder(--> HistoryRecorder:D) is export {
    .=new without $HISTORY-RECORDER;
    $HISTORY-RECORDER;
}