unit module AWS::Botocore::Logger;

class Logger is export {
    has $.name is required;
    has Bool $.on is rw is default(True);

    method debug(*@args) {
        "[$!name] @args[]".say if $!on;
    }
}
