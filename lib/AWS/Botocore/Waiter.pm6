unit module AWS::Botocore::Waiter;

sub create_waiter_with_client($waiter_name, $waiter_model, $client) is export {...}

class WaiterModel is export {
    our constant $SUPPORTED_VERSION = 2;

    has %!waiter_config;
    has $.version;
    has @.waiter_names;

    method new(%waiter_config) {
        self.bless: :%waiter_config;
    }

    submethod BUILD(:%waiter_config) {
        %!waiter_config = %waiter_config<waiters>;
        my $version = %!waiter_config<version> orelse 'unknown';
        self!verify_supported_version: $version;
        $!version = $version;
        @!waiter_names = %!waiter_config<waiters>.keys.sort;
    }

    # todo implement
    method !verify_supported_version($version) {...}
}