unit module AWS::Botocore::Credentials;

class ReadOnlyCredentials is export {
    has Str ($.access_key, $.secret_key, $.token)
}

#    no need to "normalize"; Perl 6 has Str declarator
role Credentials is export {
    has Str ($.access_key, $.secret_key, $.token) is rw;
    has Str $.method is rw is default('explicit');

    method get_frozen_credentials(--> ReadOnlyCredentials:D) {
        ReadOnlyCredentials.new: :$!access_key, :$!secret_key, :$!token
    }
}