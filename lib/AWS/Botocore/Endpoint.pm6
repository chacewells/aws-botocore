unit module AWS::Botocore::Endpoint;

use AWS::Botocore::HTTPSession;

my constant $DEFAULT-TIMEOUT = 60;
my constant $MAX-POOL-CONNECTIONS = 10;

class EndpointCreator is export {
    has $!event_emitter;

    method create-endpoint($service-model, $region-name, $endpoint-url, :$verify, :$response-parser-factory,
            :$timeout=$DEFAULT-TIMEOUT, :$max-pool-connections=$MAX-POOL-CONNECTIONS,
            :$http-session-cls=URLLib3Session, :$proxies, :$socket-options, :$client-cert) {...}
}