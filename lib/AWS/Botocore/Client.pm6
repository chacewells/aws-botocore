use v6.d;
unit module AWS::Botocore::Client;

class ClientEndpointBridge is export {
    method resolve(|) {*}
    method construct-endpoint(|) {*}
}
