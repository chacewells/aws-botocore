unit module AWS::Botocore::Signers;

class RequestSigner is export {
    has $!region-name;
    has $!signing-name;
    has $!signature-version;
    has $!credentials;
    has $!service-id;
    has $!event-emitter;
}