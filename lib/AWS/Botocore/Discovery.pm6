unit module AWS::Botocore::Discovery;

class EndpointDiscoveryException is Exception is export {
    has $.message;
}

class EndpointDiscoveryRequired is EndpointDiscoveryException is export {
    method new {
        self.bless: message => 'Endpoint Discovery is not enabled but this operation requires it.';
    }
}

class EndpointDiscoveryRefreshFailed is EndpointDiscoveryException is export {
    method new {
        self.bless: message => 'Endpoint Discovery failed to refresh the required endpoints.'
    }
}

sub block_endpoint_discovery_required_operations($model, *%) is export {
    with $model.endpoint_discovery -> $endpoint_discovery {
        die EndpointDiscoveryRequired.new with $endpoint_discovery<required>;
    }
}

class EndpointDiscoveryModel is export {}

class EndpointDiscoveryManager is export {
    has %!cache;
    has %!failed_attempts;
    has DateTime $!time.=now;
    has $!client;
    has EndpointDiscoveryModel $!model;

    # todo contstruction/initialization

    method !parse_endpoints($response) {...}
    method !cache_item($value) {...}
    method !create_cache_key(*%kwargs) {...}
    method gather_itentifiers($operation, $params) {...}
    method delete_endpoints(*%kwargs) {...}
    method !describe_endpoints(*%kwargs) {...}
    method !get_current_endpoints(*%kwargs) {...}
    method !recently_failed($cache_key) {...}
    method !select_endpoint($endpoints) {...}
    method describe_endpoint(*%kwargs) {...}
}

class EndpointDiscoveryHandler is export {
    has EndpointDiscoveryManager $!manager;

    method register($events, $service_id) {...}
    method gather_identifiers($params, $model, $context, *%) {...}
    method discover_endpoint($request, $operation_name, *%) {...}
    method handle_retries(%request_dict, $response, $operation, *%) {...}
}
