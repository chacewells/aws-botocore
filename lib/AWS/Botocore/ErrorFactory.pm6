unit module AWS::Botocore::ErrorFactory;
use AWS::Botocore::Utils;

class ClientExceptionsFactory is export {
    method create_client_exceptions($service_model) {
        state %client_exceptions_cache;
        unless %client_exceptions_cache{$service_model}:exists {
            %client_exceptions_cache{$service_model} = self!create_client_exceptions: $service_model;
        }
        %client_exceptions_cache{$service_model};
    }

    # todo implement
    method !create_client_exceptions($service_model) {...}
}