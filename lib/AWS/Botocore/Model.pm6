unit module AWS::Botocore::Model;
use AWS::Botocore::Utils;

# todo errors

role ServiceId is export {
    method hyphenize { hyphenize-service-id(self.Str) }
}

class ShapeResolver {...}
class UnresolvableShapeMap {...}

my constant @SERLIALIZED-ATTRS := <locationName queryName flattened location
payload streaming timestampFormat
xmlNamespace resultWrapper xmlAttribute
eventstream event eventheader eventpayload
jsonvalue timestampFormat hostLabel>;
my constant @METADATA-ATTRS := <required min max sensitive enum
idempotencyToken error exception
endpointdiscoveryid>;

class Shape is export {
    #    has $.MAP-TYPE = OrderedDict

    has Str $.name;
    has Str $.type-name;
    has Str $.documentation;
    has %.shape-model;
    has ShapeResolver $.shape-resolver;
    has %.cache;

    has Hash $!serialization;
    has Hash $!metadata;

    submethod BUILD(
            :shape-name($!name),
            :%!shape-model,
            :$!shape-resolver = UnresolvableShapeMap.new
            ) {
        $!type-name = %!shape-model<type>;
        $!documentation = %!shape-model<documentation> // '';
    }

    method serialization {
        without $!serialization {
            $!serialization = %!shape-model{@SERLIALIZED-ATTRS}:p.Hash;
            $!serialization<name> = $_ with $!serialization<locationName>:delete;
        }
        return $!serialization;
    }

    method metadata {
        without $!metadata {
            $!metadata = %!shape-model{@METADATA-ATTRS}:p.Hash;
        }
        return $!metadata;
    }

    method required-members {
        return self.metadata<required> // [];
    }

    # not implemented
    method members {...}

    method resolve-shape-ref(%shape-ref) { # todo type?
        return $!shape-resolver.resolve-shape-ref: %shape-ref;
    }

    method event-stream-name(--> Nil) { }
}

class StructureShape is Shape is export {
    has Hash $.shape-members-cache;
    method members {
        $!shape-members-cache //= $.shape-model<members>.kv.map( -> $name, %shape-ref {
            $name => self.resolve-shape-ref(%shape-ref)
        }).Hash;
    }

    method event-stream-name(--> Str) {
        for self.members.kv -> $member-name, Shape $member {
            return $member-name with $member.serialization<eventstream>;
        }
    }
}

class ListShape is Shape is export {
    has Shape $!member;
    method member {
        $!member = self.resolve-shape-ref: %.shape-model<member>
            without $!member;
        return $!member;
    }
}

class MapShape is Shape is export {
    has $!key;
    has $!value;
    method key(--> Shape) {
        $!key //= self.resolve-shape-ref: %.shape-model<key>;
    }

    method value(--> Shape) {
        $!value //= self.resolve-shape-ref: %.shape-model<value>;
    }
}

class StringShape is Shape is export {
    has Array $!enum;
    method enum {
        $!enum //= self.metadata<enum> // [];
    }
}

class OperationModel {...}

class ServiceModel is export {
    has %.service-description;

    has ShapeResolver $!shape-resolver
            handles (shape-for => 'get-shape-by-name')
            handles 'resolve-shape-ref';
    has Str $!service-name;
    has %.metadata;
    has %!instance-cache;
    has List $!shape-names;
    has List $!operation-names;
    has Str $!documentation;
    has ServiceId $!service-id;
    has Str $!signing-name;
    has Str $!api-version;
    has Str $!protocol;
    has Str $!endpoint-prefix;
    has OperationModel $!endpoint-discovery-operation;
    has Str $!signature-version;

    submethod BUILD(:%!service-description, Str :$!service-name) {}
    submethod TWEAK {
        %!metadata = %!service-description<metadata>;
        $!shape-resolver.=new: shape-map => %!service-description<shapes> // {};
    }

    method shape-names {
        $!shape-names //= (%!service-description<shapes> // {}).keys.List;
    }

    method operation-model(Str $operation-name --> OperationModel:D) {
        without %!instance-cache{$operation-name} {
            my %model = do try {
                CATCH { default { die "Operation not found" } } # todo catch specific exception
                %!service-description<operations>{$operation-name};
            };
            %!instance-cache{$operation-name} = OperationModel.new:
                    :operation-model(%model), :service-model(self), :name($operation-name);
        }
        return %!instance-cache{$operation-name};
    }

    method documentation {
        $!documentation //= %!service-description<documentation> // '';
    }

    method operation-names {
        $!operation-names //= (%!service-description<operations> // {}).keys.List;
    }

    method service-name {
        $!service-name //= self.endpoint-prefix;
    }

    method service-id(--> ServiceId:D) {
        $!service-id //= self!get-metadata-property('serviceId') but ServiceId;
    }

    method signing-name {
        $!signing-name //= %!metadata<signingName> // self.endpoint-prefix;
    }

    method api-version(--> Str:D) {
        $!api-version //= self!get-metadata-property('apiVersion');
    }

    method protocol {
        $!protocol //= self!get-metadata-property('protocol');
    }

    method endpoint-prefix {
        $!endpoint-prefix //= self!get-metadata-property('endpointPrefix');
    }

    method endpoint-discovery-operation(--> OperationModel:D) {
        without $!endpoint-discovery-operation {
            for self.operation-names -> $operation {
                my OperationModel $model = self.operation-model($operation);
                if $model.is-endpoint-discovery-operation {
                    $!endpoint-discovery-operation = $model;
                    last;
                }
            }
        }
        return $!endpoint-discovery-operation;
    }

    method !get-metadata-property($name) {
        try {
            return %!metadata{$name};
            CATCH { default { die "'$name' not defined in the metadata of the model: '{self.gist}'" } }
        }
    }

    multi method signature-version {
        $!signature-version //= %!metadata<signatureVersion>;
    }

    multi method signature-version(Str:D $!signature-version) {}
}

#`«
todo depended on by PageIterator, but without type information
however, using a full class implementation makes testing heavyweight
perhaps strike balance by defining roles to mixin or stub required public methods
»
class OperationModel is export {
    has %!operation-model is required;
    has ServiceModel $.service-model is required;
    has Str $!api-name;
    has Str $.wire-name;
    has Str $.documentation;
    has Bool $.deprecated;
    has Hash $.endpoint-discovery;
    has Bool $.is-endpoint-discovery-operation;
    has Shape $.input-shape;
    has Shape $.output-shape;
    has Array $!error-shapes;
    has Str $.auth-type;
    has %.metadata;
    has %.http;

    submethod BUILD(:%!operation-model, ServiceModel :$!service-model, Str :name($!api-name)) {}

    submethod TWEAK {
        $!wire-name = %!operation-model<name> // Nil;
        %!metadata = .metadata with $!service-model;
        %!http = %$_ with %!operation-model<http>;
        $!documentation = %!operation-model<documentation> // '';
        $!deprecated = %!operation-model<deprecated> // False;
        # Explicit None default. An empty dictionary for this trait means it is
        # enabled but not required to be used.
        $!endpoint-discovery = %!operation-model<endpointdiscovery> // Nil;
        $!is-endpoint-discovery-operation = %!operation-model<endpointoperation> // False;
        $!input-shape = $!service-model.resolve-shape-ref: $_ with %!operation-model<input>;
        $!output-shape = $!service-model.resolve-shape-ref: $_ with %!operation-model<output>;
        $!auth-type = %!operation-model<authtype> // Nil;
    }

    method name {
        $!api-name //= $!wire-name;
    }

    method has-event-stream-input(--> Bool) {
        self.get-event-stream-input.defined;
    }

    method has-event-stream-output(--> Bool) {
        self.get-event-stream-output.defined;
    }

    method get-event-stream-input(--> Shape) {
        get-event-stream $!input-shape;
    }

    method get-event-stream-output(--> Shape) {
        get-event-stream $!output-shape;
    }

    multi get-event-stream(Shape:U --> Nil) {}

    multi get-event-stream(Shape:D $shape --> Shape) {
        with $shape.event-stream-name -> $event-name {
            return $shape.members{$event-name};
        }
        return Nil;
    }

    method has-streaming-input(--> Bool) {
        self.get-streaming-input.defined;
    }

    method has-streaming-output(--> Bool) {
        self.get-streaming-output.defined;
    }

    method get-streaming-input(--> Shape) {
        get-streaming-body $!input-shape;
    }

    method get-streaming-output(--> Shape) {
        get-streaming-body $!output-shape;
    }

    multi get-streaming-body(Shape:U --> Nil) {}

    multi get-streaming-body(Shape:D $shape --> Shape) {
        my $payload = $shape.serialization<payload>;
        with $payload {
            my Shape $payload-shape = $shape.members{$payload};
            return $payload-shape if $payload-shape.type-name eq 'blob';
        }
        return Nil;
    }

    method error-shapes {
        $!error-shapes //= (%!operation-model<errors> // [])
                .map(-> $s { $!service-model.resolve-shape-ref($s) })
                .Array;
    }

}

my constant %SHAPE-CLASSES =
        structure   => StructureShape,
        list        => ListShape,
        map         => MapShape,
        string      => StringShape
;

class ShapeResolver is export {
    has %.shape-map;
    has %!shape-cache;

    method get-shape-by-name(Str:D $shape-name, %member-traits? --> Shape:D) {
        my %shape-model = %!shape-map{$shape-name}; # todo try/catch key not found
        my Shape $shape-class = do try given %shape-model -> (:$type, *%) {
            %SHAPE-CLASSES{$type};
        }

        if %member-traits {
            %shape-model = |%shape-model, |%member-traits;
        }
        $shape-class.new: :$shape-name, :%shape-model, :shape-resolver(self);
    }

    method resolve-shape-ref(% (Str :$shape, *%member-traits) --> Shape:D) {
        return self.get-shape-by-name: $shape, %member-traits;
    }
}

class UnresolvableShapeMap is ShapeResolver is export {
    method get-shape-by-name($shape-name, %member-traits?) {
        die "Attempted to lookup shape '$shape-name', but no shape map was provided";
    }

    method resolve-shape-ref(% (:$shape)) {
        die "Attempted to lookup shape '$shape', but no shape map was provided";
    }
}

class ShapeNameGenerator is export {
    has %!name-cache;
    method new-shape-name(Str:D $type-name --> Str:D) {
        my $current-index = ++%!name-cache{$type-name};
        return "{$type-name.uc}Type{$current-index}";
    }
}

class DenormalizedStructureBuilder is export {
    has %.members;
    has ShapeNameGenerator $!name-generator;
    has Str $.name;

    submethod TWEAK {
        $!name-generator.=new;
        $!name //= $!name-generator.new-shape-name: 'structure';
    }

    method with-members(%!members --> DenormalizedStructureBuilder) { self }

    method build-model(--> StructureShape) {
        my %shapes;
        my %denormalized = :type<structure>, :%!members;
        self!build-model: %denormalized, %shapes, $!name;
        my ShapeResolver $resolver.=new: :shape-map(%shapes);
        return StructureShape.new: :shape-name($!name), :shape-model(%shapes{$!name}), :shape-resolver($resolver);
    }

    method !build-model(%model, %shapes, Str:D $shape-name) {
        my $scalar = any <string integer boolean blob float timestamp long double char>;
        %shapes{$shape-name} = do with %model<type> {
            when 'structure'    { self!build-structure(%model, %shapes) }
            when 'list'         { self!build-list(%model, %shapes) }
            when 'map'          { self!build-map(%model, %shapes) }
            when $scalar        { self!build-scalar(%model) }
            default             { # todo die with InvalidShapeError exception
                die "InvalidShapeError: Unknown shape type: '$_'" }
        }
    }

    method !build-structure(%model, %shapes) {
        my %members;
        my %shape = self!build-initial-shape: %model;
        %shape<members> = %members;

        for %model<members>.kv -> $name, %member-model {
            my $member-shape-name = self!get-shape-name: %member-model;
            %members{$name} = { shape => $member-shape-name };
            self!build-model: %member-model, %shapes, $member-shape-name;
        }
        return %shape;
    }

    method !build-list(%model, %shapes) {
        my $member-shape-name = self!get-shape-name: %model;
        my %shape = self!build-initial-shape: %model;
        %shape<member> = {shape => $member-shape-name};
        self!build-model: %model<member>, %shapes, $member-shape-name;
        return %shape;
    }

    method !build-map(%model, %shapes) {
        my $key-shape-name = self!get-shape-name: %model<key>;
        my $value-shape-name = self!get-shape-name: %model<value>;
        my %shape = self!build-initial-shape: %model;
        %shape<key> = {shape => $key-shape-name};
        %shape<value> = {shape => $value-shape-name};
        self!build-model: %model<key>, %shapes, $key-shape-name;
        self!build-model: %model<value>, %shapes, $value-shape-name;
        return %shape;
    }

    method !build-initial-shape(%model) {
        my %shape = %model<type>:kv;
        %shape<documentation> = $_ with %model<documentation>;
        for @METADATA-ATTRS -> $attr { %shape{$attr} = $_ with %model{$attr} }
        return %shape;
    }

    method !build-scalar(%model) {
        return self!build-initial-shape: %model;
    }

    method !get-shape-name(%model) {
        with %model<shape-name> { $_ }
        else {
            $!name-generator.new-shape-name: %model<type>;
        }
    }
}
