unit module AWS::Botocore::Translate;

class Translate is export {
    # todo implement
    method build_retry_config($endpoint_prefix, $retry_model, $definitions, :$client_retry_config) is export {...}

    # todo implement
    method merge_client_retry_config($retry_config, $client_retry_config) {...}

    method resolve_references($config, $definitions) is export {...}
}
