unit module AWS::Botocore::Utils;

use URI::Encode;
use URL;

use AWS::Botocore::Exceptions;
use AWS::Botocore::BaseEventHooks;
use AWS::Botocore::Logger;

my constant @S3-ACCELERATE-WHITELIST is export = 'dualstack';

#`« for now, implement these and only these:
botocore.utils.CachedProperty (maybe make a trait)
X botocore.utils.get_service_module_name
X botocore.utils.S3RegionRedirector
X botocore.utils.S3ArnParamHandler
X botocore.utils.S3EndpointSetter
»

my regex label-re { ^ <[a..z 0..9]> <[a..z 0..9 \-]>* <[a..z 0..9]> $ }

my Logger $LOG.=new: name => $?PACKAGE.^name;
our sub LOG(--> Logger) { $LOG }

subset NameableModel of Any where {.can('metadata') and .can('service-name')};
=head get_service_module_name
Returns the module name for a service
This is the value used in both the documentation and client class name
=cut

sub get-service-module-name(NameableModel $service-model) is export {
    my $name = do given $service-model.metadata {
        .<serviceAbbreviation> // .<serviceFullName> // $service-model.service-name;
    }
    $name.subst: :g, / Amazon || AWS || \W+ /, '';
}

sub switch_host_s3_accelerate($request, $operation_name, *%) is export {...}

sub switch_to_virtual_host_style($request, $signature_version, $default_endpoint_url?, *%) is export {...}

sub fix_s3_host($requeset, $signature_version, $region_name, $default_endpoint_url?, *%) is export {...}

sub ensure-boolean($val) is export {...}

proto percent-encode-sequence(|) is export {*}
#`«
Urlencode a dict or list into a string.

This is similar to urllib.urlencode except that:

* It uses quote, and not quote_plus
* It has a default list of safe chars that don't need
  to be encoded, which matches what AWS services expect.

If any value in the input ``mapping`` is a list type,
then each list element wil be serialized.  This is the equivalent
to ``urlencode``'s ``doseq=True`` argument.

This function should be preferred over the stdlib
``urlencode()`` function.

:param mapping: Either a dict to urlencode or a list of
    ``(key, value)`` pairs.
»
multi percent-encode-sequence(%mapping) { samewith(@%mapping) }
multi percent-encode-sequence(@mapping) {
    join '&', gather map &encode-kv, @mapping;
}
multi percent-encode-sequence('') { '' }

multi encode-kv(Str $key, $value where Str|Iterable) {
    my $encoded-key = uri_encode_component $key;
    take $encoded-key ~ '=' ~ .&uri_encode_component for @$value;
}

multi encode-kv(Pair (:$key, :$value) ) {
    samewith $key, $value;
}

multi encode-kv(Iterable ($key, $value) ) {
    samewith $key, $value;
}

sub hyphenize-service-id(Str $service-id) is export {
    $service-id.lc.subst: ' ', '-', :global;
}

role Stringible is export(:Stringible) {
    method Str { self.gist }
}

class S3RegionRedirector is export {
    has $!endpoint-resolver; # has method .resolve:($, $, $, Bool $)
    has %!cache;
    has $!client; # dynamically generated in ClientCreator
    # has meta { has events }

    submethod BUILD(:$!endpoint-resolver, :%!cache, :$!client) {}

    method register(BaseEventHooks $event-emitter?) {
        with $event-emitter // $!client.meta.events {
            .register: 'needs-retry.s3', $?CLASS.^lookup('redirect-from-error').assuming(self);
            .register: 'before-call.s3', $?CLASS.^lookup('set-request-url').assuming(self);
            .register: 'before-parameter-build.s3', $?CLASS.^lookup('redirect-from-cache').assuming(self);
        }
    }

    method redirect-from-error(%request, @response, $operation, *%args) {
        return unless @response or @response.any.defined;
        if self!is-s3-accesspoint( %request<context> // {} ) {
            $LOG.debug: 'S3 request was previously redirected, not redirecting.';
            return;
        }

        if %request<context><s3_redirected> {
            $LOG.debug: 'S3 request was previously redirected, not redirecting.';
            return;
        }

        my %error = @response[1]<Error> // {};
        my $error-code = %error<Code> // Nil but Stringible;
        my %response-metadata = @response[1]<ResponseMetadata> // %();

        my $is-special-head-object = ($error-code eq <301 400>.any and $operation.name eq 'HeadObject');
        my $is-special-head-bucket = ($error-code eq <301 400>.any
                and $operation.name eq 'HeadBucket'
                and %response-metadata<HTTPHeaders><x-amz-bucket-region>:exists);
        my $is-wrong-signing-region = ($error-code eq 'AuthorizationHeaderMalformed' and %error<Region>:exists);
        my $is-redirect-status = so do .status-code == 301|302|307 with @response[0];
        my $is-permanent-redirect = so $error-code eq 'PermanentRedirect';
        return if none $is-special-head-object, $is-wrong-signing-region, $is-permanent-redirect,
                $is-special-head-bucket, $is-redirect-status;

        my $bucket = %request<context><signing><bucket>;
        my $client-region = %request<context><client_region> // Nil;
        my $new-region = self.get-bucket-region: $bucket, @response;

        without $new-region {
            $LOG.debug: ("S3 client configured for region %s but the bucket %s is not "
                ~"in that region and the proper region could not be "
                ~"automatically determined.").sprintf: |map *.gist, $client-region, $bucket;
                return;
        }

        $LOG.debug: ("S3 client configured for region %s but the bucket %s is in region"
            ~" %s; Please configure the proper region to avoid multiple "
            ~"unnecessary redirects and signing attempts.").sprintf: |map *.gist, $client-region, $bucket, $new-region;
        my %endpoint = $!endpoint-resolver.resolve: 's3', $new-region;
        my $endpoint = %endpoint<endpoint_url>;

        my %signing-context = :region($new-region), :$bucket, :$endpoint;
        %request<context><signing> = %signing-context;

        %!cache{$bucket} = %signing-context;
        :%request.gist.say;
        self.set-request-url: %request, %request<context>;

        %request<context><s3_redirected> = True;

        return 0; # 0 means don't wait to retry, apparently? need to learn the contract
    }

    method get-bucket-region($bucket, @response) {
        my %service-response = @response[1];
        my %response-headers = %service-response<ResponseMetadata><HTTPHeaders>;
        return $_ with %response-headers<x-amz-bucket-region>;

        return $_ with %service-response<Error><Region>;

        my %headers;
        try {
            my %response = $!client.head-bucket: Bucket => $bucket;
            %headers = %response<ResponseMetadata><HTTPHeaders>;
            CATCH { # todo definite error
                default { %headers = .<ResponseMetadata><HTTPHeaders> }
            }
        }

        %headers<x-amz-bucket-region> // Nil;
    }

    method set-request-url(%params, %context, *%) {
        with %context<signing><endpoint> -> $endpoint {
            %params<url> = get-new-endpoint %params<url>, $endpoint, False;
        }
    }

    #`«
    This handler retrieves a given bucket's signing context from the cache
    and adds it into the request context.
    »
    method redirect-from-cache(%params, %context, *%) {
        return if self!is-s3-accesspoint: %context;
        my $bucket = %params<Bucket> // Nil;
        %context<signing> = %!cache{$bucket} // {:$bucket};
    }

    method !is-s3-accesspoint(%context) { %context<s3_accesspoint>:exists }
}

role ArnParser is export(:ArnParser) {
    method parse-arn(Str:D $arn) {
        my @arn-parts = $arn.split(':', 6);
        if @arn-parts < 6 { # todo definited exception (InvalidArnException)
            die "Provided ARN: $arn must be of the format: arn:partition:service:region:account:resource"
        }
        Hash.new: <partition service region account resource> Z=> @arn-parts[1..*];
    }
}

class S3ArnParamHandler is export {
    my regex accesspoint-resource { ^ accesspoint <[/:]> $<resource_name> = [.+] $ }
    my constant @BLACKLISTED-OPERATIONS = <CreateBucket>;

    has ArnParser $.arn-parser;

    method register($event-emitter) { # todo type this as a hook
        $event-emitter.register: 'before-parameter-build.s3', $?CLASS.^lookup('handle-arn').assuming(self);
    }

    # model likely OperationModel
    method handle-arn(%params, $model, %context, *%) {
        return if $model.name ∈ @BLACKLISTED-OPERATIONS;
        my %arn-details = self!get-arn-details-from-bucket-param(%params) // {};
        return unless %arn-details;
        self!store-accesspoint: %params, %context, %arn-details
            if %arn-details<resource_type> eq 'accesspoint';
    }

    method !get-arn-details-from-bucket-param(%params) {
        try with %params<Bucket> -> $arn {
            my %arn-details = $!arn-parser.parse-arn: $arn;
            self!add-resource-type-and-arn: $arn, %arn-details;
            return %arn-details;
        }
        Nil;
    }

    method !add-resource-type-and-arn($arn, %arn-details) {
        if %arn-details<resource> ~~ &accesspoint-resource {
            %arn-details<resource_type resource_name> = 'accesspoint', ~$<resource_name>;
        } else {
            die "UnsupportedS3ArnError: $arn"; # todo definite error (UnsupportedS3ArnError)
        }
    }

    method !store-accesspoint(%params, %context, %arn-details) {
        %params<Bucket> = %arn-details<resource_name>;
        %context<s3_accesspoint> = {
            name => %arn-details.<resource_name>,
            |(%arn-details.<account partition region>:p)
        };
    }
}

class S3EndpointSetter is export {
    my constant $DEFAULT-PARTITION = 'aws';
    my constant $DEFAULT-DNS-SUFFIX = 'amazonaws.com';

    has $!endpoint-resolver; # has method .resolve:($, $, $, Bool $)
    has $!region;
    has %!s3-config;
    has $!endpoint-url;
    has Str $!partition;

    submethod BUILD(:$!endpoint-resolver, :$!region, :%!s3-config, :$!endpoint-url, Str :$!partition) {}

    submethod TWEAK {
        $!partition //= $DEFAULT-PARTITION;
    }

    method register(BaseEventHooks $event-emitter) {
        $event-emitter.register: 'before-sign.s3', $?CLASS.^lookup('set-endpoint').assuming(self);
    }

    method set-endpoint($request, *%args) {
        if self!use-access-endpoint: $request {
            self!validate-accesspoint-supported: $request;
            my $region-name = self!resolve-region-for-accesspoint-endpoint: $request;
            self!switch-to-accesspoint-endpoint: $request, $region-name;
            return;
        }
        switch-host-s3-accelerate $request, |%args
                if self!use-accelerate-endpoint;
        .(:$request, |%args) with self!s3-addressing-handler;
    }

    method !use-access-endpoint($request) { $request.context<s3_accesspoint>:exists }

    method !validate-accesspoint-supported($request) {
        with $!endpoint-url { # todo definite error
            die UnsupportedS3AccesspointConfigurationError.new: message => ~<Client cannot use a custom "endpoint-url"
                when specifying an access-point ARN>;
        }
        if self!use-accelerate-endpoint { # todo definite error
            die UnsupportedS3AccesspointConfigurationError.new: message => ~<Client does not support s3 accelerate
                configuration when an access-point ARN is specified>;
        }
        my $request-partition = $request.context<s3_accesspoint><partition>;
        if $request-partition ne $!partition { # todo definite error
            die UnsupportedS3AccesspointConfigurationError.new: message => "Client is configured for"
                    ~ " {:$!partition.gist} partition, but access-point ARN provided is for '$request-partition'"
                    ~ " partition. The client and access-point partition must be the same."
        }
    }

    method !resolve-region-for-accesspoint-endpoint($request) {
        if %!s3-config<use_arn_region> // True {
            my $accesspoint-region = $request.context<s3_accesspoint><region>;
            # If we are using the region from the access point,
            # we will also want to make sure that we set it as the
            # signing region as well
            override-signing-region $request, $accesspoint-region;
            return $accesspoint-region;
        }
        return $!region;
    }

    method !switch-to-accesspoint-endpoint($request, $region-name --> Nil) {
        my $accesspoint-endpoint = ~ URL.new(
                scheme      => .scheme,
                hostname    => self!get-accesspoint-hostname($request.context, $region-name),
                path        => get-accesspoint-path(.path, $request.context),
                query       => .query
            ) with URL.new: $request.url;
        $LOG.debug: "Updating URI from ", $request.url, " to ", $accesspoint-endpoint;
        $request.url = $accesspoint-endpoint;
    }

    method !get-accesspoint-hostname(%request-context, $region-name) {
        my %s3-accesspoint = %request-context<s3_accesspoint>;
        my @accesspoint-hostname-components = %s3-accesspoint<name account>.join('-'), 's3-accesspoint';
        push @accesspoint-hostname-components, 'dualstack' if %!s3-config<use_dualstack_endpoint>;
        push @accesspoint-hostname-components, $region-name, self!get-dns-suffix: $region-name;
        return join '.', @accesspoint-hostname-components;
    }

    my sub get-accesspoint-path(@path is copy, %request-context) {
        # The Bucket parameter was substituted with the access-point name as
        # some value was required in serializing the bucket name. Now that
        # we are making the request directly to the access point, we will
        # want to remove that access-point name from the path.
        my $name = %request-context<s3_accesspoint><name>;
        splice @path, $_, 1 with @path.first($name, :k);
        return @path;
    }

    method !get-dns-suffix($region-name) {
        my %resolved = $!endpoint-resolver.construct-endpoint('s3', $region-name) // {};
        return $_ with %resolved<dnsSuffix>;
        return $DEFAULT-DNS-SUFFIX;
    }

    method !use-accelerate-endpoint(--> Bool) { # todo cached property
        # Enable accelerate if the configuration is set to to true or the
        # endpoint being used matches one of the accelerate endpoints.

        # Accelerate has been explicitly configured.
        return True if %!s3-config<use_accelerate_endpoint>;

        # Accelerate mode is turned on automatically if an endpoint url is
        # provided that matches the accelerate scheme.
        return False without $!endpoint-url;

        # Accelerate is only valid for Amazon endpoints.
        my $hostname = URL.new($!endpoint-url).hostname;
        return False unless $hostname.ends-with: 'amazonaws.com';

        # The first part of the url should always be s3-accelerate.
        my @parts = split '.', $hostname;
        return False unless @parts[0] eq 's3-accelerate';

        # Url parts between 's3-accelerate' and 'amazonaws.com' which
        # represent different url features.
        my @feature-parts = @parts[1..*-2];

        # There should be no duplicate url parts.
        return False unless @feature-parts == set @feature-parts;

        # Remaining parts must all be in the whitelist.
        return so all(@feature-parts) ∈ @S3-ACCELERATE-WHITELIST;
    }

    method !s3-addressing-handler() {
        my $addressing-style = self!addressing-style; # not caching properties (yet), so store here
        # If virtual host style was configured, use it regardless of whether
        # or not the bucket looks dns compatible.
        if $addressing-style eq 'virtual' {
            $LOG.debug: 'Using S3 virtual host style addressing.';
            return &switch-to-virtual-host-style;
        }

        if $addressing-style eq 'path' or $!endpoint-url.defined {
            $LOG.debug: 'Using S3 path style addressing.';
            return Nil;
        }

        $LOG.debug: 'Defaulting to S3 virtual host style addressing with path style addressing fallback';

        return &fix-s3-host;
    }

    method !addressing-style {
        # Use virtual host style addressing if accelerate is enabled or if
        # the given endpoint url is an accelerate endpoint.
        return 'virtual' if self!use-accelerate-endpoint;
        return %!s3-config<addressing_style> // Nil but Stringible;
    }

    my sub override-signing-region($request, $region-name) {
        # S3SigV4Auth will use the context['signing']['region'] value to
        # sign with if present. This is used by the Bucket redirector
        # as well but we should be fine because the redirector is never
        # used in combination with the accesspoint setting logic.
        $request.context<signing> = {:region($region-name)};
    }
}

#`«
This handler looks at S3 requests just before they are signed.
    If there is a bucket name on the path (true for everything except
    ListAllBuckets) it checks to see if that bucket name conforms to
    the DNS naming conventions.  If it does, it alters the request to
    use ``virtual hosting`` style addressing rather than ``path-style``
    addressing.
»
# todo unit tests
sub fix-s3-host(:$request, :$signature-version, :region-name($), :$default-endpoint-url is copy, *%args) is export {
    $default-endpoint-url = 's3.amazonaws.com' if $request.context<use_global_endpoint>;
    try {
        switch-to-virtual-host-style :$request, :$signature-version, :$default-endpoint-url;
        CATCH {
            when InvalidDNSNameError {
                $LOG.debug: "Not changing URI, bucket is not DNS compatible: {.bucket-name}";
            }
        }
    }
}

sub switch-to-virtual-host-style(:$request, :signature-version($), :$default-endpoint-url is copy, *%) is export {
    with $request.auth-path {
        # The auth_path has already been applied (this may be a
        # retried request).  We don't need to perform this
        # customization again.
        return;
    } elsif is-get-bucket-location-request $request {
        # For the GetBucketLocation response, we should not be using
        # the virtual host style addressing so we can avoid any sigv4
        # issues.
        $LOG.debug: "Request is GetBucketLocation operation, not checking for DNS compatibility.";
        return;
    }
    my URL $parts.=new: $request.url;
    $request.auth-path = '/' ~ $parts.Str(:path);
    my @path-parts = $parts.path;

    $default-endpoint-url //= $parts.hostname;

    if @path-parts {
        my $bucket-name = $parts.path[0];
        return unless $bucket-name; # if bucket name empty, we shouldn't check for DNS compatibility
        $LOG.debug: "Checking for DNS compatible bucket for {$request.url}";
        if check-dns-name $bucket-name {
            $request.auth-path ~= '/' if @path-parts == 1 and $request.auth-path.ends-with('/').not;

            @path-parts.=grep: none $bucket-name;
            my $global-endpoint = $default-endpoint-url;
            my $host = "$bucket-name.$global-endpoint";
            my $new-uri = ~URL.new(scheme => $parts.scheme, :hostname($host), :path(@path-parts), :query($parts.query));
            $request.url = $new-uri;
            $LOG.debug: "URI updated to $new-uri";
        } else { # todo definite InvalidDNSNameError
            die InvalidDNSNameError.new: :$bucket-name;
        }
    }
}

#`«
    Check to see if the ``bucket_name`` complies with the
    restricted DNS naming conventions necessary to allow
    access via virtual-hosting style.

    Even though "." characters are perfectly valid in this DNS
    naming scheme, we are going to punt on any name containing a
    "." character because these will cause SSL cert validation
    problems if we try to use virtual-hosting style addressing.
»
sub check-dns-name(Str:D $_ --> Bool) {
    .contains('.').not && (3 ≤ .chars ≤ 63) && ($_ ~~ &label-re).so
}

my sub is-get-bucket-location-request($request) { $request.url.ends-with: '?location' }

sub switch-host-s3-accelerate($request, :$operation-name, *%) is export {
    # Switches the current s3 endpoint with an S3 Accelerate endpoint

    # Note that when registered the switching of the s3 host happens
    # before it gets changed to virtual. So we are not concerned with ensuring
    # that the bucket name is translated to the virtual style here and we
    # can hard code the Accelerate endpoint.
    my @parts = URL.new($request.url).hostname.split('.').grep(* ∈ @S3-ACCELERATE-WHITELIST);
    my $endpoint = 'https://s3-accelerate.';
    $endpoint ~= @parts.join('.') ~ '.' if @parts;
    $endpoint ~= 'amazonaws.com';

    return if $operation-name eq any <ListBuckets CreateBuckets DeleteBucket>;
    switch-hosts $request, $endpoint, False;
}

my sub switch-hosts($request, $new-endpoint, Bool $use-new-scheme? = True) {
    my $final-endpoint = get-new-endpoint $request.url, $new-endpoint, $use-new-scheme;
    $request.url = $final-endpoint;
}

sub get-new-endpoint($original-endpoint, $new-endpoint, $use-new-scheme? = True) {
    my URL $new-endpoint-components.=new: $new-endpoint;
    my URL $original-endpoint-components.=new: $original-endpoint;
    my $scheme = $use-new-scheme
            ?? $new-endpoint-components.scheme
            !! $original-endpoint-components.scheme;
    my URL $final-endpoint-components.=new: :$scheme, hostname => $new-endpoint-components.hostname,
            path => $original-endpoint-components.path,
            query => $original-endpoint-components.query;
    my $final-endpoint = ~$final-endpoint-components;
    $LOG.debug: 'Updating URI from %s to %s'.sprintf: $original-endpoint, $final-endpoint;
    return $final-endpoint;
}

sub validate-jmespath-for-set(Str:D $expression) is export {
    # todo raise definite error
    die "InvalidExpressionError: {:$expression.gist}" if $expression eq '.'|'['|']'|'*';
}

sub set-value-from-jmespath(%source, $expression, $value, $is-first? = True) is export {
    validate-jmespath-for-set $expression if $is-first;

    my ($current-key, $remainder) is default('') = split '.', $expression, 1;

    die "InvalidExpressionError: {:$expression.gist}" unless $current-key;

    if $remainder {
        %source{$current-key} = {} unless $current-key ∈ %source;
        return set-value-from-jmespath %source{$current-key}, $remainder, $value, False;
    }
    %source{$current-key} = $value;
}

#`«Given two dict, merge the second dict into the first.
    The dicts can have arbitrary nesting.
    :param append_lists: If true, instead of clobbering a list with the new
        value, append all of the new values onto the original list.
»
sub merge-hashes(%a, %b, Bool $append-lists? = False) is export {
    for %b -> $key, $value {
        if $value ~~ Hash {
            if $key ∈ %a {
                merge-hashes %a{$key}, $value;
            } else {
                %a{$key} = $value;
            }
        } elsif $value ~~ Array and $append-lists {
            if $key ∈ %a and %a{$key} ~~ Array {
                %a{$key}.append: @$value;
            } else {
                %a{$key} = $value;
            }
        } else {
            %a{$key} = $value;
        }
    }
}
