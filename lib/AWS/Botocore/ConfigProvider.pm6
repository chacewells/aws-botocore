unit module AWS::Botocore::ConfigProvider;

class ConfigValueStore is export {
    has %!overrides;
    has %!mapping;

    submethod BUILD(:%!overrides, :%!mapping) {}

    method get-config-variable($logical-name) {
        return $_ with %!overrides{$logical-name};
        with %!mapping{$logical-name} -> $provider {
            return $provider.provide;
        } else {
            return Nil;
        }
    }

    method set-config-variable($logical-name, $value) {
        %!overrides{$logical-name} = $value;
    }

    method clear-config-variable($logical-name) {
        %!overrides{$logical-name}:delete;
    }

    method set-config-provider($logical-name, $provider) {
        %!mapping{$logical-name} = $provider;
    }

}