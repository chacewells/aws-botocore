unit module AWS::Botocore::RetryHandler;

use AWS::Botocore::Logger;

my Logger $LOG.=new: name => $?PACKAGE.^name;

class BaseChecker {...}

class RetryHandler does Callable is export {
    has BaseChecker $!checker;
    has &!action where { $_ ~~ :(:$attempts) };

    method CALL-ME($attempts, $response, $caught_exception, *%) {
        if self.$!checker($attempts, $response, $caught_exception) {
            my $result = &!action(:$attempts);
            $LOG.debug: "Retry needed, action of $result";
            return $result;
        }
        $LOG.debug: "No retry needed";
    }
}

class RetryHandlerFactory is export {
    # todo implement
    method create_retry_handler(%config, :$operation_name) {
        return RetryHandler.new: BaseChecker.new;
    }
}

class BaseChecker is Method is export {
    # todo implement
    method CALL-ME($attempt_number, $response, $caught_exceptions) {...}
}