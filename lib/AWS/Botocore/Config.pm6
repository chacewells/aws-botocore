unit module AWS::Botocore::Config;

class Config is export {
    has %!data;
    # todo emulate Pythonish config from boto3
    method new(*%data) {
        self.bless: :%data;
    }
}