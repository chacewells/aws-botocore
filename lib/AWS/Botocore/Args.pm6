unit module AWS::Botocore::Args;

use AWS::Botocore::BaseEventHooks;
use AWS::Botocore::Config;
use AWS::Botocore::Endpoint;
use AWS::Botocore::Parsers;
use AWS::Botocore::Serialize;
use AWS::Botocore::Signers;
use AWS::Botocore::Utils;
use AWS::Botocore::Logger;

enum Sockopt is export <IPPROTO_TCP TCP_NODELAY SOL_SOCKET SO_KEEPALIVE>;

my Logger $LOG.=new: name => $?PACKAGE.^name;

my constant @VALID_STS_REGIONAL_ENDPOINTS_CONFIG = <
    legacy
    regional
>;

my constant @LEGACY-GLOBAL-STS-REGIONS = <
    ap-northeast-1
    ap-south-1
    ap-southeast-1
    ap-southeast-2
    aws-global
    ca-central-1
    eu-central-1
    eu-north-1
    eu-west-1
    eu-west-2
    eu-west-3
    sa-east-1
    us-east-1
    us-east-2
    us-west-1
    us-west-2
>;

class ClientArgsCreator is export {
    has BaseEventHooks $!event-emitter;
    has $!user-agent;
    has $!response-parser-factory;
    has $!loader;
    has $!exceptions-factory;
    has $!config-store;

    submethod BUILD(BaseEventHooks :$!event-emitter, :$!user-agent, :$!response-parser-factory, :$!loader,
            :$!exceptions-factory, :$!config-store) {}

    method get-client-args(:$service-model, :$region-name, :$is-secure, :$endpoint-url, :$verify, :$credentials,
            :$scoped-config, :$client-config, :$endpoint-bridge) {
        my %final-args = self.compute-client-args: :$service-model, :$client-config, :$endpoint-bridge, :$region-name,
                :$endpoint-url, :$is-secure, :$scoped-config;

        my (:$service-name, :$parameter-validation, :%endpoint-config, :$protocol, :%config, :%s3-config, :$partition,
                :$socket-options, *%) := %final-args;

        my ($signing-region, $endpoint-region-name) = %endpoint-config<signing-region endpoint-region-name>;
        without $signing-region|$endpoint-region-name {
            ($signing-region, $endpoint-region-name) = get-default-s3-region $service-name, $endpoint-bridge;
            %config<region-name> = $endpoint-region-name;
        }

        my $event-emitter = $!event-emitter.copy; # todo determine clone method
#        todo figure out wtf I was doing here
        my RequestSigner $request-signer.=new: service-id => $service-model.service-id, %endpoint-config<signing-name>:p,
                %endpoint-config<signature-version>:p, region-name => $signing-region, :$credentials, :$event-emitter;

        %config<s3> = %s3-config;
        my Config $new-config.=new: |%config;
        my EndpointCreator $endpoint-creator.=new: :$event-emitter;

        my $endpoint = $endpoint-creator.create-endpoint: :$service-model, region-name => $endpoint-region-name,
                endpoint-url => %endpoint-config<endpoint-url>, :$verify, :$!response-parser-factory,
                %config<max-pool-connections>:p, proxies => $new-config.proxies,
                :timeout($new-config.connect-timeout, $new-config.read-timeout), :$socket-options,
                client-cert => $new-config.client-cert;

        my $serializer = create-serializer $protocol, $parameter-validation;
        my $response-parser = create-parser $protocol;
        return { :$serializer, :$endpoint, :$response-parser, :$event-emitter, :$request-signer, :$service-model,
                    :$!loader, client-config => $new-config, :$partition, :$!exceptions-factory };
    }

    my sub get-default-s3-region($service-name, $endpoint-bridge) {
        return $endpoint-bridge.resolve('s3')<signing-region region-name>
                if $service-name eq 's3';
        return Nil, Nil;
    }
}