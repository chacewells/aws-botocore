unit module AWS::Botocore::BaseEventHooks;

role BaseEventHooks is export {
    method emit($event-name, *%args) {...}
    method register($event-name, &handler:(*%), :$unique-id = Nil, Bool :$unique-id-uses-count = False) {...}
    method register-first($event-name, &handler:(*%), :$unique-id, Bool :$unique-id-uses-count = False) {...}
    method register-last($event-name, &handler:(*%), :$unique-id, Bool :$unique-id-uses-count = False) {...}
    method unregister($event-name, &handler:(*%), :$unique-id, Bool :$unique-id-uses-count = False) {...}
    method copy {...}
}
