unit module AWS::Botocore::Loaders;

class Loader is export {
    method load_service_model($service_name, $type_name, :$api_version) {}
}

