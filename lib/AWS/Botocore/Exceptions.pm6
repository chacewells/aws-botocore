unit module AWS::Botocore::Exceptions;

class InvalidDNSNameError is Exception is export {
    has $.bucket-name;
    has Str $!message;

    submethod BUILD(:$!bucket-name, Str :$!message) {}

    method message(--> Str:D) {
        $!message // "{$?CLASS.^name}: {:$!bucket-name.gist}";
    }
}

class UnsupportedS3AccesspointConfigurationError is Exception is export {
    has Str $.message;
}
