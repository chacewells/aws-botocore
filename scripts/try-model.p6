#!/usr/bin/env perl6
use lib 'lib';
use AWS::Botocore::Model;

my %service_description = (
    operations => {
        foo => 'bar',
        baz => 'bang'
    },
    metadata => {:what<metacrazy>, :who<dere>},
        shapes => {:wonder<bread>, :bags<money>}
);

my $service_name = 'barbituate';

my $service_model = ServiceModel.new: %service_description, $service_name;

$service_model.operation_names».say;
$service_model.metadata.say;
$service_model.shape_resolver.say;
