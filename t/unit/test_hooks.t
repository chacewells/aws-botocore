use lib <lib>;
use Test;
use AWS::Botocore::Hooks;

subtest 'Test NodeList' => {
    use AWS::Botocore::Hooks :NodeList;

    my NodeList $node-list;
    my $unwrap = ENTER &subtest.wrap: sub (|) {
        $node-list.=new;
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    subtest 'Test at-pos' => {
        plan 4;
        push $node-list[0], 5;
        push $node-list[1], 10;
        push $node-list[2], 15;
        is $node-list[0], [5], 'Should be [5]';
        is $node-list[1], [10], 'Should be [10]';
        is $node-list[2], [15], 'Should be [15]';

        $node-list[0] = ^100;
        is $node-list[0], [^100], 'Should be 0..99';
    }

    subtest 'Test at-pos bad' => {
        plan 1;
        try {
            $node-list[3];
            flunk "Should blow up";
            CATCH { default { pass "Should blow up" } }
        }
    }

    subtest 'Test exists-pos' => {
        plan 4;
        ok $node-list[0]:exists, '0 exists';
        ok $node-list[1]:exists, '1 exists';
        ok $node-list[2]:exists, '2 exists';
        nok $node-list[3]:exists, "3 doesn't exist";
    }

    subtest 'Test elems' => {
        plan 1;
        is $node-list.elems, 3, 'always has 3 elems';
    }

    subtest 'Test copy with push' => {
        plan 4;
        $node-list[0] = ^11;
        $node-list[1] = ^21;
        $node-list[2] = ^31;

        my $copy = $node-list.copy;
        is $copy[0], $node-list[0], "First of copy is same";
        is $copy[1], $node-list[1], "Middle of copy is same";
        is $copy[2], $node-list[2], "Last of copy is same";

        push $copy[0], 50;
        isnt $copy[0], $node-list[0], "First isn't same anymore";
    }

    subtest 'Test copy with update' => {
        plan 1;
        $node-list[0] = ^11;
        $node-list[1] = ^21;
        $node-list[2] = ^31;
        my $copy = $node-list.copy;
        $copy[0][1]++;
        isnt $copy[0], $node-list, "First isn't same anymore";
    }

    subtest 'Test copy with ref type' => {
        plan 1;
        my class A { has $.a is rw };
        push $node-list[0], A.new(:5a);
        my $copy = $node-list.copy;
        $copy[0][0].a++;
        is $copy[0][0].a, $node-list[0][0].a, "Field update changed both";
    }

    done-testing;
}

subtest 'Test PrefixTrie' => {
    use AWS::Botocore::Hooks :Trie;

    my PrefixTrie $trie;

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        with $trie.=new {
            .append-item: 'foo.bar', 'buckets';
            .append-item: 'foo.bar', 'whiffles', :0section;
            .append-item: 'foo.bar', 'waffles', :0section;
            .append-item: 'foo.bar.baz', 'bazzle';
            .append-item: 'foo.bar.baz', 'handball', :2section;
        }
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    subtest 'test append' => {
        plan 2;
        is $trie.prefix-search('foo.bar.baz'), <bazzle handball whiffles waffles buckets>, 'Should return what was put in';
        is $trie.prefix-search('foo.bar'), <whiffles waffles buckets>, 'Should just get shallow items';
    }

    subtest 'Test remove existing item deepest default list' => {
        plan 1;
        $trie.remove-item: 'foo.bar.baz', 'bazzle';
        is $trie.prefix-search('foo.bar.baz'), <handball whiffles waffles buckets>, "Shouldn't contain 'bazzle'";
    }

    subtest 'Test remove existing item deepest last list' => {
        plan 1;
        $trie.remove-item: 'foo.bar.baz', 'handball';
        is $trie.prefix-search('foo.bar.baz'), <bazzle whiffles waffles buckets>, "Shouldn't contain 'handball'";
    }

    subtest 'Test remove existing item shallow default list' => {
        plan 1;
        $trie.remove-item: 'foo.bar', 'buckets';
        is $trie.prefix-search('foo.bar'), <whiffles waffles>, "Shouldn't contain 'buckets'";
    }

    subtest 'Test remove existing item shallow first list' => {
        plan 1;
        $trie.remove-item: 'foo.bar', 'whiffles';
        is $trie.prefix-search('foo.bar'), < waffles buckets>, "Shouldn't contain 'whiffles'";
    }

    done-testing;
}

subtest 'Test HierarchicalEventEmitter' => {
    my HierarchicalEmitter $emitter;
    my @hook-calls;

    my sub hook (*%args) {
        push @hook-calls, %args;
        %args;
    }

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        @hook-calls[]:delete;
        $emitter.=new;
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    subtest 'Test non-dot behavior' => {
        plan 1;
        $emitter.register: 'no-dot', &hook;
        $emitter.emit: 'no-dot';
        is @hook-calls.elems, 1, 'Should have called no-dot hook';
    }

    subtest 'Test with dots' => {
        plan 1;
        $emitter.register: 'foo.bar.baz', &hook;
        $emitter.emit: 'foo.bar.baz';
        is @hook-calls.elems, 1, 'Should have called dotted hook';
    }

    subtest 'Test catch-all hook' => {
        plan 2;
        $emitter.register: 'foo', &hook;
        $emitter.register: 'foo.bar', &hook;
        $emitter.register: 'foo.bar.baz', &hook;
        $emitter.emit: 'foo.bar.baz';
        is @hook-calls.elems, 3, 'Should have called hook 3 times';
        is-deeply @hook-calls, [{:event-name<foo.bar.baz>} xx 3], 'Should have called with same event-name';
    }

    subtest 'Test hook called in proper order' => {
        plan 1;
        my @calls;
        $emitter.register: 'foo', -> *% { push @calls, 'foo' };
        $emitter.register: 'foo.bar', -> *% { push @calls, 'foo.bar' };
        $emitter.register: 'foo.bar.baz', -> *% { push @calls, 'foo.bar.baz' };
        $emitter.emit: 'foo.bar.baz';
        is @calls, <foo.bar.baz foo.bar foo>, 'Should have called shallow hook first';
    }

    done-testing;
}

subtest 'Test AliasedEmitter' => {
    my @hook-calls;
    my sub hook(*%args) { push @hook-calls, %args }

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        @hook-calls[]:delete;
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    my sub get-emitter(%event-aliases --> EventAliaser:D) {
        my HierarchicalEmitter $emitter.=new;
        EventAliaser.new: :$emitter, :%event-aliases;
    }

    subtest 'Test event emitted' => {
        plan 1;
        my %aliases = :bar<bear>;
        my EventAliaser $emitter = get-emitter %aliases;
        $emitter.register: 'foo.bear.baz', &hook;
        $emitter.emit: 'foo.bear.baz';
        my @calls = @hook-calls».<event-name>;
        is-deeply @calls, [<foo.bear.baz>], "Should have triggered handler";
    }

    subtest 'test-aliased-event-emitted' => {
        plan 1;
        my %aliases = :bar<bear>;
        my EventAliaser $emitter = get-emitter %aliases;
        $emitter.register: 'foo.bear.baz', &hook;
        $emitter.emit: 'foo.bar.baz';
        my @calls = @hook-calls».<event-name>;
        is-deeply @calls, [<foo.bear.baz>], "Should have triggered handler from alias";
    }

    subtest 'Test alias with dots emitted' => {
        plan 1;
        my %aliases = 'api.bar' => 'bear';
        my EventAliaser $emitter = get-emitter %aliases;
        $emitter.register: 'foo.bear.baz', &hook;
        $emitter.emit: 'foo.api.bar.baz';
        my @calls = @hook-calls».<event-name>;
        is-deeply @calls, [<foo.bear.baz>], "Should have triggered handler from alias";
    }

    subtest 'Test aliased event registered' => {
        plan 1;
        my %aliases = :bar<bear>;
        my EventAliaser $emitter = get-emitter %aliases;
        $emitter.register: 'foo.bar.baz', &hook;
        $emitter.emit: 'foo.bear.baz';
        my @calls = @hook-calls».<event-name>;
        is-deeply @calls, [<foo.bear.baz>], "Should have triggered event";
    }

    subtest 'Test aliased event with dots registered' => {
        plan 1;
        my %aliases = 'api.bar' => 'bear';
        my EventAliaser $emitter = get-emitter %aliases;
        $emitter.register: 'foo.api.bar.baz', &hook;
        $emitter.emit: 'foo.bear.baz';
        my @calls = @hook-calls».<event-name>;
        is-deeply @calls, [<foo.bear.baz>], "Should have triggered event";
    }

    subtest 'Test event unregistered' => {
        plan 2;
        my %aliases = :bar<bear>;
        my EventAliaser $emitter = get-emitter %aliases;

        $emitter.register: 'foo.bar.baz', &hook;
        $emitter.emit: 'foo.bear.baz';
        my @calls = @hook-calls».<event-name>;
        is-deeply @calls, [<foo.bear.baz>], "Should have triggered event";

        @hook-calls[]:delete;
        $emitter.unregister: 'foo.bear.baz', &hook;
        $emitter.emit: 'foo.bear.baz';
        is @hook-calls.elems, 0, "Should trigger no events";
    }

    subtest 'Test aliased event unregistered' => {
        plan 2;
        my %aliases = :bar<bear>;
        my EventAliaser $emitter = get-emitter %aliases;

        $emitter.register: 'foo.bar.baz', &hook;
        $emitter.emit: 'foo.bear.baz';
        my @calls = @hook-calls».<event-name>;
        is-deeply @calls, [<foo.bear.baz>], "Should triggered event from alias";

        @hook-calls[]:delete;
        $emitter.unregister: 'foo.bear.baz', &hook;
        $emitter.emit: 'foo.bear.baz';
        is @hook-calls.elems, 0, "Should trigger no events from alias";
    }

    subtest 'Test aliased event with dots unregistered' => {
        plan 2;
        my %aliases = 'api.bar' => 'bear';
        my EventAliaser $emitter = get-emitter %aliases;

        $emitter.register: 'foo.api.bar.baz', &hook;
        $emitter.emit: 'foo.bear.baz';
        my @calls = @hook-calls».<event-name>;
        is-deeply @calls, [<foo.bear.baz>], "Should trigger event from alias";

        @hook-calls[]:delete;
        $emitter.unregister: 'foo.api.bar.baz', &hook;
        $emitter.emit: 'foo.bear.baz';
        is @hook-calls.elems, 0, "Should trigger no events from alias";
    }

    done-testing;
}

subtest 'Test stop processing' => {
    my HierarchicalEmitter $emitter;
    my @hook-calls;

    my sub hook1(*% --> Nil) { push @hook-calls, 'hook1' };
    my sub hook2(*%) { push @hook-calls, 'hook2'; 'hook2-response' };
    my sub hook3(*%) { push @hook-calls, 'hook3'; 'hook3-response' };

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        @hook-calls[]:delete;
        $emitter.=new;
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    subtest 'Test all hooks' => {
        plan 1;
        $emitter.register: 'foo', &hook1;
        $emitter.register: 'foo', &hook2;
        $emitter.register: 'foo', &hook3;
        $emitter.emit: 'foo';
        is-deeply @hook-calls, [<hook1 hook2 hook3>], "Should have called hooks in order";
    }

    subtest 'Test stop processing after first response' => {
        plan 2;
        $emitter.register: 'foo', &hook1;
        $emitter.register: 'foo', &hook2;
        $emitter.register: 'foo', &hook3;
        my (&handler, $response) = $emitter.emit-until-response: 'foo';

        is $response, 'hook2-response', "Should be the hook2 response";
        is-deeply @hook-calls, [<hook1 hook2>], "Should have triggered 1 and 2 (not 3)";
    }

    subtest 'Test no responses' => {
        plan 2;
        # Here we register a handler that will not return a response
        # and ensure we get back proper values.
        $emitter.register: 'foo', &hook1;
        my @responses = $emitter.emit: 'foo';
        is-deeply @hook-calls, [<hook1>], "Should have triggered hook1";
        is-deeply @responses, [$(&hook1, Any)], "Should not have response value";
    }

    subtest 'Test no handlers' => {
        plan 2;
        # Here we have no handlers, but still expect a tuple of return
        # values.
        my (&handler, $response) = $emitter.emit-until-response: 'foo';
        nok &handler.defined, "&handler shouldn't be defined";
        nok $response.defined, Q[$response shouldn't be defined];
    }

    done-testing;
}

subtest 'Test first non-none response' => {
    subtest 'Test all none' => {
        plan 1;
        nok first-non-none-response([]).defined, "Shouldn't be defined";
    }

    subtest 'Test first non none' => {
        plan 1;
        my $correct-value = 'correct value';
        my $wrong-value = 'wrong value';
        my @responses = $(Nil, Nil), $(Nil, $correct-value), $(Nil, $wrong-value);
        is first-non-none-response(@responses), $correct-value, "Should retrieve first value";
    }

    subtest 'Test default value if non-none found' => {
        plan 1;
        my @responses = $(Nil, Nil), $(Nil, Nil);
        is first-non-none-response(@responses, :default<notfound>), 'notfound', "Should be 'notfound'";
    }

    subtest 'Test response with one element blows up' => {
        plan 1;
        my @responses = $(Nil, Nil), $[1];
        try {
            first-non-none-response @responses;
            flunk "One element Array should die";
            CATCH { default { pass "One element Array should die" } }
        }
    }

    subtest 'Test response with three elements blows up' => {
        plan 1;
        my @responses = $(Nil, Nil), (1..3);
        try {
            first-non-none-response @responses;
            flunk "Three element range should die";
            CATCH { default { pass "Three element range should die" } }
        }
    }

    done-testing;
}

subtest 'Test wildcard handlers' => {
    my HierarchicalEmitter $emitter;
    my @hook-calls;
    my sub hook(*%args) { push @hook-calls, %args };

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        @hook-calls[]:delete;
        $emitter.=new;
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    sub assert-hook-is-called-given-event(Str $event) {
        my $starting = @hook-calls.elems;
        $emitter.emit: $event;
        my $after = @hook-calls.elems;
        flunk "Handler was not called for event: $event" unless $after > $starting;
        is @hook-calls[*-1]<event-name>, $event, "event-name should be '$event'";
    }

    sub assert-hook-is-not-called-given-event(Str $event) {
        my $starting = @hook-calls.elems;
        $emitter.emit: $event;
        my $after = @hook-calls.elems;
        is $after, $starting, "Handler should not be called for event: $event, last_event: @hook-calls[]";
    }

    subtest 'Test one level wildcard handler' => {
        plan 7;
        given $emitter {
            .register: 'foo.*.baz', &hook;
            .register: 'other.bar.baz', &hook;
            .register: 'qqq.baz', &hook;
            .register: 'dont.call.me', &hook;
            .register: 'dont', &hook;
        }
        # these calls should trigger our hook
        assert-hook-is-called-given-event 'foo.bar.baz';
        assert-hook-is-called-given-event 'foo.qux.baz';
        assert-hook-is-called-given-event 'foo.anything.baz';

        # these calls should not match our hook
        assert-hook-is-not-called-given-event 'foo';
        assert-hook-is-not-called-given-event 'foo.bar';
        assert-hook-is-not-called-given-event 'bar.qux.baz';
        assert-hook-is-not-called-given-event 'foo-bar';
    }

    subtest 'Test hierarchical wildcard handler' => {
        plan 5;
        $emitter.register: 'foo.*.baz', &hook;
        assert-hook-is-called-given-event 'foo.bar.baz.qux';
        assert-hook-is-called-given-event 'foo.bar.baz.qux.foo';
        assert-hook-is-called-given-event 'foo.qux.baz.qux';
        assert-hook-is-called-given-event 'foo.qux.baz.qux.foo';

        assert-hook-is-not-called-given-event 'bar.qux.baz.foo';
    }

    subtest 'Test multiple wildcard events' => {
        plan 10;
        $emitter.register: 'foo.*.*.baz', &hook;
        assert-hook-is-called-given-event 'foo.bar.baz.baz';
        assert-hook-is-called-given-event 'foo.ANY.THING.baz';
        assert-hook-is-called-given-event 'foo.AT.ALL.baz';

        # more specific than registered
        assert-hook-is-called-given-event 'foo.bar.baz.baz.extra';
        assert-hook-is-called-given-event 'foo.bar.baz.baz.extra.stuff';

        # too short
        assert-hook-is-not-called-given-event 'foo';
        assert-hook-is-not-called-given-event 'foo.bar';
        assert-hook-is-not-called-given-event 'foo.bar.baz';

        # bad ending segment
        assert-hook-is-not-called-given-event 'foo.ANY.THING.notbaz';
        assert-hook-is-not-called-given-event 'foo.ANY.THING.stillnotbaz';
    }

    subtest 'Test can unregister for wildcard events' => {
        plan 8;
        $emitter.register: 'foo.*.*.baz', &hook;
        # call multiple times to verify caching behavior
        assert-hook-is-called-given-event 'foo.bar.baz.baz';
        assert-hook-is-called-given-event 'foo.bar.baz.baz';
        assert-hook-is-called-given-event 'foo.bar.baz.baz';

        $emitter.unregister: 'foo.*.*.baz', &hook;
        assert-hook-is-not-called-given-event 'foo.bar.baz.baz';
        assert-hook-is-not-called-given-event 'foo.bar.baz.baz';
        assert-hook-is-not-called-given-event 'foo.bar.baz.baz';

        $emitter.register: 'foo.*.*.baz', &hook;
        assert-hook-is-called-given-event 'foo.bar.baz.baz';
        assert-hook-is-called-given-event 'foo.bar.baz.baz';
    }

    subtest 'Test unregister does not exist' => {
        plan 1;
        $emitter.register: 'foo.*.*.baz', &hook;
        $emitter.unregister: 'foo.*.*.baz', &hook;
        $emitter.unregister: 'foo.*.*.baz', &hook;
        assert-hook-is-not-called-given-event 'foo.bar.baz.baz';
    }

    subtest 'Test cache cleared properly' => {
        plan 5;
        $emitter.register: 'foo.*.*.baz', &hook;
        assert-hook-is-called-given-event 'foo.bar.baz.baz';

        $emitter.register: 'foo.*.*.bar', &hook;
        assert-hook-is-called-given-event 'foo.bar.baz.baz';
        assert-hook-is-called-given-event 'foo.bar.baz.bar';

        $emitter.unregister: 'foo.*.*.baz', &hook;
        assert-hook-is-called-given-event 'foo.bar.baz.bar';
        assert-hook-is-not-called-given-event 'foo.bar.baz.baz';
    }

    subtest 'Test complicated register/unregister' => {
        plan 4;
        given $emitter {
            .register: 'foo.bar.baz.qux', &hook;
            .register: 'foo.bar.baz', &hook;
            .register: 'foo.bar', &hook;
            .register: 'foo', &hook;
            .unregister: 'foo.bar.baz', &hook;
            .unregister: 'foo', &hook;
            .unregister: 'foo.bar', &hook;
        }

        assert-hook-is-called-given-event 'foo.bar.baz.qux';

        assert-hook-is-not-called-given-event 'foo.bar.baz';
        assert-hook-is-not-called-given-event 'foo.bar';
        assert-hook-is-not-called-given-event 'foo';
    }

    subtest 'Test register multiple handlers for same event' => {
        plan 1;
        $emitter.register: 'foo.bar.baz', &hook;
        $emitter.register: 'foo.bar.baz', &hook;

        $emitter.emit: 'foo.bar.baz';
        is @hook-calls.elems, 2, 'Should have called hook twice';
    }

    subtest 'Test register with unique id' => {
        plan 2;
        $emitter.register: 'foo.bar.baz', &hook, :unique-id<foo>;
        $emitter.register: 'foo.bar.baz', &hook, :unique-id<foo>;
        $emitter.register: 'foo.other', &hook, :unique-id<foo>;

        $emitter.emit: 'foo.bar.baz';
        is @hook-calls.elems, 1, "Should call hook once";

        @hook-calls[]:delete;

        $emitter.emit: 'foo.other';
        is @hook-calls.elems, 0, "Shouldn't have called hook under other event";
    }

    subtest 'Test remove handler with unique id' => {
        plan 4;
        my sub hook2(*%args) { push @hook-calls, %args }
        $emitter.register: 'foo.bar.baz', &hook, :unique-id<foo>;
        $emitter.register: 'foo.bar.baz', &hook2;
        $emitter.emit: 'foo.bar.baz';
        is @hook-calls.elems, 2, "Should have called 2 hooks";

        @hook-calls[]:delete;

        $emitter.unregister: 'foo.bar.baz', &hook2;
        $emitter.emit: 'foo.bar.baz';
        is @hook-calls.elems, 1, "Should now trigger only 1 hook";

        @hook-calls[]:delete;

        $emitter.unregister: 'foo.bar.baz', :unique-id<foo>;
        $emitter.emit: 'foo.bar.baz';
        is @hook-calls.elems, 0, "Should fire no hooks";

        try {
            CATCH { flunk "Should not blow up" }
            $emitter.unregister: 'foo.bar.baz', :unique-id<foo>;
            pass "Should not blow up";
        }
    }

    subtest 'Test remove handler with and without unique id' => {
        plan 2;
        $emitter.register: 'foo.bar.baz', &hook, :unique-id<foo>;
        $emitter.register: 'foo.bar.baz', &hook;

        $emitter.unregister: 'foo.bar.baz', &hook;
        $emitter.emit: 'foo.bar.baz';
        is @hook-calls.elems, 1, "Should have called hook once";

        @hook-calls[]:delete;

        $emitter.unregister: 'foo.bar.baz', &hook;
        $emitter.emit: 'foo.bar.baz';
        is @hook-calls.elems, 0, "No more hook calls";
    }

    subtest 'Test register with uses count initially' => {
        plan 1;
        $emitter.register: 'foo', &hook, :unique-id<foo>, :unique-id-uses-count;
        # Subsequent calls must set ``unique_id_uses_count`` to True.
        try {
            $emitter.register: 'foo', &hook, :unique-id<foo>;
            flunk "Should die";
            CATCH { default { pass "Should die" } }
        }
    }

    subtest 'Test register with uses count not initially' => {
        plan 1;
        $emitter.register: 'foo', &hook, :unique-id<foo>;
        # Subsequent calls must set ``unique_id_uses_count`` to False.
        try {
            $emitter.register: 'foo', &hook, :unique-id<foo>, :unique-id-uses-count;
            flunk "Should have died";
            CATCH { default { pass "Died" } }
        }
    }

    subtest 'Test register with uses count unregister' => {
        plan 4;
        $emitter.register: 'foo', &hook, :unique-id<foo>, :unique-id-uses-count;
        $emitter.register: 'foo', &hook, :unique-id<foo>, :unique-id-uses-count;
        # Event was registered to use a count so it must be specified
        # that a count is used when unregistering
        try {
            $emitter.unregister: 'foo', &hook, :unique-id<foo>;
            flunk "Should have died";
            CATCH { default { pass "Died" } }
        }
        $emitter.emit: 'foo';
        is @hook-calls.elems, 1, "Should have called hook once";
        $emitter.unregister: 'foo', &hook, :unique-id<foo>, :unique-id-uses-count;
        @hook-calls[]:delete;
        $emitter.emit: 'foo';
        is @hook-calls.elems, 1, "Should still call hook";
        $emitter.unregister: 'foo', &hook, :unique-id<foo>, :unique-id-uses-count;
        # now the event should be unregistered
        @hook-calls[]:delete;
        $emitter.emit: 'foo';
        is @hook-calls.elems, 0, "Should not have triggered hook";
    }

    subtest 'Test register with no uses count unregister' => {
        plan 1;
        $emitter.register: 'foo', &hook, :unique-id<foo>;
        try {
            $emitter.unregister: 'foo', &hook, :unique-id<foo>, :unique-id-uses-count;
            flunk "Should have died";
            CATCH { default { pass "Died" } }
        }
    }

    subtest 'Test handlers called in order' => {
        plan 1;
        my sub handler($call-number, *%args) { push @hook-calls, { :$call-number, |%args } }

        $emitter.register: 'foo', &handler.assuming(1);
        $emitter.register: 'foo', &handler.assuming(2);
        $emitter.emit: 'foo';
        is-deeply @hook-calls».<call-number>.Array, [1, 2], "Should have called in order";
    }

    subtest 'Test handler call order with hierarchy' => {
        plan 1;
        my sub handler($call-number, *%args) { push @hook-calls, { :$call-number, |%args } }

        $emitter.register: 'foo.bar.baz', &handler.assuming(1);
        $emitter.register: 'foo.bar', &handler.assuming(3);
        $emitter.register: 'foo', &handler.assuming(5);
        $emitter.register: 'foo.bar.baz', &handler.assuming(2);
        $emitter.register: 'foo.bar', &handler.assuming(4);
        $emitter.register: 'foo', &handler.assuming(6);
        $emitter.emit: 'foo.bar.baz';

        is-deeply @hook-calls».<call-number>.Array, [1..6], "Should call in order";
    }

    subtest 'Test register first single level' => {
        plan 1;
        my sub handler($call-number, *%args) { push @hook-calls, { :$call-number, |%args } }

        $emitter.register: 'foo', &handler.assuming(3);
        $emitter.register: 'foo', &handler.assuming(4);
        $emitter.register-first: 'foo', &handler.assuming(1);
        $emitter.register-first: 'foo', &handler.assuming(2);
        $emitter.register: 'foo', &handler.assuming(5);
        $emitter.emit: 'foo';

        is-deeply @hook-calls».<call-number>.Array, [1..5], "Should call in order";
    }

    subtest 'test register first hierarchy' => {
        plan 1;
        my sub handler($call-number, *%args) { push @hook-calls, { :$call-number, |%args } }

        $emitter.register: 'foo', &handler.assuming(5);
        $emitter.register: 'foo.bar', &handler.assuming(2);

        $emitter.register-first: 'foo', &handler.assuming(4);
        $emitter.register-first: 'foo.bar', &handler.assuming(1);

        $emitter.register: 'foo', &handler.assuming(6);
        $emitter.register: 'foo.bar', &handler.assuming(3);

        $emitter.emit: 'foo.bar';
        is-deeply @hook-calls».<call-number>.Array, [1..6], "Should call in order";
    }

    subtest 'Test register last hierarchy' => {
        plan 1;
        my sub handler($call-number, *%args) { push @hook-calls, { :$call-number, |%args } }

        $emitter.register-last: 'foo', &handler.assuming(3);
        $emitter.register: 'foo', &handler.assuming(2);
        $emitter.register-first: 'foo', &handler.assuming(1);
        $emitter.emit: 'foo';
        is-deeply @hook-calls».<call-number>.Array, [1,2,3], "Should call in order";
    }

    subtest 'Test register unregister first last' => {
        plan 1;
        $emitter.register: 'foo', &hook;
        $emitter.register-last: 'foo.bar', &hook;
        $emitter.register-first: 'foo.bar.baz', &hook;

        $emitter.unregister: 'foo.bar.baz', &hook;
        $emitter.unregister: 'foo.bar', &hook;
        $emitter.unregister: 'foo', &hook;

        $emitter.emit: 'foo';
        is @hook-calls.elems, 0, "Shouldn't call any hooks";
    }

    subtest 'Test copy emitter' => {
        plan 7;
        my (@first, @second);
        my sub first-handler(:$id-name, *%) { push @first, $id-name }
        my sub second-handler(:$id-name, *%) { push @second, $id-name }
        $emitter.register: 'foo.bar.baz', &first-handler;
        $emitter.emit: 'foo.bar.baz', :id-name<first-time>;
        is-deeply @first, ['first-time'], "id-name logged to @first";
        is-deeply @second, [], "nothing logged to @second";

        my HierarchicalEmitter $copied-emitter = $emitter.copy;
        # If we emit from the copied emitter, we should still
        # only see the first handler called.
        $copied-emitter.emit: 'foo.bar.baz', :id-name<second-time>;
        is-deeply @first, [<first-time second-time>], "Should have called first handler again";
        is-deeply @second, [], "Should not have called second handler";

        # However, if we register an event handler with the copied
        # emitter, the first emitter will not see this.
        $copied-emitter.register: 'foo.bar.baz', &second-handler;

        $copied-emitter.emit: 'foo.bar.baz', :id-name<third-time>;
        is-deeply @first, [<first-time second-time third-time>], "Should call first handler";
        is-deeply @second, ['third-time'], "Should call second handler";

        # And vice-versa, emitting from the original emitter
        # will not trigger the second_handler.
        # We'll double check this by unregistering/re-registering
        # the event handler.
        $emitter.unregister: 'foo.bar.baz', &first-handler;
        $emitter.register: 'foo.bar.baz', &first-handler;
        $emitter.emit: 'foo.bar.baz', :id-name<last-time>;
        is-deeply @second, ['third-time'], "No additional events to @second";
    }

    subtest 'Test copy emitter with unique id event' => {
        plan 6;
        my (@first, @second);
        my sub first-handler(:$id-name, *%) { push @first, $id-name }
        my sub second-handler(:$id-name, *%) { push @second, $id-name }

        $emitter.register: 'foo', &first-handler, :unique-id<bar>;
        $emitter.emit: 'foo', :id-name<first-time>;
        is-deeply @first, ['first-time'], "&first-handler should be triggered";
        is-deeply @second, [], "&second-handler not yet triggered";

        my HierarchicalEmitter $copied-emitter = $emitter.copy;

        # If we register an event handler with the copied
        # emitter, the event should not get registered again
        # because the unique id was already used.
        $copied-emitter.register: 'foo', &second-handler, :unique-id<bar>;
        $copied-emitter.emit: 'foo', :id-name<second-time>;
        is-deeply @first, [<first-time second-time>], Q[Should have triggered first event];
        is-deeply @second, [], "Should not trigger second (because of unique-id)";

        # If we unregister the first event from the copied emitter,
        # We should be able to register the second handler.
        $copied-emitter.unregister: 'foo', &first-handler, :unique-id<bar>;
        $copied-emitter.register: 'foo', &second-handler, :unique-id<bar>;
        $copied-emitter.emit: 'foo', :id-name<third-time>;
        is-deeply @first, [<first-time second-time>], "No more events for &first-handler";
        is-deeply @second, ['third-time'], "Event fires for &second-handler";
    }

    done-testing;
}

done-testing;
