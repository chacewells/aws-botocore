use lib 'lib';
use Test;
use Test::Mock;

use AWS::Botocore;
use AWS::Botocore::Client;
use AWS::Botocore::Exceptions;
use AWS::Botocore::Hooks;
use AWS::Botocore::Model;
use AWS::Botocore::Request;
use AWS::Botocore::Utils :ArnParser, :DEFAULT;

AWS::Botocore::Utils::LOG.on = False;

subtest 'Test xform-name' => {
    subtest 'Test upper camel case' => {
        plan 2;
        is xform-name('UpperCamelCase'), 'upper_camel_case', "camel case to _";
        is xform-name('UpperCamelCase', '-'), 'upper-camel-case', "camel case to -";
    }

    subtest 'Test lower camel case' => {
        plan 2;
        is xform-name('lowerCamelCase'), 'lower_camel_case', "lower camel case to _";
        is xform-name('lowerCamelCase', '-'), 'lower-camel-case', "lower camel case to -";
    }

    subtest 'test consecutive upper case' => {
        plan 2;
        is xform-name('HTTPHeaders'), 'http_headers', "should break out consecutive upper case";
        is xform-name('HTTPHeaders', '-'), 'http-headers', "should break out consecutive upper case";
    }

    subtest 'Test consecutive upper case middle string' => {
        plan 2;
        is xform-name('MainHTTPHeaders'), 'main_http_headers', "Should break out";
        is xform-name('MainHTTPHeaders', '-'), 'main-http-headers', "Should break out";
    }

    subtest 'Test s3 prefix' => {
        plan 1;
        is xform-name('S3BucketName'), 's3_bucket_name', "Should handle S3 prefix";
    }

    subtest 'Test already snake case' => {
        plan 3;
        is xform-name('leave_alone'), 'leave_alone', "Already snake case";
        is xform-name('s3_bucket_name'), 's3_bucket_name', "Already snake case";
        is xform-name('bucket_s3_name'), 'bucket_s3_name', "Already snake case";
    }

    subtest 'Test special cases' => {
        plan 6;
        is xform-name('SwapEnvironmentCNAMEs'), 'swap_environment_cnames', "Handle special case";
        is xform-name('SwapEnvironmentCNAMEs', '-'), 'swap-environment-cnames', "Handle special case";
        is xform-name('CreateCachediSCSIVolume', '-'), 'create-cached-iscsi-volume', "Handle special case";
        is xform-name('DescribeCachediSCSIVolumes', '-'), 'describe-cached-iscsi-volumes', "Handle special case";
        is xform-name('DescribeStorediSCSIVolumes', '-'), 'describe-stored-iscsi-volumes', "Handle special case";
        is xform-name('CreateStorediSCSIVolume', '-'), 'create-stored-iscsi-volume', "Handle special case";
    }

    subtest 'Test special case ends with s' => {
        plan 1;
        is xform-name('GatewayARNs', '-'), 'gateway-arns', "Handle special case ends with 's'";
    }

    subtest 'Test partial rename' => {
        plan 2;
        is xform-name('IPV6'), 'ipv6', "Should just be lc'd";
        is xform-name('IPV6', '_'), 'ipv6', "Should just be lc'd";
    }

    subtest 'Test s3 partial rename' => {
        plan 2;
        is xform-name('s3Resources', '-'), 's3-resources', "Should be '-' separated";
        is xform-name('s3Resources', '_'), 's3_resources', "Should be '_' separated";
    }

    done-testing;
}

subtest 'Test get-service-module-name' => {
    my %service-description;
    my ServiceModel $service-model;

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        %service-description =
            :metadata{
                :serviceFullName('AWS MyService'),
                :apiVersion<2014-01-01>,
                :signatureVersion<v4>,
                :protocol<query>
            },
            :operations{},
            :shapes{}
        ;
        $service-model.=new: :%service-description, :service-name<myservice>;
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    subtest 'Test default' => {
        plan 1;
        is get-service-module-name($service-model), 'MyService', "Service name should be MyService";
    }

    subtest 'Test client name with amazon' => {
        plan 1;
        $service-model.metadata<serviceFullName> = 'Amazon MyService';
        is get-service-module-name($service-model), 'MyService', "Service name should be MyService";
    }

    subtest 'Test client name using abbreviation' => {
        plan 1;
        $service-model.metadata<serviceAbbreviation> = 'Abbreviation';
        is get-service-module-name($service-model), 'Abbreviation', "Service name should be Abbreviation";
    }

    subtest 'Test client name with non-alphabet characters' => {
        plan 1;
        $service-model.metadata<serviceFullName> = 'Amazon My-Service';
        is get-service-module-name($service-model), 'MyService', "Service name should be MyService";
    }

    subtest 'Test client name with no full name or abbreviation' => {
        plan 1;
        $service-model.metadata<serviceFullName>:delete;
        is get-service-module-name($service-model), 'myservice', "Service name should be myservice";
    }

    done-testing;
}

subtest 'Test percent-encode-sequence' => {
    subtest 'Test percent encode empty' => {
        plan 1;
        is percent-encode-sequence({}), '', "Empty hash => ''";
    }

    subtest 'Test percent encode special chars' => {
        plan 1;
        is percent-encode-sequence({ k1 => 'with spaces++/' }), 'k1=with%20spaces%2B%2B%2F', "Should URI encode";
    }

    subtest 'Test percent encode string string tuples' => {
        plan 1;
        is percent-encode-sequence([<k1 v1>, <k2 v2>]), 'k1=v1&k2=v2', "Tuples as k=v&k=v";
    }

    subtest 'Test percent encode pair array' => {
        plan 1;
        is percent-encode-sequence([:k1<v1>, :k2<v2>]), 'k1=v1&k2=v2', "Pair array as k=v&k=v";
    }

    subtest 'Test percent encode three tuple' => {
        plan 1;
        try {
            percent-encode-sequence [$(<one two three>)];
            flunk "Three-tuple should die";
            CATCH { default { pass "Three-tuple should die" } }
        }
    }

    subtest 'Test percent encode hash string string' => {
        plan 1;
        my %data = :k1<v1>, :k2<v2>;
        my $possible-matches = /'k1=v1&k2=v2'/ | /'k2=v2&k1=v1'/;
        ok percent-encode-sequence(%data) ~~ $possible-matches, "Should be like k1=v1&k2=v2";
    }

    subtest 'Test percent encode single list of values' => {
        plan 1;
        is percent-encode-sequence({k1 => ['a', 'b', 'c']}), 'k1=a&k1=b&k1=c', "Should pair key with each value";
    }

    subtest 'Test percent encode list values of string' => {
        plan 1;
        is percent-encode-sequence([
            ('k1', $['a', 'list']),
            ('k2', $['another', 'list'])
        ]),
        'k1=a&k1=list&k2=another&k2=list',
        "Should pair each key with each of its values";
    }

    done-testing;
}

subtest 'Test S3RegionRedirector' => {
    my S3RegionRedirector $redirector;
    my ($endpoint-resolver, $client, %cache, $operation);
    my @head-bucket-calls; # capture for client.head-bucket

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        %cache{}:delete;
        $endpoint-resolver = mocked(ClientEndpointBridge, returning => {
            resolve => { :endpoint_url<https://eu-central-1.amazonaws.com> }
        });
        $client = set-client-response-headers;
        $redirector.=new: :$endpoint-resolver, :$client;
        $operation = class { has $.name is rw = 'foo' }.new;
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    my sub set-client-response-headers(%headers?) {
        @head-bucket-calls[]:delete;
        # todo client error response
        my %success-response = :ResponseMetadata{:HTTPHeaders(%headers)};
        return class {
            method head-bucket(|args) {
                push @head-bucket-calls, args;
                return {}, %success-response;
            }
        }.new;
    }

    subtest 'Test set-request-url' => {
        plan 1;
        my %params = :url<https://us-west-2.amazonaws.com/foo>;
        my %context = :signing{:endpoint<https://eu-central-1.amazonaws.com>};
        $redirector.set-request-url: %params, %context;
        is %params<url>, 'https://eu-central-1.amazonaws.com/foo', "Should match eu-central-1 URL";
    }

    subtest 'Test only changes request url if endpoint present' => {
        plan 1;
        my %params = :url<https://us-west-2.amazonaws.com/foo>;
        $redirector.set-request-url: %params, %;
        is %params<url>, 'https://us-west-2.amazonaws.com/foo', "Should match original";
    }

    subtest 'Test set-request-url keeps old scheme' => {
        plan 1;
        my %params = :url<http://us-west-2.amazonaws.com/foo>;
        my %context = :signing{:endpoint<https://eu-central-1.amazonaws.com>}
        $redirector.set-request-url: %params, %context;
        is %params<url>, 'http://eu-central-1.amazonaws.com/foo', "Should keep old scheme";
    }

    subtest 'Test sets signing context from cache' => {
        plan 1;
        my %signing-context = :endpoint<bar>;
        %cache<foo> = %signing-context;
        $redirector.=new: :$endpoint-resolver, :$client, :%cache;
        my %params = :Bucket<foo>;
        my %context;
        $redirector.redirect-from-cache: %params, %context;
        %context<signing>:p.gist.say;
        is-deeply %context<signing>, %signing-context, "\%context<signing> set with %signing-context";
    }

    subtest 'Test only changes context if bucket in cache' => {
        plan 1;
        my %signing-context = :endpoint<bar>;
        %cache<bar> = %signing-context;
        $redirector.=new: :$endpoint-resolver, :$client, :%cache;
        my %params = :Bucket<foo>;
        my %context;
        $redirector.redirect-from-cache: %params, %context;
        nok %context<signing> eqv %signing-context, "\%context<signing> should not match %signing-context";
    }

    subtest 'Test redirect from error' => {
        plan 4;
        my %request = :context{:signing{:bucket<foo>}}, :url<https://us-west-2.amazonaws.com/foo>;
        my @response = Nil, {
            :Error{:Code<PermanentRedirect>, :Endpoint<foo.eu-central-1.amazonaws.com>, :Bucket<foo>},
            :ResponseMetadata{:HTTPHeaders{:x-amz-bucket-region<eu-central-1>}}
        };

        my $redirect-response = $redirector.redirect-from-error: %request, @response, $operation;
        is $redirect-response, 0, "Response should be 0 so that there is no retry delay";

        is %request<url>, 'https://eu-central-1.amazonaws.com/foo';

        my %expected-signing-context = :endpoint<https://eu-central-1.amazonaws.com>, :bucket<foo>,
            :region<eu-central-1>;
        my %signing-context = %request<context><signing>;
        is-deeply %signing-context, %expected-signing-context, "Signing contexts should match";
        ok %request<context><s3_redirected>, "s3_redirected should be True";
    }

    subtest 'Test does not redirect if previously redirected' => {
        plan 1;
        my %request = :context{ :signing{:bucket<foo>, :region<us-west-2>}, :s3_redirected },
                :url<https://us-west-2.amazonaws.com/foo>;
        my @response = Nil, {
            :Error{ :Code<400>, :Message('Bad Request') },
            :ResponseMetadata{
                :HTTPHeaders{:x-amz-bucket-region<us-west-2>}
            }
        };
        my $redirect-response = $redirector.redirect-from-error: %request, @response, $operation;
        nok $redirect-response.defined, "Response should be undefined";
    }

    subtest 'Test does not redirect unless PermanentRedirect received' => {
        plan 2;
        my %request;
        my @response = Nil, {};
        my $redirect-response = $redirector.redirect-from-error: %request, @response, $operation;
        nok $redirect-response.defined, "Response should be undefined";
        is-deeply %request, {}, "request hash should be empty";
    }

    subtest 'Test does not redirect if region cannot be found' => {
        plan 1;
        my %request = :url<https://us-west-2.amazonaws.com/foo>, :context{:signing{:bucket<foo>}};
        my @response = Nil, {
            :Error{ :Code<PermanentRedirect>, :Endpoint<foo.eu-central-1.amazonaws.com>, :Bucket<foo> },
            :ResponseMetadata{:HTTPHeaders{}}
        };

        my $redirect-response = $redirector.redirect-from-error: %request, @response, $operation;

        is $redirect-response, Any, "Should be undefined";
    }

    subtest 'Test redirects 301' => {
        plan 1;
        my %request = :url<https://us-west-2.amazonaws.com/foo>, :context{:signing{:bucket<foo>}};
        my @response = Nil, {
            :Error{ :Code<301>, :Message('Moved Permanently') },
            :ResponseMetadata{:HTTPHeaders{:x-amz-bucket-region<eu-central-1>}}
        };

        $operation.name = 'HeadObject';
        my $redirect-response = $redirector.redirect-from-error: %request, @response, $operation;
        is $redirect-response, 0, "Should be redirected";
    }

    subtest 'Test redirects 400 head bucket' => {
        plan 2;
        my %request = :url<https://us-west-2.amazonaws.com/foo>, :context{:signing{:bucket<foo>}};
        my @response = Nil, {
            :Error{:Code<400>, :Message('Bad Request')},
            :ResponseMetadata{ :HTTPHeaders{:x-amz-bucket-region<eu-central-1>}}
        };

        $operation.name = 'HeadObject';
        my $redirect-response = $redirector.redirect-from-error: %request, @response, $operation;
        is $redirect-response, 0, "Should be redirected";

        $operation.name = 'ListObjects';
        $redirect-response = $redirector.redirect-from-error: %request, @response, $operation;
        nok $redirect-response.defined, "Response should be undefined";
    }

    subtest 'Test does not redirect 400 head bucket no region header' => {
        plan 2;
        my %request = :url<https://us-west-2.amazonaws.com/foo>, :context{:signing{:bucket<foo>}};
        my @response = Nil, {
            :Error{:Code<400>, :Message('Bad Request')}, :ResponseMetadata{:HTTPHeaders{}}
        };

        $operation.name = 'HeadBucket';
        my $redirect-response = $redirector.redirect-from-error: %request, @response, $operation;
        nok $redirect-response.defined, "Response should be undefined";
        :@head-bucket-calls.gist.say;
        is @head-bucket-calls.elems, 0, "client.head-bucket should not be called";
    }

    subtest 'Test does not redirect if undefined response' => {
        plan 1;
        my %request = :url<https://us-west-2.amazonaws.com/foo>, :context{:signing{:bucket<foo>}};
        my $redirect-response = $redirector.redirect-from-error: %request, (), $operation;
        nok $redirect-response.defined, "Response should be undefined";
    }

    subtest 'Test get region from response' => {
        plan 1;
        my @response = Nil, {
            Error => {
                Code => 'PermanentRedirect',
                Endpoint => 'foo.eu-central-1.amazonaws.com',
                Bucket => 'foo'
            },
            :ResponseMetadata{:HTTPHeaders{:x-amz-bucket-region<eu-central-1>}}
        };
        my $region = $redirector.get-bucket-region: 'foo', @response;
        is $region, 'eu-central-1', "Region should match amz-bucket-region";
    }

    subtest 'Test get region from response error body' => {
        plan 1;
        my @response = Nil, {
            Error => {
                Code => 'PermanentRedirect',
                Endpoint => 'foo.eu-central-1.amazonaws.com',
                Bucket => 'foo',
                Region => 'eu-central-1'
            },
            :ResponseMetadata{:HTTPHeaders{}}
        };
        my $region = $redirector.get-bucket-region: 'foo', @response;
        is $region, 'eu-central-1', "Region should match error region";
    }

    subtest 'Test get region from head bucket error' => {
        plan 1;
        $client = set-client-response-headers {:x-amz-bucket-region<eu-central-1>};
        $redirector.=new: :$endpoint-resolver, :$client;
        my @response = Nil, {
            Error => {
                Code => 'PermanentRedirect',
                Endpoint => 'foo.eu-central-1.amazonaws.com',
                Bucket => 'foo'
            },
            :ResponseMetadata{:HTTPHeaders{}}
        };
        my $region = $redirector.get-bucket-region: 'foo', @response;
        is $region, 'eu-central-1', "Should match response headers region";
    }

    subtest 'Test get region from head bucket success' => {
        plan 1;
        my %success-response = :ResponseMetadata{ :HTTPHeaders{:x-amz-bucket-region<eu-central-1>} };
        $client = class {
            method head-bucket(|) { %success-response }
        }.new;
        $redirector.=new: :$endpoint-resolver, :$client;
        my @response = Nil, {
            Error => {
                Code => 'PermanentRedirect',
                Endpoint => 'foo.eu-central-1.amazonaws.com',
                Bucket => 'foo'
            },
            :ResponseMetadata{:HTTPHeaders{}}
        };
        my $region = $redirector.get-bucket-region: 'foo', @response;
        is $region, 'eu-central-1', "Should match success-response region";
    }

    subtest 'Test no redirect from error for accesspoint' => {
        plan 1;
        my %request = :url<https://myendpoint-123456789012.s3-accesspoint.us-west-2.amazonaws.com/key>,
                :context{:s3_accesspoint{}};
        my @response = Nil, {
            :Error{:Code<400>, Message => 'Bad Request'},
            :ResponseMetadata{:HTTPHeaders{:x-amz-bucket-region<eu-central-1>}}
        };
        $operation.name = 'HeadObject';
        my $redirect-response = $redirector.redirect-from-error: %request, @response, $operation;
        nok $redirect-response.defined, "Response should be undefined";
    }

    subtest 'Test no redirect from cache for accesspoint' => {
        plan 1;
        %cache<foo> = {:endpoint<foo-endpoint>};
        $redirector.=new: :$endpoint-resolver, :$client, :%cache;
        my %params = :Bucket<foo>;
        my %context = :s3_accesspoint{};
        $redirector.redirect-from-cache: %params, %context;
        cmp-ok 'signing', '∉', %context, "context shouldn't contain 'signing'";
    }

    done-testing;
}

subtest 'Test ArnParser' => {
    $_ = ArnParser;

    subtest 'Test parse' => {
        plan 1;
        is-deeply .parse-arn('arn:aws:s3:us-west-2:1023456789012:myresource'), {
            :partition<aws>, :service<s3>, :region<us-west-2>, :account('1023456789012'), :resource<myresource>
        }, "Should assign parts to appropriate keys";
    }

    subtest 'Test parse invalid arn' => {
        plan 1;
        try {
            .parse-arn: 'arn:aws:s3';
            flunk "Should die";
            CATCH { default { pass "Should die" } }
        }
    }

    subtest 'Test parse arn with resource type' => {
        plan 1;
        is-deeply .parse-arn('arn:aws:s3:us-west-2:1023456789012:bucket_name:mybucket'), {
            :partition<aws>, :service<s3>, :region<us-west-2>, :account('1023456789012'),
            :resource<bucket_name:mybucket>
        }, "Should not split final ':'";
    }

    subtest 'Test parse arn with empty elements' => {
        plan 1;
        is-deeply .parse-arn('arn:aws:s3:::mybucket'), {
            :partition<aws>, :service<s3>, :region(''), :account(''), :resource<mybucket>
        }, "Should get empty string for region and account";
    }

    done-testing;
}

subtest 'Test S3ArnParamHandler' => {
    my S3ArnParamHandler $arn-handler;
    my $model;

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        $arn-handler.=new;
        $model = class { has $.name is rw is default('GetObject') }.new;
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    subtest 'Test register' => {
        plan 1;
        my $event-emitter = mocked(HierarchicalEmitter);
        $arn-handler.register: $event-emitter;
        check-mock($event-emitter,
                *.called('register', times => 1, with => :('before-parameter-build.s3', Callable)));
    }

    subtest 'Test accesspoint ARN' => {
        plan 2;
        my %params = :Bucket<arn:aws:s3:us-west-2:123456789012:accesspoint/endpoint>;
        my %context;
        $arn-handler.handle-arn: %params, $model, %context;
        is-deeply %params, {:Bucket<endpoint>}, "params<Bucket> should be 'endpoint'";
        is-deeply %context, {:s3_accesspoint{:name<endpoint>, :account('123456789012'), :region<us-west-2>,
            :partition<aws>}}, "context should be populated with ARN attributes";
    }

    subtest 'Test accesspoint arn with colon' => {
        plan 2;
        my %params = :Bucket<arn:aws:s3:us-west-2:123456789012:accesspoint:endpoint>;
        my %context;
        $arn-handler.handle-arn: %params, $model, %context;
        is-deeply %params, {:Bucket<endpoint>}, "params<Bucket> should be 'endpoint'";
        is-deeply %context, {:s3_accesspoint{:name<endpoint>, :account('123456789012'), :region<us-west-2>,
            :partition<aws>}}, "context should be populated with ARN attributes";
    }

    subtest 'Test ignores bucket names' => {
        plan 2;
        my %params = :Bucket<mybucket>;
        my %context;
        $arn-handler.handle-arn: %params, $model, %context;
        is-deeply %params, {:Bucket<mybucket>}, "params<Bucket> should be 'mybucket'";
        nok %context, "context should be empty";
    }

    subtest 'Test ignores create bucket' => {
        plan 2;
        my $arn = 'arn:aws:s3:us-west-2:123456789012:accesspoint/endpoint';
        my %params = Bucket => $arn;
        my %context;
        $model.name = 'CreateBucket';
        $arn-handler.handle-arn: %params, $model, %context;
        is-deeply %params, {:Bucket($arn)}, "params should stay the same";
        is %context.elems, 0, "context should be empty";
    }

    done-testing;
}

subtest 'Test switch-to-virtual-host-style' => {
    my AWSRequest $request;
    my $region-name;
    my $signature-version;

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        $request.=new: :method<PUT>, :url<https://foo.amazonaws.com/bucket/key.txt>;
        $region-name = 'us-west-2';
        $signature-version = 's3';
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    subtest 'Test switch-to-virtual-host-style' => {
        plan 2;
        switch-to-virtual-host-style :$request, :$signature-version, :$region-name;
        is $request.url, 'https://bucket.foo.amazonaws.com/key.txt', "Should hoist bucket name to front of host";
        is $request.auth-path, '/bucket/key.txt', "auth-path should be /bucket/key.txt";
    }

    subtest 'Test uses default endpoint' => {
        plan 2;
        switch-to-virtual-host-style :$request, :$signature-version, :$region-name,
                :default-endpoint-url<s3.amazonaws.com>;
        is $request.url, 'https://bucket.s3.amazonaws.com/key.txt', "Should use default-endpoint-url for hostname";
        is $request.auth-path, '/bucket/key.txt', "auth-path should be /bucket/key.txt";
    }

    subtest 'Test throws invalid DNS name error' => {
        $request.url = 'https://foo.amazonaws.com/mybucket.foo/key.txt';
        try {
            switch-to-virtual-host-style :$request, :$signature-version, :$region-name;
            flunk "Should die with {InvalidDNSNameError.^name}";
            CATCH {
                when InvalidDNSNameError { pass "Should die with {.^name}" }
                default { .resume }
            }
        }
    }

    subtest 'Test fix s3 host only applied once' => {
        plan 2;
        switch-to-virtual-host-style :$request, :$signature-version, :$region-name;
        switch-to-virtual-host-style :$request, :$signature-version, :$region-name;
        is $request.url, 'https://bucket.foo.amazonaws.com/key.txt', ~< Calling the handler again should not affect the
                                                                        end result>;
        is $request.auth-path, "/bucket/key.txt", "Path should be /bucket/key.txt";
    }

    subtest 'Test virtual host style for make bucket' => {
        plan 1;
        $request.url = 'https://foo.amazonaws.com/bucket';
        switch-to-virtual-host-style :$request, :$signature-version, :$region-name;
        # note: URL lib cuts '/' on paths with no parts
        is $request.url, 'https://bucket.foo.amazonaws.com', "Should have no path";
    }

    subtest 'Test virtual host style not used for get bucket location' => {
        plan 1;
        my $original-url = 'https://foo.amazonaws.com/bucket?location';
        $request.url = $original-url;
        switch-to-virtual-host-style :$request, :$signature-version, :$region-name;
        is $request.url, $original-url, ~<The request url should not have been modified because this is a request
                                        for GetBucketLocation.>
    }

    subtest 'Test virtual host styl not used for list buckets' => {
        plan 1;
        my $original-url = 'https://foo.amazonaws.com/';
        $request.url = $original-url;
        switch-to-virtual-host-style :$request, :$signature-version, :$region-name;
        is $request.url, $original-url, ~<The request url should not have been modified because this is a request for
                                            GetBucketLocation>;
    }

    subtest 'Test is unaffected by sigv4' => {
        plan 1;
        $signature-version = 's3v4';
        switch-to-virtual-host-style :$request, :$signature-version, :$region-name,
                :default-endpoint-url<s3.amazonaws.com>;
        is $request.url, 'https://bucket.s3.amazonaws.com/key.txt', "Shouldn't behave differently with v4";
    }

    done-testing;
}

subtest 'Test fix-s3-host' => {
    my AWSRequest $request;
    my ($region-name, $signature-version);

    my $unwrap = &subtest.wrap: sub (|) {
        $request.=new: :method<PUT>, :url<https://s3-us-west-2.amazonaws.com/bucket/key.txt>;
        $region-name = 'us-west-2';
        $signature-version = 's3';
        callsame;
    }

    subtest 'Test fix-s3-host-initial' => {
        plan 2;
        fix-s3-host :$request, :$signature-version, :$region-name;
        is $request.url, 'https://bucket.s3-us-west-2.amazonaws.com/key.txt', "should hoist bucket to host";
        is $request.auth-path, '/bucket/key.txt', "auth-path should be '/bucket/key.txt'";
    }

    subtest 'Test fix-s3-host only applied once' => {
        plan 2;
        fix-s3-host :$request, :$signature-version, :$region-name;
        fix-s3-host :$request, :$signature-version, :$region-name;
        is $request.url, 'https://bucket.s3-us-west-2.amazonaws.com/key.txt', ~<Calling the handler again should not
                                                                                affect the end result>;
        is $request.auth-path, '/bucket/key.txt', ~<Calling the handler again should not
                                                    affect the end result>;
    }

    subtest 'Test DNS style not used to get bucket location' => {
        my $original-url = 'https://s3-us-west-2.amazonaws.com/bucket?location';
        $request.=new: :method<GET>, :url($original-url);
        fix-s3-host :$request, :$signature-version, :$region-name;
        is $request.url, $original-url, "URL should not change";
    }

    subtest 'Test can provide default endpoint URL' => {
        plan 1;
        fix-s3-host :$request, :$signature-version, :$region-name, :default-endpoint-url<foo.s3.amazonaws.com>;
        is $request.url, 'https://bucket.foo.s3.amazonaws.com/key.txt', "Should use default for host";
    }

    subtest 'Test no endpoint URL uses request URL' => {
        plan 1;
        fix-s3-host :$request, :$signature-version, :$region-name, :default-endpoint-url<foo.s3.amazonaws.com>;
        is $request.url, 'https://bucket.foo.s3.amazonaws.com/key.txt', "should use provided default";
    }

    done-testing;
}

subtest 'Test S3EndpointSetter' => {
    my ($operation-name, $signature-version, $region-name, $account, $bucket, $key, $accesspoint-name, $partition,
            $endpoint-resolver, $dns-suffix);
    my S3EndpointSetter $endpoint-setter;

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        $operation-name = 'GetObject';
        $signature-version = 's3v4';
        $region-name = 'us-west-2';
        $account = '123456789012';
        $bucket = 'mybucket';
        $key = 'key.txt';
        $accesspoint-name = 'myaccesspoint';
        $partition = 'aws';
        $endpoint-resolver = mocked(ClientEndpointBridge, overriding => {
            construct-endpoint => -> | { %{ dnsSuffix => $dns-suffix } }
        });
        $dns-suffix = 'amazonaws.com';
        $endpoint-setter.=&init;
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    my method init(S3EndpointSetter: *%args) {
        my %setter-args = :$endpoint-resolver, :region($region-name);
        %setter-args{%args.keys} = %args.values;
        self.new: |%setter-args;
    }

    my sub get-s3-request(:$bucket, :$key, :$scheme = 'https://', :$querystring --> AWSRequest) {
        my $url = $scheme ~ 's3.us-west-2.amazonaws.com/';
        $url ~= $_ with $bucket;
        $url ~= "/$_" with $key;
        $url ~= "?$_" with $querystring;
        return AWSRequest.new: :method<GET>, :$url;
    }

    my sub get-s3-accesspoint-request(:$ap-name is copy = $accesspoint-name, Associative :$ap-context is copy,
            *%s3-request-args
            --> AWSRequest) {
        my $request = get-s3-request bucket => $ap-name, |%s3-request-args;
        $_ = get-s3-accesspoint-context name => $ap-name without $ap-context;
        $request.context<s3_accesspoint> = $ap-context;
        return $request;
    }

    my sub get-s3-accesspoint-context(*%overrides) {
        my %accesspoint-context = name => $accesspoint-name, :$account, region => $region-name, :$partition;
        %accesspoint-context{%overrides.keys} = %overrides.values;
        return %accesspoint-context;
    }

    my sub call-set-endpoint(S3EndpointSetter $endpoint-setter, $request, *%args) {
        my %set-endpoint-args = :$operation-name, :$signature-version, :$region-name;
        %set-endpoint-args{%args.keys} = %args.values;
        $endpoint-setter.set-endpoint: $request, |%set-endpoint-args;
    }

    subtest 'Test register' => {
        plan 1;
        my $called-args;
        my $event-emitter = mocked(HierarchicalEmitter);
        $endpoint-setter.register: $event-emitter;
        check-mock($event-emitter,
                *.called('register', with => :('before-sign.s3', Callable)));
    }

    subtest 'Test accesspoint endpoint' => {
        plan 1;
        my $request = get-s3-accesspoint-request;
        call-set-endpoint $endpoint-setter, $request;
        my $expected-url = "https://{$accesspoint-name}-{$account}.s3-accesspoint.{$region-name}.amazonaws.com";
        is $request.url, $expected-url, "Request url should match expected";
    }

    subtest 'Test accesspoint preserves key in path' => {
        plan 1;
        my $request = get-s3-accesspoint-request :$key;
        call-set-endpoint $endpoint-setter, $request;
        my $expected-url = "https://{$accesspoint-name}-{$account}.s3-accesspoint.{$region-name}.amazonaws.com/$key";
        is $request.url, $expected-url, "Request url should match expected";
    }

    subtest 'Test accesspoint preserves scheme' => {
        plan 1;
        my $request = get-s3-accesspoint-request :scheme<http://>;
        call-set-endpoint $endpoint-setter, $request;
        my $expected-url = "http://{$accesspoint-name}-{$account}.s3-accesspoint.{$region-name}.amazonaws.com";
        is $request.url, $expected-url, "Request url should match expected";
    }

    subtest 'Test uses resolved DNS suffix' => {
        plan 1;
        $dns-suffix = 'mysuffix.com';
        my $request = get-s3-accesspoint-request;
        call-set-endpoint $endpoint-setter, $request;
        my $expected-url = "https://{$accesspoint-name}-{$account}.s3-accesspoint.{$region-name}.mysuffix.com";
        is $request.url, $expected-url, "Request url should match expected";
    }

    subtest 'Tests uses region of client if use-arn disabled' => {
        plan 1;
        my $client-region = 'client-region';
        $endpoint-setter.=&init: region => $client-region, :s3-config{:!use_arn_region};
        my $request = get-s3-accesspoint-request;
        call-set-endpoint $endpoint-setter, $request;
        my $expected-url = "https://{$accesspoint-name}-{$account}.s3-accesspoint.{$client-region}.amazonaws.com";
        is $request.url, $expected-url, "Request URL should match expected";
    }

    subtest 'Test accesspoint errors for custom endpoint' => {
        plan 1;
        $endpoint-setter.=&init: endpoint-url => 'https://custom.com';
        my $request = get-s3-accesspoint-request;
        try {
            call-set-endpoint $endpoint-setter, $request;
            flunk "Should die";
            CATCH { default { pass "Should die" } }
        }
    }

    subtest 'Test errors for mismatching partition' => {
        plan 1;
        $endpoint-setter.=&init: partition => 'aws-cn';
        my %accesspoint-context = get-s3-accesspoint-context partition => 'aws';
        my $request = get-s3-accesspoint-request ap-context => %accesspoint-context;
        try {
            call-set-endpoint $endpoint-setter, $request;
            flunk "Should die";
            CATCH { default { pass "Should die" } }
        }
    }

    subtest 'Test errors for mismatching partition when using client region' => {
        plan 1;
        $endpoint-setter.=&init: :s3-config{:!use_arn_region}, :partition<aws-cn>;
        my %accesspoint-context = get-s3-accesspoint-context partition => 'aws';
        my $request = get-s3-accesspoint-request ap-context => %accesspoint-context;
        try {
            call-set-endpoint $endpoint-setter, $request;
            flunk "Should die with {UnsupportedS3AccesspointConfigurationError.^name}";
            CATCH {
                when UnsupportedS3AccesspointConfigurationError {
                    pass "Should die with {UnsupportedS3AccesspointConfigurationError.^name}";
                }
                default { .resume }
            }
        }
    }

    subtest 'Test set-endpoint for auto' => {
        plan 1;
        $endpoint-setter.=&init: :s3-config{:addressing_style<auto>};
        my $request = get-s3-request :$bucket, :$key;
        call-set-endpoint $endpoint-setter, $request;
        my $expected-url = "https://{$bucket}.s3.us-west-2.amazonaws.com/$key";
        is $request.url, $expected-url, "Request URL should match expected";
    }

    subtest 'Test set-endpoint for virtual' => {
        plan 1;
        $endpoint-setter.=&init: :s3-config{:addressing_style<virtual>};
        my $request = get-s3-request :$bucket, :$key;
        call-set-endpoint $endpoint-setter, $request;
        my $expected-url = "https://{$bucket}.s3.us-west-2.amazonaws.com/$key";
        is $request.url, $expected-url, "Request URL should match expected";
    }

    subtest 'Test set-endpoint for path' => {
        plan 1;
        $endpoint-setter.=&init: :s3-config{:addressing_style<path>};
        my $request = get-s3-request :$bucket, :$key;
        call-set-endpoint $endpoint-setter, $request;
        my $expected-url = "https://s3.us-west-2.amazonaws.com/$bucket/$key";
        is $request.url, $expected-url,  "Request URL should match expected";
    }

    subtest 'Test set-endpoint for accelerate' => {
        $endpoint-setter.=&init: :s3-config{:use_accelerate_endpoint};
        my $request = get-s3-request :$bucket, :$key;
        call-set-endpoint $endpoint-setter, $request;
        my $expected-url = "https://$bucket.s3-accelerate.amazonaws.com/$key";
        is $request.url, $expected-url, "Request URL should match expected";
    }

    done-testing;
}

done-testing;
