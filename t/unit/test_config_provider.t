use lib 'lib';
use Test;
use AWS::Botocore::ConfigProvider;

subtest 'Test ConfigValueStore' => {
    subtest 'Test does provide Nil if no variable exists' => {
        plan 1;
        my $provider = ConfigValueStore.new;
        nok $provider.get-config-variable('fake-variable').defined, "Should return undefined value";
    }

    subtest 'Test does provide value' => {
        plan 1;
        my $fake-variable = class { has $.provide = 'foo' }.new;
        my ConfigValueStore $provider .= new: :mapping{:$fake-variable};
        my $value = $provider.get-config-variable: 'fake-variable';
        is $value, 'foo', "Should return 'foo'";
    }

    subtest 'Test can set variable' => {
        plan 1;
        my ConfigValueStore $provider.=new;
        $provider.set-config-variable: 'fake-variable', 'foo';
        my $value = $provider.get-config-variable: 'fake-variable';
        is $value, 'foo', "Should return 'foo'";
    }

    subtest 'Test can set config provider' => {
        plan 2;
        my $foo-provider = class { has $.provide = 'foo' }.new;
        my ConfigValueStore $provider.=new: :mapping{fake-variable => $foo-provider};

        my $value = $provider.get-config-variable: 'fake-variable';
        is $value, 'foo', "Should return 'foo'";

        my $bar-provider = class { has $.provide = 'bar' }.new;
        $provider.set-config-provider: 'fake-variable', $bar-provider;
        $value = $provider.get-config-variable: 'fake-variable';
        is $value, 'bar', "Should return 'bar'";
    }

    done-testing;
}

done-testing;
