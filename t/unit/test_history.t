use lib 'lib';
use AWS::Botocore::History;
use Test;

class RecordingHandler does BaseHistoryHandler { # todo mock
    has @.recorded-calls;
    method emit($event_type, $payload, $source) {
        push @!recorded-calls, ($event_type, $payload, $source);
    }
}

class ExceptionThrowingHandler does BaseHistoryHandler {
    method emit($, $, $) { die "Should not have died" }
}

subtest 'Test HistoryRecorder' => {
    my RecordingHandler $handler;
    my HistoryRecorder $recorder;

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        .=new for $handler, $recorder;
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    subtest 'one handler' => {
        plan 1;
        $recorder.enable;
        $recorder.add-handler: $handler;
        $recorder.record: 'foo', 'bar', 'source';
        is-deeply $handler.recorded-calls, [$(<foo bar source>)], 'One event recorded';
    }

    subtest 'multiple handlers' => {
        plan 2;
        my RecordingHandler $handler2.=new;
        $recorder.enable;
        $recorder.add-handler: $handler;
        $recorder.add-handler: $handler2;
        $recorder.record: 'foo', 'bar', 'source';
        my @expected = [$(<foo bar source>)];
        is-deeply $handler.recorded-calls, @expected, 'Handler 1: one event recorded';
        is-deeply $handler2.recorded-calls, @expected, 'Handler 2: one event recorded';
    }

    subtest 'use botocore source by default' => {
        plan 1;
        $recorder.enable;
        $recorder.add-handler: $handler;
        $recorder.record: 'foo', 'bar';
        is-deeply $handler.recorded-calls, [$(<foo bar BOTOCORE>)], 'Uses BOTOCORE source by default';
    }

    subtest 'does not call handlers when never enabled' => {
        plan 1;
        $recorder.add-handler: $handler;
        $recorder.record: 'foo', 'bar';
        is $handler.recorded-calls.elems, 0, 'Does not call handlers when not enabled';
    }

    subtest 'does not call handlers when disabled' => {
        plan 1;
        $recorder.enable;
        $recorder.disable;
        $recorder.add-handler: $handler;
        $recorder.record: 'foo', 'bar';
        is $handler.recorded-calls.elems, 0, 'Does not call handlers when disabled';
    }

    subtest 'can ignore handler exceptions' => {
        plan 1;
        my ExceptionThrowingHandler $bad-handler.=new;
        $recorder.enable;
        $recorder.add-handler: $bad-handler;
        $recorder.add-handler: $handler;
        try {
            $recorder.record: 'foo', 'bar';
            is $handler.recorded-calls, [$(<foo bar BOTOCORE>)], 'Healthy handler still recorded';
            CATCH { default { flunk 'Should not die' } }
        }
    }

    done-testing;
}

subtest 'can get history recorder' => {
    plan 2;
    my $recorder = get-global-history-recorder;
    ok $recorder ~~ HistoryRecorder:D, 'Recorder should be a HistoryRecorder instance';

    my $recorder2 = get-global-history-recorder;
    ok $recorder === $recorder2, 'Recorders should be same instance';
}

done-testing;
