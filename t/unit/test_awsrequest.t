use lib 'lib';
use Test;
use File::Temp;
use AWS::Botocore::Request;

subtest 'Test prepare-request-hash' => {
    my Str ($ua, $eu);
    my %base-request;

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        $ua = 'botocore/1.0';
        $eu = 'https://s3.amazonaws.com';
        %base-request =
            'body'          => '',
            'headers'       => {},
            'method'        => 'GET',
            'query_string'  => '',
            'url_path'      => '/',
            'context'       => {}
        ;
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    my sub prepare-base-request-hash(%request, Str :$endpoint-url = $eu, Str :$user-agent = $ua,
            :%context) {
        %base-request{ %request.keys } = %request.values;
        prepare-request-hash %base-request, :$endpoint-url, :$user-agent, :%context;
    }

    subtest 'Test prepare-request-hash for get' => {
        plan 3;
        my %request = method => 'GET', url_path => '/';
        prepare-base-request-hash %request, endpoint-url => 'https://s3.amazonaws.com';
        is %base-request<method>, 'GET', "Should have GET method";
        is %base-request<url>, 'https://s3.amazonaws.com', "Should be s3 URL";
        is %base-request<headers><User-Agent>, $ua, "Should update user agent";
    }

    subtest 'Test prepare-request-hash for get no user agent' => {
        plan 1;
        $ua = Nil;
        my %request = method => 'GET', url_path => '/';
        prepare-base-request-hash %request;
        nok 'User-Agent' ∈ %base-request<headers>, "Shouldn't have User-Agent header";
    }

    subtest 'Test query-string-serialized to url' => {
        plan 1;
        my %request = method => 'GET',
                query_string => { prefix => 'foo' },
                url_path => '/mybucket';
        prepare-base-request-hash %request;
        is %base-request<url>, 'https://s3.amazonaws.com/mybucket?prefix=foo', "Should construct proper URL";
    }

    subtest 'Test url path combined with endpoint url' => {
        plan 1;
        my %request = query_string => { prefix => 'foo' }, url_path => '/mybucket';
        my $endpoint-url = 'https://custom.endpoint/foo/bar';
        prepare-base-request-hash %request, :$endpoint-url;
        is %base-request<url>, 'https://custom.endpoint/foo/bar/mybucket?prefix=foo',
                "Should append query string";
    }

    subtest 'Test url path with trailing slash' => {
        plan 1;
        todo "URL modules doesn't currently support paths with trailing slashes; need PR to fix";
        try prepare-base-request-hash { url_path => '/mybucket' }, endpoint-url => 'https://custom.endpoint/foo/bar/';
        is %base-request<url>, 'https://custom.endpoint/foo/bar/mybucket', "Should append to path";
    }

    subtest 'Test url path is slash' => {
        plan 1;
        todo "URL modules doesn't currently support paths with trailing slashes; need PR to fix";
        try prepare-base-request-hash {url_path => '/'}, endpoint-url => 'https://custom.endpoint/foo/bar/';
        is %base-request<url>, 'https://custom.endpoint/foo/bar', "Should chop slash";
    }

    subtest 'Test url path is slash with endpoint url no slash' => {
        plan 1;
        prepare-base-request-hash {url_path => '/'}, endpoint-url => 'https://custom.endpoint/foo/bar';
        is %base-request<url>, 'https://custom.endpoint/foo/bar', "Shouldn't change";
    }

    subtest 'Test custom endpoint with query string' => {
        plan 1;
        prepare-base-request-hash {url_path => '/baz', query_string => {x => 'y'}},
                endpoint-url => 'https://custom.endpoint/foo/bar?foo=bar';
        is %base-request<url>, 'https://custom.endpoint/foo/bar/baz?foo=bar&x=y', "Should add params";
    }

    done-testing;
}

subtest 'Test AWSRequest' => {
    my AWSRequest $request;
    my AWSPreparedRequest $prepared-request;
    my Str $filename;
    my IO::Handle $fh;

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        try .close with $fh;
        ($filename, $fh) = tempfile;
        $request.=new: method => 'GET', url => 'http://example.com';
        $prepared-request = $request.prepare;
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    subtest 'Test can prepare url params' => {
        plan 1;
        $request.=new: url => 'http://example.com', params => { foo => 'bar' };
        $prepared-request = $request.prepare;
        is $prepared-request.url, 'http://example.com?foo=bar', "Should add query params to url";
    }

    subtest 'Test can prepare dict body' => {
        plan 1;
        my %data = dead => 'beef';
        $request.=new: url => 'http://example.com', :%data;
        $prepared-request = $request.prepare;
        is $prepared-request.body, 'dead=beef', "Should xform body to query string";
    }

    subtest 'Test can prepare dict body unicode values' => {
        plan 1;
        my %data = Text => "\x30c6\x30b9\x30c8 string";
        my $expected_body = 'Text=%E3%83%86%E3%82%B9%E3%83%88%20string';
        $request.=new: url => 'http://example.com/', :%data;
        $prepared-request = $request.prepare;
        is $prepared-request.body, $expected_body, "Should URL unicode chars";
    }

    subtest 'Test can prepare dict body unicode keys' => {
        plan 1;
        my %data= "\x30c6\x30b9\x30c8" => 'string';
        my $expected_body = '%E3%83%86%E3%82%B9%E3%83%88=string';
        $request.=new: url => 'http://example.com/', :%data;
        $prepared-request = $request.prepare;
        is $prepared-request.body, $expected_body, "Should URL encode key";
    }

    subtest 'Test can prepare empty body' => {
        plan 2;
        $request.=new: url => 'http://example.com/', data => '';
        $prepared-request = $request.prepare;
        nok $prepared-request.body.defined, "Body shouldn't be defined";
        my $content-length = $prepared-request.headers<content-length>;
        is $content-length, '0', "Length should be zero";
    }

    subtest 'Test request body is prepared' => {
        plan 1;
        $request.=new: url => 'http://example.com', data => 'body';
        is $request.body, 'body', "Should be what was put in";
    }

    subtest 'Test prepare body content adds content length' => {
        plan 1;
        my $content = 'foobarbaz'.encode;
        my $expected-len = $content.bytes.Str;
        $fh.write: $content;
        $fh.seek: 0;
        $request.=with: data => $fh, method => 'POST';
        $prepared-request = $request.prepare;
        my $calculated-len = $prepared-request.headers<Content-Length>;
        is $calculated-len, $expected-len, "Should match length of file data";
    }

    subtest "Test prepare body doesn't override content length" => {
        plan 1;
        $request.=with: method => 'PUT', headers => { Content-Length =>'20' }, data => 'asdf';
        $prepared-request = $request.prepare;
        is $prepared-request.headers<Content-Length>, '20', "Should reflect content length provided";
    }

    subtest "Test prepare body doesn't set content length head" => {
        plan 1;
        $request.=with: method => 'HEAD', data => 'thisshouldntbehere';
        $prepared-request = $request.prepare;
        nok $prepared-request.headers<Content-Length>:exists, "Shouldn't have content length";
    }

    subtest 'Test can reset stream handles binary' => {
        plan 1;
        my $contents = 'notastream'.encode;
        $prepared-request.=with: body => $contents;
        $prepared-request.reset-stream;
        is $prepared-request.body, $contents, "Shouldn't have changed after 'stream' reset";
    }

    subtest 'Test can reset stream' => {
        plan 2;
        my $contents = 'foobarbaz'.encode;
        $fh.write: $contents;
        $fh.seek: 0;
        $prepared-request.=with: body => $fh;
        # pretend the request was partially sent
        $fh.read;
        isnt $fh.tell, 0, "IO cursor shouldn't be zero";
        # have the prepared request reset its stream
        $prepared-request.reset-stream;
        is $fh.tell, 0, "Stream should be reset";
    }

    subtest 'Test cannot reset stream dies' => {
        plan 2;
        my $contents = 'foobarbaz'.encode;
        $fh.write: $contents;
        my class Unseekable {
            has IO::Handle $.handle;
            method read { $!handle.read }
            method seek { die "Can't seek" }
        }
        my Unseekable $body.=new: handle => $fh;
        $prepared-request.=with: body => $body;
        $body.read;
        isnt $fh.tell, 0, "Seek pointer shouldn't be zero";
        try {
            $prepared-request.reset-stream;
            flunk "Should die on seek";
            CATCH { default { pass "Should die on seek" } }
        }
    }

    subtest 'Test duck type for file check' => {
        plan 1;
        class LooksLikeFile {
            has Bool $.seek-called = False;
            method read {}
            method seek(|) { $!seek-called = True }
        }
        my LooksLikeFile $looks-like-file.=new;
        $prepared-request.=with: body => $looks-like-file;
        $prepared-request.reset-stream;
        ok $looks-like-file.seek-called, "Seek called on non-file object";
    }

    done-testing;
}

done-testing;
