use lib 'lib';
use Test;
use Test::Mock;

use AWS::Botocore::Args;
use AWS::Botocore::BaseEventHooks;
use AWS::Botocore::Client;
use AWS::Botocore::ConfigProvider;
use AWS::Botocore::Hooks;
use AWS::Botocore::Model;

my ClientArgsCreator $args-create;
my BaseEventHooks $event-emitter;
my ConfigValueStore $config-store;
my ($service-name, $region, $endpoint-url);
my ServiceModel $service-model;
my ClientEndpointBridge $bridge;
my @default-socket-options;

my $unwrap = ENTER &subtest.wrap: sub (|) {
    $event-emitter = mocked(HierarchicalEmitter);
    $config-store.=new;
    $args-create.=new: :$event-emitter, :$config-store;
    ($service-name, $region, $endpoint-url) = <ec2 us-west-2 https://ec2/>;
    $service-model = get-service-model;
}

my sub get-service-model(:service-name($name) = $service-name) {
    my $service-model = mocked(ServiceModel, overriding => {
        service-name => $name,
            endpoint-prefix => $name,
            metadata => {
                service-full-name => 'MyService',
                protocol => 'query'
            },
            operation-names => []
    });
    return $service-model;
}

done-testing;
