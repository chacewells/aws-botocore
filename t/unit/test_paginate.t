use lib 'lib';

use MONKEY-TYPING;
# PageIterator trusts StartingParamsInjector

use Test;
use Test::Mock;

use AWS::Botocore::Paginate;
use AWS::Botocore::Model;

sub get-operation-model-mock {
    mocked(OperationModel, returning => {
        input-shape => mocked(Shape, returning => {
            members => { MaxKeys => class { has $.type-name = 'integer' }.new } # service classes are dynamically generated
        })
    });
}

subtest 'TokenDecoder tests' => {
    plan 5;
    my TokenDecoder $decoder;
    my ($token, %expected); # work variables

    $token = 'eyJmb28iOiAiYmFyIn0=';
    %expected = foo => 'bar';
    is-deeply $decoder.decode($token), %expected, "Decodes to expected hash";

    $token = 'eyJib3RvX2VuY29kZWRfa2V5cyI6IFtbImZvbyJdXSwgImZvbyI6ICJZbUZ5In0=';
    %expected = foo => 'bar';
    is-deeply $decoder.decode($token), %expected, "Decodes to expected hash";

    $token = 'eyJmb28iOiB7ImJhciI6ICJZbUY2In0sICJib3RvX2VuY29kZWRfa2V5cyI6'
         ~ 'IFtbImZvbyIsICJiYXIiXV19';
    %expected = foo => {:bar<baz>};
    is-deeply $decoder.decode($token), %expected, "Decodes to expected hash";

    $token = 'eyJib3RvX2VuY29kZWRfa2V5cyI6IFtbImZvbyIsICJiYXIiLCAxXV0sICJmb28i'
         ~ 'OiB7ImJhciI6IFsiYmF6IiwgIlltbHUiXX19';
    %expected = foo => {bar => [<baz bin>]};
    is-deeply $decoder.decode($token), %expected, "Decodes to expected hash";

    $token = 'eyJib3RvX2VuY29kZWRfa2V5cyI6IFtbImZvbyIsICJiaW4iXSwgWyJmb28iLCAi'
         ~ 'YmFyIl1dLCAiZm9vIjogeyJiaW4iOiAiWW1GdCIsICJiYXIiOiAiWW1GNiJ9fQ==';
    %expected = foo => {:bar<baz>, :bin<bam>};
    is-deeply $decoder.decode($token), %expected, "Decodes to expected hash";
}

subtest 'TokenEncoder Tests' => {
    plan 5;
    my TokenDecoder $decoder;
    my TokenEncoder $encoder;
    my (%original, $token); # work variables

    %original = foo => 'bar';
    $token = $encoder.encode(%original);
    is-deeply $decoder.decode($token), %original, "Encoded/decoded to same value";

    %original = foo => 'bar';
    $token = $encoder.encode: %original;
    is-deeply $decoder.decode($token), %original, "Encoded/decoded to same value";

    %original = foo => {:bar<baz>};
    $token = $encoder.encode: %original;
    is-deeply $decoder.decode($token), %original, "Encoded/decoded to same value";

    %original = foo => {bar => [<baz bin>]};
    $token = $encoder.encode: %original;
    is-deeply $decoder.decode($token), %original, "Encoded/decoded to same value";

    %original = foo => {:bar<baz>, :bin<bam>};
    $token = $encoder.encode: %original;
    is-deeply $decoder.decode($token), %original, "Encoded/decoded to same value";
}

subtest 'PaginatorModel Tests' => {
    plan 2;
    my %paginator-config;
    my PaginatorModel $paginator-model;

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        %paginator-config<pagination> = {
            :ListFoos{ :output_token<NextToken>, :input_token<NextToken>, :result_key<Foo> }
        };
        $paginator-model.=new: :%paginator-config;
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    subtest "Test get ListFoos paginator" => {
        plan 1;
        my %paginator-config = $paginator-model.get-paginator: 'ListFoos';
        is-deeply %paginator-config, {:output_token<NextToken>, :input_token<NextToken>, :result_key<Foo>},
                "Should match ListFoos hash";
    }

    subtest "Test get ListBars paginator should die" => {
        plan 1;
        try {
            $ = $paginator-model.get-paginator: 'ListBars';
            flunk "Should die";
            CATCH { default { pass "Should die" } }
        }
    }
}

subtest 'Pagination Tests' => {
    my Paginator $paginator;
    my (&method, $model, %pagination-config); # work variables

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        %pagination-config = :output_token<NextToken>, :input_token<NextToken>, :result_key<Foo>;
        $model = get-operation-model-mock;
        (&method, $paginator) = Nil xx *;
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    subtest 'Test result key available' => {
        plan 1;
        &method = { $_ };
        $paginator.=new: :$model, :&method, :%pagination-config;
        is-deeply $paginator.result-keys».<expression>.Array, ['Foo'], "Only result key expression should be 'Foo'";
    }

    subtest 'Test no next token' => {
        plan 1;
        my %response = 'not_the_next_token' => 'foobar';
        &method = sub (|) { %response };
        $paginator.=new: :$model, :&method, :%pagination-config;
        my $page-iterator := $paginator.paginate;
        my @actual = gather { take $_ for $page-iterator };
        is-deeply @actual, [$%response], "Response should match mock return value";
    }

    subtest 'Test next token in response' => {
        plan 1;
        my @responses = {:NextToken<token1>}, ${:NextToken<token2>}, ${:not_next_token<foo>};
        &method = sub (|) { @responses[(state $ = 0)++] };
        $paginator.=new: :$model, :&method, :%pagination-config;
        my $page-iterator := $paginator.paginate;
        my @actual = gather take $_ for $page-iterator;
        is-deeply @actual, @responses, "Should match input responses";
    }

    subtest 'Test next token is string' => {
        plan 1;
        %pagination-config = :output_token<Marker>, :input_token<Marker>, :result_key<Users>, :limit_key<MaxKeys>;
        my @responses = {:Users[<User1>], :Marker<m1>}, {:Users[<User2>], :Marker<m2>}, {:Users[<User3>]};
        &method = { @responses[(state $ = 0)++] };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $page-iterator = $paginator.paginate: PaginationConfig => { MaxItems => 1 };
        my $result = $page-iterator.build-full-result;
        ok $result<NextToken> ~~ Str, "Should be string";
    }

    subtest 'Test any passed in args are unmodified' => {
        plan 2;
        my @responses = {:NextToken<token1>}, {:NextToken<token2>}, {:not_next_token<foo>};
        my @called-args;
        &method = sub (*%args) { push @called-args, %args; @responses[(state $ = 0)++] };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $page-iterator := $paginator.paginate(:Foo<foo>, :Bar<bar>);
        my @actual = gather { take $_ for $page-iterator };
        is-deeply @actual, @responses, "actual should match input responses";
        is-deeply @called-args, [
            { :Foo<foo>, :Bar<bar> },
            { :Foo<foo>, :Bar<bar>, :NextToken<token1> },
            { :Foo<foo>, :Bar<bar>, :NextToken<token2> },
        ], "Should include next token on each call";
    }

    subtest 'Test exception is raised if same next token' => {
        plan 1;
        my @responses = {:NextToken<token1>}, {:NextToken<token2>}, {:NextToken<token2>};
        &method = sub (|) { @responses[(state $ = 0)++] };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $page-iterator := $paginator.paginate;
        try {
            @ = gather { take $_ for $page-iterator };
            flunk "Should die";
            CATCH { default { pass "Should die" } }
        }
    }

    subtest 'Test next token with or expression' => {
        plan 1;
        %pagination-config = :output_token('NextToken || NextToken2'), :input_token<NextToken>, :result_key<Foo>;
        my @responses = {:NextToken<token1>}, {:NextToken2<token2>}, {:NextToken<token3>, :NextToken2<token4>},
                {:not_next_token<foo>};
        my @called-args;
        &method = sub (*%args) {
            push @called-args, %args<NextToken>;
            @responses[(state $ = 0)++]
        };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $page-iterator := $paginator.paginate;
        @ = gather take $_ for $page-iterator;
        is-deeply @called-args, [Any, 'token1', 'token2', 'token3'], "Should match NextToken on calls 2..4";
    }

    subtest 'Test more tokens' => {
        plan 1;
        todo "Nevermind; better look into this";
        %pagination-config = :more_results<IsTruncated>, :output_token<NextToken>, :input_token<NextToken>,
                :result_key<Foo>;
        my @responses = {:Foo[1], :IsTruncated, :NextToken<token1>}, {:Foo[2], :IsTruncated, :NextToken<token2>},
                {:Foo[3], :IsTruncated, :NextToken<token3>}, {:Foo[4], :not_next_token<foo>};
        my @called-args;
        &method = sub (*%args) {
            push @called-args, %args<NextToken>;
            @responses[(state $ = 0)++]
        };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $page-iterator := $paginator.paginate;
        @ = gather take $_ for $page-iterator;
        is-deeply @called-args, [Any, 'token1', 'token2'], "Should be called with [Any, token1, token2]";
    }

    subtest 'Test more tokens is path expression' => {
        plan 1;
        todo "Nevermind; better look into this";
        %pagination-config = :more_results<Foo.IsTruncated>, :output_token<NextToken>, :input_token<NextToken>,
                :result_key<Bar>;
        my @responses = {:Foo{:IsTruncated}, :NextToken<token1>}, {:Foo{:IsTruncated}, :NextToken<token2>};
        my @called-args;
        &method = sub (*%args) {
            push @called-args, %args<NextToken>;
            @responses[(state $ = 0)++]
        };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $page-iterator := $paginator.paginate;
        @ = gather take $_ for $page-iterator;
        is-deeply @called-args, [Any, 'token1'], "Should be called with [Any, token1]";
    }

    subtest 'Test page size' => {
        plan 1;
        %pagination-config = :output_token<Marker>, :input_token<Marker>, :result_key<Users>, :limit_key<MaxKeys>;
        my @responses = {:Users['User1'], :Marker<m1>}, {:Users['User2'], :Marker<m2>}, {:Users['User3']};
        my @called-args;
        &method = sub (*%args) {
            push @called-args, %args;
            @responses[(state $ = 0)++]
        };

        $paginator.=new: :&method, :$model, :%pagination-config;
        my $page-iterator := $paginator.paginate: :PaginationConfig{:1PageSize};
        @ = gather take $_ for $page-iterator;
        is-deeply @called-args, [{:1MaxKeys}, {:Marker<m1>, :1MaxKeys}, {:Marker<m2>, :1MaxKeys}],
                "Called args should match";
    }

    subtest 'Test with empty markers' => {
        plan 2;
        my @responses = {:Users['User1'], :Marker('')}, {:Users['User1'], :Marker('')}, {:Users['User1'], :Marker('')};
        my @called-args;
        &method = sub (*%args) { push @called-args, %args; @responses[(state $ = 0)++] };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $page-iterator := $paginator.paginate;
        my @users = gather take .<Users>.Slip for $page-iterator;
        is-deeply @called-args, $[$({})], "Should be called once with no args";
        is-deeply @users, ['User1'], "'User1' is the only return value";
    }

    subtest 'Test build full result with single key' => {
        plan 1;
        %pagination-config = :output_token<Marker>, :input_token<Marker>, :result_key<Users>, :limit_key<MaxKeys>;
        my @responses = {:Users['User1'], :Marker<m1>}, {:Users['User2'], :Marker<m2>}, {:Users['User3']};
        &method = sub (|) { @responses[(state $ = 0)++] };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $page-iterator := $paginator.paginate;
        my %complete = $page-iterator.build-full-result;
        is-deeply %complete<Users>.Array, [<User1 User2 User3>], "Should collect all users";
    }

    subtest 'Test-build-multiple-results' => {
        plan 1;
        %pagination-config = :output_token<Marker>, :input_token<Marker>, :result_key<Users>, :limit_key<MaxKeys>;
        my ($PageSize, $MaxItems) = 2, 3;
        my @responses = {:Users[<User1 User2>], :Marker<m1>},
                {:Users[<User3 User4>], :Marker<m2>},
                # todo: python has 'Users' : ['User3', 'User4'] again
                # on second iteration, jmespath search results in [User4]
                # behavior is not the same for Perl5 jmespath module, so returning singleton array
                {:Users[<User4>], :Marker<m2>},
                {:Users[<User5 User6 User7>], :Marker<m3>};
        &method = sub (|) {
            @responses[(state $ = 0)++]
        };

        $paginator.=new: :&method, :$model, :%pagination-config;
        my %result = do {
            my $pages := $paginator.paginate: :PaginationConfig{:$PageSize, :$MaxItems};
            $pages.build-full-result;
        }

        %result = do {
            my $pages := $paginator.paginate: :PaginationConfig{
                :$PageSize, :$MaxItems, :StartingToken(%result<NextToken>)
            };
            $pages.build-full-result;
        }

        my $expected-token = TokenEncoder.encode: { :Marker<m2>, :2boto_truncate_amount };
        todo "Flaky test";
        is %result<NextToken>, $expected-token, "NextToken should match";
    }
}

class StartingParamsInjector {...}

augment class PageIterator {
    trusts StartingParamsInjector
}

# util to forward starting params to PageIterator
class StartingParamsInjector does Callable {
    has PageIterator $.page-iterator;
    method CALL-ME(%args) { $!page-iterator!PageIterator::inject-starting-params: %args }
}

subtest 'Test Paginator page size' => {
    my Paginator $paginator;
    my StartingParamsInjector $inject-starting-params;
    my (&method, $model, %pagination-config); # work variables

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        %pagination-config = :output_token<Marker>, :input_token<Marker>, :result_Key[<Users Groups>],
                :limit_key<MaxKeys>;
        ($paginator, $inject-starting-params, &method) = Nil xx *;
        $model = get-operation-model-mock;
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    subtest 'Test no page size' => {
        plan 1;
        my %args = :arg1<foo>, :arg2<bar>;
        my %ref-args = :arg1<foo>, :arg2<bar>;
        &method = sub (|) {};
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $page-iterator := $paginator.paginate: |%args;
        $inject-starting-params.=new: :$page-iterator;
        $inject-starting-params(%args);
        is-deeply %args, %ref-args, "Args should match ref-args after injection";
    }

    subtest 'Test page size' => {
        plan 1;
        my %args = :arg1<foo>, :arg2<bar>, :PaginationConfig{:5PageSize};
        my %extracted-args = :arg1<foo>, :arg2<bar>;
        # Note that ``MaxKeys`` in ``setUp()`` is the parameter used for
        # the page size for pagination.
        my %ref-args = :arg1<foo>, :arg2<bar>, :5MaxKeys;
        &method = sub (|) {};
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $page-iterator := $paginator.paginate: |%args;
        $inject-starting-params.=new: :$page-iterator;
        $inject-starting-params(%extracted-args);
        is-deeply %extracted-args, %ref-args, "Extracted args should get extra MaxKeys pair";
    }

    subtest 'Test page size incorrectly provided' => {
        plan 1;
        my %args = :arg1<foo>, :arg2<bar>, :PaginationConfig{:5PageSize};
        %pagination-config<limit_key>:delete;
        &method = sub (|) {};
        $paginator.=new: :&method, :$model, :%pagination-config;
        try { # todo die with definite error (like PaginationError/PaginationException)
            $paginator.paginate: |%args;
            flunk "Should die";
            CATCH { default { pass "Should die" } }
        }
    }
}

subtest 'Test paginator with path expressions' => {
    my Paginator $paginator;
    my (&method, $model, %pagination-config); # work vars

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        %pagination-config = :output_token['NextMarker || ListBucketResult.Contents[-1].Key'],
                :input_token<next_marker>, :result_key<Contents>;
        $model = get-operation-model-mock;
        (&method, $paginator) = Nil xx *;
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    subtest 'Test s3 list objects' => {
        plan 1;
        my @responses = {:NextMarker<token1>}, {:NextMarker<token2>}, {:not_next_token<foo>};
        my @called-args;
        &method = sub (*%args) { push @called-args, %args<next_marker>; @responses[(state $ = 0)++] };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $page-iterator := $paginator.paginate;
        @ = gather take $_ for $page-iterator;
        is-deeply @called-args, [Any, 'token1', 'token2'], "next_token should be [(Any) token1 token2]";
    }

    subtest 'Test s3 list object complex' => {
        plan 1;
        my @responses = {:NextMarker<token1>}, {:ListBucketResult{:Contents[{:Key<first>}, {:Key<Last>}]}},
                {:not_next_token<foo>};
        my @called-args;
        &method = sub (*%args) { push @called-args, %args<next_marker>; @responses[(state $ = 0)++] };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $page-iterator := $paginator.paginate;
        @ = gather take $_ for $page-iterator;
        is-deeply @called-args, [Any, 'token1', 'Last'], "next_token should be [(Any) token1 Last]";
    }
}

# fixme whole test is todos
subtest 'Test binary tokens' => {
    my Paginator $paginator;
    my (&method, $model, %pagination-config);

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        %pagination-config = :output_token<Marker>, :input_token<Marker>, :result_key<Users>;
        $model = get-operation-model-mock;
        (&method, $paginator) = Nil xx *;
    }
    LEAVE &subtest.unwrap: $unwrap;

    subtest 'Test build full result with bytes' => {
        plan 2;
        todo "Results not getting truncated to 3 items", 2;
        my @responses = {:Users[<User1 User2>], :Marker(Blob.new(0xff))},
                {:Users[<User3 User4>], :Marker(Blob.new(0xfe))}, {:Users[<User5>]};
        &method = sub (|) { @responses[(state $ = 0)++] };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $page-iterator := $paginator.paginate: :PaginationConfig{:3MaxItems};
        my %complete = $page-iterator.build-full-result;
        my $expected-token = TokenEncoder.encode: {:Marker(Blob.new(0xff)), :1boto_truncate_amount};
        is %complete<NextToken>, $expected-token, "NextToken should match";
        is-deeply %complete<Users>.map({$_}).Array, [<User1 User2 User3>], "Users should be [User1 User2 User3]";
    }

    subtest 'Test build full result with nested bytes' => {
        plan 2;
        todo "Results not getting truncated to 3 items", 2;
        my @responses = {:Users[<User1 User2>], :Marker{:key(Blob.new(0xff))}},
                {:Users[<User3 User4>], :Marker{:key(Blob.new(0xfe))}}, {:Users[<User5>]};
        &method = sub (|) { @responses[(state $ = 0)++] };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $page-iterator := $paginator.paginate: :PaginationConfig{:3MaxItems};
        my %complete = $page-iterator.build-full-result;
        my $expected-token = TokenEncoder.encode: {:Marker{:key(Blob.new(0xff))}, :1boto_truncate_amount};
        is %complete<NextToken>, $expected-token, "NextToken should match";
        is-deeply %complete<Users>.map({$_}).Array, [<User1 User2 User3>], "Users should be [User1 User2 User3]";
    }

    subtest 'Test build full result with listed bytes' => {
        plan 2;
        todo "Results not getting truncated to 3 items", 2;
        my @responses = {:Users[<User1 User2>], :Marker{:key['foo', Blob.new(0xff)]}},
                {:Users[<User3 User4>], :Marker{:key['foo', Blob.new(0xfe)]}},
                {:Users[<User5>]};
        &method = sub (|) { @responses[(state $ = 0)++] };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $page-iterator = $paginator.paginate: :PaginationConfig{:3MaxItems};
        my %complete = $page-iterator.build-full-result;
        my $expected-token = TokenEncoder.encode: {:Marker{:key['foo', Blob.new(0xff)]}, :1boto_truncate_amount};
        is %complete<NextToken>, $expected-token, "NextToken should match";
        is-deeply %complete<Users>.map({$_}).Array, [<User1 User2 User3>], "Users should be [User1 User2 User3]";
    }

    subtest 'Test build full result with multiple byte values' => {
        plan 2;
        todo "Results not getting truncated to 3 items", 2;
        my @responses = {:Users[<User1 User2>], :Marker{:key(Blob.new(0xff)), :key2(Blob.new(0x0ef))}},
                {:Users[<User3 User4>], :Marker{:key(Blob.new(0xfe)), :key2(Blob.new(0x0ee))}},
                {:Users[<User5>]};
        &method = sub (|) { @responses[(state $ = 0)++] };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $page-iterator = $paginator.paginate: :PaginationConfig{:3MaxItems};
        my %complete = $page-iterator.build-full-result;
        my $expected-token = TokenEncoder.encode: {
            :Marker{:key(Blob.new(0xff)), :key2(Blob.new(0xef))},
            :1boto_truncate_token
        };
        is %complete<NextToken>, $expected-token, "NextToken should match";
        is-deeply %complete<Users>.map({$_}).Array, [<User1 User2 User3>], "Users should be [User1 User2 User3]";
    }

    subtest 'Test resume with bytes' => {
        plan 2;
        todo "Starting token not fast-forwarding results", 2;
        my @responses = {:Users[<User3 User4>], :Marker(Blob.new(0xfe))}, {:Users[<User5>]};
        my @called-args;
        &method = sub (*%args) { push @called-args, %args<Marker>; @responses[(state $ = 0)++] };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $StartingToken = TokenEncoder.encode: {:Marker(Blob.new(0xff)), :1boto_truncate_amount};
        my $page-iterator = $paginator.paginate: :PaginationConfig{:$StartingToken};
        my %complete = $page-iterator.build-full-result;
        is-deeply %complete<Users>.map({$_}).Array, [<User4 User5>], "Users should be [User4 User5]";
        ok @called-args.grep({ .defined and $_ eqv [0xff] }).so, "Should have been called with 0xff marker";
    }

    subtest 'Test resume with nested bytes' => {
        plan 2;
        todo "Starting token not fast-forwarding results", 2;
        my @responses = {:Users[<User3 User4>], :Marker{:key(Blob.new(0xfe))}}, {:Users[<User5>]};
        my @called-args;
        &method = sub (*%args) { push @called-args, %args<Marker>; @responses[(state $ = 0)++] };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $StartingToken = TokenEncoder.encode: {:Marker{:key(Blob.new(0xff))}, :1boto_truncate_amount};
        my $page-iterator = $paginator.paginate: :PaginationConfig{:$StartingToken};
        my %complete = $page-iterator.build-full-result;
        is-deeply %complete<Users>.map({$_}).Array, [<User4 User5>], "Users should be [User4 User5]";
        ok @called-args.grep({ .defined and $_ eqv {:key[0xff]} }).so, "Should have been called with :key[0xff] marker";
    }

    subtest 'Test resume with listed bytes' => {
        plan 2;
        todo "Starting token not fast-forwarding results", 2;
        my @responses = {:Users[<User3 User4>], :Marker{:key(Blob.new(0xfe)), :key2(Blob.new(0xee))}},
                {:Users[<User5>]};
        my @called-args;
        &method = sub (*%args) { push @called-args, %args<Marker>; @responses[(state $ = 0)++] };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $StartingToken = TokenEncoder.encode: {:Marker{:key['foo', Blob.new(0xff)]}, :1boto_truncate_amount};
        my $page-iterator = $paginator.paginate: :PaginationConfig{:$StartingToken};
        my %complete = $page-iterator.build-full-result;
        is-deeply %complete<Users>.map({$_}).Array, [<User4 User5>], "Users should be [User4 User5]";
        ok @called-args.grep({.defined and $_ eqv {:key['foo', [0xff]]}}).so, "Should have been called with "
                ~ '{:key[foo 0xff]} marker';
    }

    subtest 'Test resume with multiple bytes values' => {
        plan 2;
        todo "Starting token not fast-forwarding results", 2;
        my @responses = {:Users[<User3 User4>], :Marker{:key(Blob.new(0xfe)), :key2(Blob.new(0xee))}},
                {:Users[<User5>]};
        my @called-args;
        &method = sub (*%args) { push @called-args, %args<Marker>; @responses[(state $ = 0)++] };
        $paginator.=new: :&method, :$model, :%pagination-config;
        my $StartingToken = TokenEncoder.encode: {
            :Marker{:key(Blob.new(0xff)), :key2(Blob.new(0xef))},
            :1boto_truncate_amount
        };
        my $page-iterator = $paginator.paginate: :PaginationConfig{:$StartingToken};
        my %complete = $page-iterator.build-full-result;
        is-deeply %complete<Users>.map({$_}).Array, [<User4 User5>], "Users should be [User4 User5]";
        ok @called-args.grep({ .defined and $_ eqv {:key(Blob.new(0xfe)), :key2(Blob.new(0xee))} }).so,
                'Should be called with Marker like {:key[0xfe], :key2[0xee]}';
    }
}

done-testing;
