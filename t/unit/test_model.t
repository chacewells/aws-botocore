use lib 'lib';
use Test;
use AWS::Botocore::Model;

subtest 'Test ServiceId' => {
    plan 2;
    is ('my service' but ServiceId).hyphenize, 'my-service', "Should replace spaces with hyphen";
    is ('MyService' but ServiceId).hyphenize, 'myservice', "Should lowercase string";
}

subtest 'Test ServiceModel' => {
    my %model;
    my ServiceModel $service-model;

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        %model =
            metadata => {
                protocol => 'query',
                endpointPrefix => 'endpoint-prefix',
                serviceId => 'MyService'
            },
            documentation => 'Documentation value',
            operations => {},
            shapes => {
                StringShape => {
                    type => 'string'
                }
            }
        ;
        $service-model.=new: :service-description(%model);
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    subtest 'Test metadata available' => {
        plan 1;
        is $service-model.metadata<protocol>, 'query', "protocol should be 'query'";
    }

    subtest 'Test service name can be overridden' => {
        plan 1;
        $service-model.=new: :service-description(%model), :service-name<myservice>;
        is $service-model.service-name, 'myservice', "service-name should be overridden";
    }

    subtest 'Test service name defaults to endpoint prefix' => {
        plan 1;
        is $service-model.service-name, 'endpoint-prefix', "Should default to endpoint-prefix";
    }

    subtest 'Test service id' => {
        plan 1;
        is $service-model.service-id, 'MyService', "service-id should be 'MyService'";
    }

    subtest 'test-hyphenize-service-id' => {
        plan 1;
        is $service-model.service-id.hyphenize, 'myservice', "Should hyphenize to 'myservice'";
    }

    subtest 'Test service id does not exists' => {
        plan 1;
        my %service-description = :metadata{:protocol<query>, :endpointPrefix<endpoint-prefix> },
                :documentation('Documentation value'), :operations{}, :shapes{:StringShape{:type<string>} };
        my $service-name = 'myservice';
        my ServiceModel $service-model.=new: :%service-description, :$service-name;
        try { # todo die with UndefinedModelAttributeError
            $service-model.service-id;
            flunk "Should die";
            CATCH { default { pass "Should die" } }
        }
    }

    subtest 'Test operation does not exist' => {
        plan 1;
        try { # todo die with OperationNotFoundError
            $service-model.operation-model: 'NoExistOperation';
            flunk "Should die";
            CATCH { default { pass "Should die" } }
        }
    }

    subtest 'Test signing name defaults to endpoint prefix' => {
        plan 1;
        is $service-model.signing-name, 'endpoint-prefix', "signing-name should default to 'endpoint-prefix'";
    }

    subtest 'Test documentation exposed as property' => {
        plan 1;
        is $service-model.documentation, 'Documentation value', "documentation should match input";
    }

    subtest 'Test shape names' => {
        plan 1;
        is-deeply $service-model.shape-names.Array, ['StringShape'], "shape-names should be ['StringShape']";
    }

    subtest 'Test gist has service name' => {
        plan 1;
        ok $service-model.gist ~~ /endpoint'-'prefix/, "gist contains service name";
    }

    done-testing;
}

subtest 'Test OperationModel from Service' => {
    my %model;
    my ServiceModel $service-model;
    my $unwrap = ENTER &subtest.wrap: sub (|) {
        %model =
            'metadata' => {'protocol' => 'query', 'endpointPrefix' => 'foo'},
            'documentation' => '',
            'operations' => {
                'OperationName' => {
                    'http' => {
                        'method' => 'POST',
                        'requestUri' => '/',
                    },
                    'name' => 'OperationName',
                    'input' => ${ 'shape' => 'OperationNameRequest' },
                    'output' => ${ 'shape' => 'OperationNameResponse', },
                    'errors' => $[${'shape' => 'NoSuchResourceException'}],
                    'documentation' => 'Docs for OperationName',
                    'authtype' => 'v4'
                    },
                'OperationTwo' => {
                    'http' => {
                        'method' => 'POST',
                        'requestUri' => '/',
                    },
                    'name' => 'OperationTwo',
                    'input' => ${ 'shape' => 'OperationNameRequest' },
                    'output' => ${ 'shape' => 'OperationNameResponse', },
                    'errors' => [${'shape' => 'NoSuchResourceException'}],
                    'documentation' => 'Docs for OperationTwo',
                }
            },
            'shapes' => {
                'OperationNameRequest' => {
                    'type' => 'structure',
                    'members' => {
                        'Arg1' => ${'shape' => 'stringType'},
                        'Arg2' => ${'shape' => 'stringType'},
                    }
                },
                'OperationNameResponse' => {
                    'type' => 'structure',
                    'members' => ${
                        'String' => ${ 'shape' => 'stringType', }
                    }
                },
                'NoSuchResourceException' => {
                    'type' => 'structure',
                    'members' => {}
                },
                'stringType' => ${ 'type' => 'string', }
            }
        ;
        $service-model.=new: :service-description(%model);
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    subtest 'Test wire name always matches model' => {
        plan 2;
        my OperationModel $operation.=new: :operation-model(%model<operations><OperationName>),
                :$service-model, :name<Foo>;
        is $operation.name, 'Foo', "Operation name is 'Foo'";
        is $operation.wire-name, 'OperationName', "wire-name is 'OperationName'";
    }

    subtest 'Test operation name in repr' => {
        plan 1;
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        ok $operation.gist ~~ /OperationName/, "representation of OperationModel contains 'OperationName'";
    }

    subtest 'Test name and wire name defaults to same value' => {
        plan 2;
        my OperationModel $operation.=new: :operation-model(%model<operations><OperationName>), :$service-model;
        is $operation.name, 'OperationName', "Name should be 'OperationName'";
        is $operation.wire-name, 'OperationName', "Wire name should also be 'OperationName'";
    }

    subtest 'Test-name-from-service' => {
        plan 1;
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        ok $operation.gist ~~ /OperationName/, "Operation gist contains operation key";
    }

    subtest 'Test name from service model when differs from name' => {
        plan 1;
        %model<operations><Foo> = %model<operations><OperationName>;
        $service-model.=new: :service-description(%model);
        my OperationModel $operation = $service-model.operation-model: 'Foo';
        is $operation.name, 'Foo', "Operation name should match its service description key";
    }

    subtest 'Test operation input model' => {
        plan 6;
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        is $operation.name, 'OperationName', "Operation name is 'OperationName'";
        is $operation.metadata<protocol>, 'query', "protocol == 'query'";
        is $operation.http<method>, 'POST', "Method is 'POST'";
        is $operation.http<requestUri>, '/', "requestUri is '/'";
        my Shape $shape = $operation.input-shape;
        is $shape.name, 'OperationNameRequest', "Shape name is 'OperationNameRequest'";
        is-deeply $shape.members.keys.sort.Array, [<Arg1 Arg2>], "Shape members should be Arg1, Arg2";
    }

    subtest 'Test has documentation property' => {
        plan 1;
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        is $operation.documentation, 'Docs for OperationName', "Docs should match model docs";
    }

    subtest 'Test service model available from operation model' => {
        plan 1;
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        ok $operation.service-model === $service-model, "Operation should contain service model reference";
    }

    subtest 'Test operation output model' => {
        plan 2;
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        my Shape $output = $operation.output-shape;
        is-deeply $output.members.keys.Array, [<String>], "Members should be singleton array ['String']";
        nok $operation.has-streaming-output, "Shouldn't contain streaming output";
    }

    subtest 'Test operation shape not required' => {
        plan 1;
        %model<operations><OperationName><output>:delete;
        $service-model.=new: :service-description(%model);
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        my Shape $output-shape = $operation.output-shape;
        nok $output-shape.defined, "output shape should be undefined";
    }

    subtest 'Test error shapes' => {
        plan 2;
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        is $operation.error-shapes.elems, 1, "There should be one error shape";
        # todo
        is $operation.error-shapes[0].name, 'NoSuchResourceException', "error name should be 'NoSuchResourceException'";
    }

    subtest 'Test has auth type' => {
        plan 1;
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        is $operation.auth-type, 'v4', "Auth type should be 'v4'";
    }

    subtest 'Test auth type not set' => {
        plan 1;
        nok $service-model.operation-model('OperationTwo').auth-type.defined, "Auth type shouldn't be defined";
    }

    subtest 'Test deprecated present' => {
        plan 1;
        %model<operations><OperationName><deprecated> = True;
        $service-model.=new: :service-description(%model);
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        ok $operation.deprecated, "Should be deprecated";
    }

    subtest 'Test deprecated present false' => {
        plan 1;
        %model<operations><OperationName><deprecated> = False;
        $service-model.=new: :service-description(%model);
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        nok $operation.deprecated, "Shouldn't be deprecated";
    }

    subtest 'Test deprecated absent' => {
        plan 1;
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        nok $operation.deprecated, "Shouldn't be deprecated";
    }

    subtest 'Test endpoint operation present' => {
        plan 1;
        %model<operations><OperationName><endpointoperation> = True;
        $service-model.=new: :service-description(%model);
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        ok $operation.is-endpoint-discovery-operation, "Should be endpoint discovery operation";
    }

    subtest 'Test endpoint operation present false' => {
        plan 1;
        %model<operations><OperationName><endpointoperation> = False;
        $service-model.=new: :service-description(%model);
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        nok $operation.is-endpoint-discovery-operation, "Shouldn't be endpoint discovery operation";
    }

    subtest 'Test endpoint operation absent' => {
        plan 1;
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        nok $operation.is-endpoint-discovery-operation, "Shouldn't be endpoint discovery operation";
    }

    subtest 'Test endpoint discovery present' => {
        plan 1;
        .<endpointdiscovery> = {:required} with %model<operations><OperationName>;
        $service-model.=new: :service-description(%model);
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        ok $operation.endpoint-discovery<required>, "required should be present and true";
    }

    subtest 'Test endpoint discovery absent' => {
        plan 1;
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        nok $operation.endpoint-discovery.defined, "endpoint discovery shouldn't be defined";
    }

    done-testing;
}

subtest 'TestOperationModelEventStreamTypes' => {
    my %model;
    my ServiceModel $service-model;

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        %model =
            'metadata' => {'protocol' => 'rest-xml', 'endpointPrefix' => 'foo'},
            'documentation' => '',
            'operations' => {
                'OperationName' => {
                    'http' => {
                        'method' => 'POST',
                        'requestUri' => '/',
                    },
                    'name' => 'OperationName',
                    'input' => {'shape' => 'OperationRequest'},
                    'output' => {'shape' => 'OperationResponse'},
                }
            },
            'shapes' => {
                'NormalStructure' => {
                    'type' => 'structure',
                    'members' => {
                        'Input' => {'shape' => 'StringType'}
                    }
                },
                'OperationRequest' => {
                    'type' => 'structure',
                    'members' => {
                        'String' => {'shape' => 'StringType'},
                        "Body" => {'shape' => 'EventStreamStructure'}
                    },
                    'payload' => 'Body'
                },
                'OperationResponse' => {
                    'type' => 'structure',
                    'members' => {
                        'String' => {'shape' => 'StringType'},
                        "Body" => {'shape' => 'EventStreamStructure'}
                    },
                    'payload' => 'Body'
                },
                'StringType' => {'type' => 'string'},
                'BlobType' => {'type' => 'blob'},
                'EventStreamStructure' => {
                    'eventstream' => True,
                    'type' => 'structure',
                    'members' => {
                        'EventA' => {'shape' => 'EventAStructure'},
                        'EventB' => {'shape' => 'EventBStructure'}
                    }
                },
                'EventAStructure' => {
                    'event' => True,
                    'type' => 'structure',
                    'members' => {
                        'Payload' => {
                            'shape' => 'BlobType',
                            'eventpayload' => True
                        },
                        'Header' => {
                            'shape' => 'StringType',
                            'eventheader' => True
                        }
                    }
                },
                'EventBStructure' => {
                    'event' => True,
                    'type' => 'structure',
                    'members' => {
                        'Records' => {'shape' => 'StringType'}
                    }
                }
            }
        ;
        $service-model.=new: :service-description(%model);
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    my sub update-operation(*%args) {
        .{%args.keys} = %args.values with %model<operations><OperationName>;
    }

    subtest 'Test event stream input for operation' => {
        plan 2;
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        ok $operation.has-event-stream-input, "Should have event stream input";
        my Shape $event-stream-input = $operation.get-event-stream-input;
        is $event-stream-input.name, 'EventStreamStructure', "Should be an 'EventStreamStructure'";
    }

    subtest 'Test no event stream input for operation' => {
        plan 2;
        update-operation input => { shape => 'NormalStructure' };
        $service-model.=new: :service-description(%model);
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        nok $operation.has-event-stream-input, "Should not have event stream input";
        nok $operation.get-event-stream-input.defined, "event stream input should be undefined";
    }

    subtest 'Test event stream output for operation' => {
        plan 2;
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        ok $operation.has-event-stream-output, "Should have event stream output";
        my Shape $output = $operation.get-event-stream-output;
        is $output.name, 'EventStreamStructure', "output should be 'EventStreamStructure'";
    }

    subtest 'Test no event stream output for operation' => {
        plan 2;
        update-operation output => { shape => 'NormalStructure' };
        $service-model.=new: :service-description(%model);
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        nok $operation.has-event-stream-output, "Should not have event stream output";
        nok $operation.get-event-stream-output.defined, "event stream output should be undefined";
    }

    subtest 'Test no output shape' => {
        plan 2;
        update-operation :output(Nil);
        %model<operations><OperationName><output>:delete;
        $service-model.=new: :service-description(%model);
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        nok $operation.has-event-stream-output, "Shouldn't have event stream output";
        nok $operation.get-event-stream-output.defined, "event stream output should be undefined";
    }

    done-testing;
}

subtest 'Test operation model streaming types' => {
    my %model;
    my ServiceModel $service-model;

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        %model =
            'metadata' => {'protocol' => 'query', 'endpointPrefix' => 'foo'},
            'documentation' => '',
            'operations' => {
                'OperationName' => {
                    'name' => 'OperationName',
                    'input' => { 'shape' => 'OperationRequest', },
                    'output' => { 'shape' => 'OperationResponse', },
                }
            },
            'shapes' => {
                'OperationRequest' => {
                    'type' => 'structure',
                    'members' => {
                        'String' => { 'shape' => 'stringType', },
                        "Body" => { 'shape' => 'blobType', }
                    },
                    'payload' => 'Body'
                },
                'OperationResponse' => {
                    'type' => 'structure',
                    'members' => {
                        'String' => { 'shape' => 'stringType', },
                        "Body" => { 'shape' => 'blobType', }
                    },
                    'payload' => 'Body'
                },
                'stringType' => { 'type' => 'string', },
                'blobType' => { 'type' => 'blob' }
            }
        ;
        $service-model.=new: :service-description(%model);
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    my sub remove-payload(Str $type) {
        %model<shapes>{"Operation$type"}<payload>:delete
    }

    subtest 'Test streaming input for operation' => {
        plan 2;
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        ok $operation.has-streaming-input, "Should have streaming input";
        is $operation.get-streaming-input.name, 'blobType', "Input name should be 'blobType'";
    }

    subtest 'Test not streaming input for operation' => {
        plan 2;
        remove-payload 'Request';
        $service-model.=new: :service-description(%model);
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        nok $operation.has-streaming-input, "Should not have streaming input";
        nok $operation.get-streaming-input.defined, "streaming input should not be defined";
    }

    subtest 'Test streaming output for operation' => {
        plan 2;
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        ok $operation.has-streaming-output, "Should have streaming output";
        is $operation.get-streaming-output.name, 'blobType', "Input name should be 'blobType'";
    }

    subtest 'Test not streaming output for operation' => {
        plan 2;
        remove-payload 'Request';
        $service-model.=new: :service-description(%model);
        my OperationModel $operation = $service-model.operation-model: 'OperationName';
        nok $operation.has-streaming-input, "Should not have streaming input";
        nok $operation.get-streaming-input.defined, "streaming input should not be defined";
    }

    done-testing;
}

subtest 'Test deep merge' => {
    my %shapes;
    my ShapeResolver $shape-resolver;

    my $unwrap = ENTER &subtest.wrap: sub (|) {
        %shapes =
            'SetQueueAttributes' => {
                'type' => 'structure',
                'members' => {
                    'MapExample' => {'shape' => 'StrToStrMap', 'locationName' => 'Attribute'},
                }
            },
            'SetQueueAttributes2' => {
                'type' => 'structure',
                'members' => {
                    'MapExample' => {'shape' => 'StrToStrMap', 'locationName' => 'Attribute2'},
                }
            },
            'StrToStrMap' => {
                'type' => 'map',
                'key' => {'shape' => 'StringType', 'locationName' => 'Name'},
                'value' => {'shape' => 'StringType', 'locationName' => 'Value'},
                'flattened' => True,
                'name' => 'NotAttribute',
            },
            'StringType' => {'type' => 'string'}
        ;
        $shape-resolver.=new: :shape-map(%shapes);
        callsame;
    }
    LEAVE &subtest.unwrap: $unwrap;

    subtest 'Test deep merge' => {
        plan 4;
        my StructureShape $shape = $shape-resolver.get-shape-by-name: 'SetQueueAttributes';
        my MapShape $map-merged = $shape.members<MapExample>;
        # map_merged has a serialization as a member trait as well as
        # in the StrToStrMap.
        # The member trait should have precedence.
        is-deeply $map-merged.serialization, {:name<Attribute>, :flattened},
                Q[Serializes as {:name<Attribute>:flattened}];
        # Ensure we don't merge/mutate the original hashes.
        is $map-merged.key.serialization<name>, 'Name', "key is 'Name'";
        is $map-merged.value.serialization<name>, 'Value', "value is 'Value'";
        is $map-merged.key.serialization<name>, 'Name', "key is still 'Name'?";
    }

    subtest 'Test merges copy hash' => {
        plan 2;
        my StructureShape $shape = $shape-resolver.get-shape-by-name: 'SetQueueAttributes';
        my MapShape $map-merged = $shape.members<MapExample>;
        is $map-merged.serialization<name>, 'Attribute', "name is 'Attribute'";

        my StructureShape $shape2 = $shape-resolver.get-shape-by-name: 'SetQueueAttributes2';
        my MapShape $map-merged2 = $shape2.members<MapExample>;
        is $map-merged2.serialization<name>, 'Attribute2', "name is 'Attribute2'";
    }

    done-testing;
}

subtest 'Test ShapeResolver' => {
    subtest 'Test get-shape-by-name' => {
        plan 2;
        my %shape-map = :Foo{ :type<structure>, :members{:Bar{:shape<StringType>}, :Baz{:shape<StringType>}} },
                :StringType{:type<string>};
        my ShapeResolver $resolver.=new: :%shape-map;
        my Shape $shape = $resolver.get-shape-by-name: 'Foo';
        is $shape.name, 'Foo', "Shape should be Foo";
        is $shape.type-name, 'structure', "Shape type should be structure";
    }

    subtest 'Test resolve shape reference' => {
        plan 2;
        my %shape-map = :Foo{ :type<structure>, :members{ :Bar{shape => 'StringType'}, :Baz{shape => 'StringType'} } },
                :StringType{:type<string>};
        my ShapeResolver $resolver.=new: :%shape-map;
        my Shape $shape = $resolver.resolve-shape-ref: {shape => 'StringType'};
        is $shape.name, 'StringType', "Shape should be StringType";
        is $shape.type-name, 'string', "Shape type should be string";
    }

    subtest 'Test resolve shape reference with member traits' => {
        plan 2;
        my %shape-map =
                Foo => {
                    type => 'structure',
                    members => {
                        Bar => ${shape => 'StringType'},
                        Baz => {shape => 'StringType', locationName => 'other'},
                    }
                },
                StringType => ${ type => "string" }
        ;
        my ShapeResolver $resolver.=new: :%shape-map;
        my Shape $shape = $resolver.resolve-shape-ref: {:shape<StringType>, :locationName<other>};
        is $shape.serialization<name>, 'other', "serialization.name should be 'other'";
        is $shape.name, 'StringType', "Shape should be StringType";
    }

    subtest 'Test serialization cache' => {
        plan 2;
        my %shape-map =
            Foo => {
                type => 'structure',
                members => {
                    Baz => {shape => 'StringType', locationName => 'other'},
                }
            },
            StringType => {
                type => "string"
            }
        ;
        my ShapeResolver $resolver.=new: :%shape-map;
        my Shape $shape = $resolver.resolve-shape-ref: {:shape<StringType>, :locationName<other>};
        is $shape.serialization<name>, 'other', "serialization.name should be 'other'";
        is $shape.serialization<name>, 'other', "serialization.name should be 'other'";
    }

    subtest 'Test shape overrides' => {
        plan 1;
        my %shape-map = StringType => { type => 'string', documentation => 'Original documentation' };
        my ShapeResolver $resolver.=new: :%shape-map;
        my Shape $shape = $resolver.get-shape-by-name: 'StringType';
        is $shape.documentation, 'Original documentation', "documentation should match";
    }

    subtest 'Test shape type structure' => {
        plan 3;
        my %shape-map =
            ChangePasswordRequest => {
                type => 'structure',
                members => {
                    OldPassword => ${shape => 'passwordType'},
                    NewPassword => ${shape => 'passwordType'},
                }
            },
            passwordType => ${ type => "string" }
        ;
        my ShapeResolver $resolver.=new: :%shape-map;
        my StructureShape $shape = $resolver.get-shape-by-name: 'ChangePasswordRequest';
        is $shape.type-name, 'structure', "Type should be structure";
        is $shape.name, 'ChangePasswordRequest', "Name should be ChangePasswordRequest";
        is-deeply $shape.members.keys.sort.Array, [<NewPassword OldPassword>], "Member keys should match input";
    }

    subtest 'Test shape metadata' => {
        plan 4;
        my %shape-map =
            'ChangePasswordRequest' => {
                'type' => 'structure',
                'required' => ['OldPassword', 'NewPassword'],
                'members' => {
                    'OldPassword' => ${'shape' => 'passwordType'},
                    'NewPassword' => ${'shape' => 'passwordType'},
                }
            },
            passwordType => ${
                type => "string",
                min => 1,
                max => 128,
                sensitive => True
            }
        ;
        my ShapeResolver $resolver = ShapeResolver.new: :%shape-map;
        my StructureShape $shape = $resolver.get-shape-by-name: 'ChangePasswordRequest';
        is-deeply $shape.metadata<required>.sort.Array, [<NewPassword OldPassword>],
                "Should contain member keys";
        my Shape $member = $shape.members<OldPassword>;
        is $member.metadata<min>, 1, "Min should be 1";
        is $member.metadata<max>, 128, "Max should be 128";
        ok $member.metadata<sensitive>, "sensitive is True";
    }

    subtest 'Test shape list' => {
        plan 3;
        my %shape-map =
            'mfaDeviceListType' => {
                "type" => "list",
                "member" => ${"shape" => "MFADevice"},
            },
            'MFADevice' => {
                'type' => 'structure',
                'members' => {
                    'UserName' => ${'shape' => 'userNameType'}
                }
            },
            'userNameType' => ${ 'type' => 'string' }
        ;
        my ShapeResolver $resolver = ShapeResolver.new: :%shape-map;
        my ListShape $shape = $resolver.get-shape-by-name: 'mfaDeviceListType';
        is $shape.member.type-name, 'structure', "Member should be structure";
        is $shape.member.name, 'MFADevice', "Member should be MFA device";
        is-deeply $shape.member.members{}:k.Array, ['UserName'], "UserName should be member";
    }

    subtest 'Test shape does not exist' => {
        plan 1;
        my ShapeResolver $resolver.=new;
        try {
            $resolver.get-shape-by-name: 'NothingHere';
            flunk "Should die";
            CATCH { default { pass "Should die" } }
        }
    }

    subtest 'Test missing type key' => {
        plan 1;
        my %shape-map = UnknownType => {NotTheTypeKey => 'someUnknownType'};
        my ShapeResolver $resolver.=new: :%shape-map;
        try {
            $resolver.get-shape-by-name: 'UnknownType';
            flunk "Should die";
            CATCH { default { pass "Should die" } }
        }
    }

    subtest 'Test bad shape ref' => {
        plan 1;
        my %shape-map = Struct => {
            type => 'structure',
            members => {
                A => ${:type<string>},
                B => ${:type<string>},
            }
        };
        my ShapeResolver $resolver.=new: :%shape-map;
        try {
            my $struct = $resolver.get-shape-by-name: 'Struct';
            $struct.members;
            flunk "Should die";
            CATCH { default { pass "Should die" } }
        }
    }

    subtest 'Test shape name in gist' => {
        plan 1;
        my %shape-map = :StringType{:type<string>};
        my ShapeResolver $resolver.=new: :%shape-map;
        ok $resolver.get-shape-by-name('StringType').gist.contains('StringType'),
                "gist should contain 'StringType'";
    }

    done-testing;
}

subtest 'Test Builders' => {
    my DenormalizedStructureBuilder $b;
    my $unwrap = ENTER &subtest.wrap: sub (|) { $b.=new };
    LEAVE &subtest.unwrap: $unwrap;

    subtest 'Test structure shape builder with scalar types' => {
        plan 2;
        my StructureShape $shape = $b.with-members({ :A{:type<string>}, :B{:type<integer>}, }).build-model;
        ok $shape ~~ StructureShape, "Should be a StructureShape";
        is-deeply $shape.members.keys.sort.Array, [<A B>], "Member keys should be [A B]";
    }

    subtest 'Test structure shape with structure type' => {
        plan 4;
        my StructureShape $shape = $b.with-members({
            A => {
                :type<structure>,
                members => { 'A-1' => {:type<string>} }
            }
        }).build-model;
        ok $shape ~~ StructureShape, "Should be a StructureShape";
        is-deeply $shape.members.keys.Array, [<A>], "Member keys same as input";
        is-deeply $shape.members<A>.type-name, 'structure', "Type should be 'structure'";
        is-deeply $shape.members<A>.members.keys.Array, [<A-1>], "Member's members same as input";
    }

    subtest 'Test structure shape with list' => {
        plan 2;
        my StructureShape $shape = $b.with-members({
            A => {
                :type<list>,
                member => {:type<string>}
            }
        }).build-model;
        ok $shape.members<A> ~~ ListShape, "Should be ListShape";
        is $shape.members<A>.member.type-name, 'string', "Should be 'string' type";
    }

    subtest 'Test structure shape with map type' => {
        plan 3;
        my StructureShape $shape = $b.with-members({
            A => {
                type => 'map',
                key => ${:type<string>},
                value => ${:type<string>}
            }
        }).build-model;
        ok $shape.members<A> ~~ MapShape, "Should be MapShape";
        my MapShape $map-shape = $shape.members<A>;
        is $map-shape.key.type-name, 'string', "key type should be 'string'";
        is $map-shape.value.type-name, 'string', "value type should be 'string'";
    }

    subtest 'Test nested structure' => {
        plan 1;
        my $shape = $b.with-members({
            A => {
                :type<structure>,
                members => { :B{ :type<structure>, :members{ :C{:type<string>} } } }
            }
        }).build-model;
        is $shape.members<A>.members<B>.members<C>.type-name, 'string', "nested type should be 'string'";
    }

    subtest 'Test enum values on string used' => {
        plan 4;
        my @enum-values = <foo bar baz>;
        my StructureShape $shape = $b.with-members({
            :A{ :type<string>, :enum(@enum-values) }
        }).build-model;
        ok $shape ~~ StructureShape, "Should be StructureShape";
        my StringShape $string-shape = $shape.members<A>;
        ok $string-shape ~~ StringShape, "Should be StringShape";
        is-deeply $string-shape.metadata<enum>, @enum-values, "metadata<enum> should match enums input";
        is-deeply $string-shape.enum, @enum-values, "String enum should match input";
    }

    subtest 'Test documentation on shape used' => {
        plan 1;
        my StructureShape $shape = $b.with-members({
            :A{ :type<string>, :documentation<MyDocs> }
        }).build-model;
        is $shape.members<A>.documentation, 'MyDocs', "documentation should match input";
    }

    subtest 'Test min max used in metadata' => {
        plan 2;
        my StructureShape $shape = $b.with-members({
            :A{ :type<string>, :documentation<MyDocs>, :min(2), :max(3) }
        }).build-model;
        my %metadata = $shape.members<A>.metadata;
        is %metadata<min>, 2, "min is 2";
        is %metadata<max>, 3, "max is 3";
    }

    subtest 'Test use shape name when provided' => {
        plan 1;
        my StructureShape $shape = $b.with-members({
            :A{ :type<string>, :shape-name<MyStringShape> }
        }).build-model;
        is $shape.members<A>.name, 'MyStringShape', "name should match input";
    }

    subtest 'Test unknown shape type' => {
        plan 1;
        try {
            $b.with-members({ A => {:type<brand-new-shape-type>} }).build-model;
            flunk "Should die";
            # todo die with InvalidShapeError
            CATCH { default { pass "Should die" } }
        }
    }

    subtest 'Test ordered shape builder' => {
        skip "Not yet implemented";
        # todo need ordered hash type
    }

    done-testing;
}

done-testing;
